var datatramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế Xã Lộc Hòa, Thành phố Nam Định, Tỉnh Nam Định",
   "address": "Xã Lộc Hòa, Thành phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4335694,
   "Latitude": 106.1491451
 },
 {
   "STT": 2,
   "Name": "Trạm y tế Xã Nam Phong, Thành phố Nam Định, Tỉnh Nam Định",
   "address": "Xã Nam Phong, Thành phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4230198,
   "Latitude": 106.1978733
 },
 {
   "STT": 3,
   "Name": "Trạm y tế Xã Mỹ Xá, Thành phố Nam Định, Tỉnh Nam Định",
   "address": "Xã Mỹ Xá, Thành phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4152861,
   "Latitude": 106.145553
 },
 {
   "STT": 4,
   "Name": "Trạm y tế Xã Lộc An,  Thành phố Nam Định, Tỉnh Nam Định",
   "address": "Xã Lộc An,  Thành phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4039108,
   "Latitude": 106.153117
 },
 {
   "STT": 5,
   "Name": "Trạm y tế Xã Nam Vân, Thành phố Nam Định, Tỉnh Nam Định",
   "address": "Xã Nam Vân, Thành phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.3974479,
   "Latitude": 106.1866496
 },
 {
   "STT": 6,
   "Name": "Trạm y tế Xã Mỹ Hà , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Hà , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4888622,
   "Latitude": 106.1286901
 },
 {
   "STT": 7,
   "Name": "Trạm y tế Xã Mỹ Tiến , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Tiến , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4705607,
   "Latitude": 106.1081103
 },
 {
   "STT": 8,
   "Name": "Trạm y tế Xã Mỹ Thắng , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Thắng , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4619909,
   "Latitude": 106.1375107
 },
 {
   "STT": 9,
   "Name": "Trạm y tế Xã Mỹ Trung , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Trung , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4717069,
   "Latitude": 106.1875016
 },
 {
   "STT": 10,
   "Name": "Trạm y tế Xã Mỹ Tân , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Tân , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4558942,
   "Latitude": 106.2107543
 },
 {
   "STT": 11,
   "Name": "Trạm y tế Xã Mỹ Phúc , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Phúc , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4655449,
   "Latitude": 106.1610343
 },
 {
   "STT": 12,
   "Name": "Trạm y tế Xã Mỹ Hưng , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Hưng , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4394064,
   "Latitude": 106.1316303
 },
 {
   "STT": 13,
   "Name": "Trạm y tế Xã Mỹ Thuận , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Thuận , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4570889,
   "Latitude": 106.0669571
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Xã Mỹ Thịnh , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Thịnh , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4380655,
   "Latitude": 106.0845931
 },
 {
   "STT": 15,
   "Name": "Trạm y tế Xã Mỹ Thành , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "address": "Xã Mỹ Thành , Huyện Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4237181,
   "Latitude": 106.11399
 },
 {
   "STT": 16,
   "Name": "Trạm y tế Xã Minh Thuận , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Minh Thuận , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.4387727,
   "Latitude": 106.0463837
 },
 {
   "STT": 17,
   "Name": "Trạm y tế Xã Hiển Khánh  , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Hiển Khánh  , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.4365582,
   "Latitude": 106.0698963
 },
 {
   "STT": 18,
   "Name": "Trạm y tế Xã Tân Khánh , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Tân Khánh , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3972979,
   "Latitude": 106.0516078
 },
 {
   "STT": 19,
   "Name": "Trạm y tế Xã Hợp Hưng , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Hợp Hưng , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.4123044,
   "Latitude": 106.0816536
 },
 {
   "STT": 20,
   "Name": "Trạm y tế Xã Đại An , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Đại An , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3974059,
   "Latitude": 106.1169299
 },
 {
   "STT": 21,
   "Name": "Trạm y tế Xã Tân Thành , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Tân Thành , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.385673,
   "Latitude": 106.1492721
 },
 {
   "STT": 22,
   "Name": "Trạm y tế Xã Cộng Hòa , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Cộng Hòa , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3860211,
   "Latitude": 106.075964
 },
 {
   "STT": 23,
   "Name": "Trạm y tế Xã Trung Thành , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Trung Thành , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3859903,
   "Latitude": 106.0845931
 },
 {
   "STT": 24,
   "Name": "Trạm y tế Xã Quang Trung , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Quang Trung , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3785433,
   "Latitude": 106.1022307
 },
 {
   "STT": 25,
   "Name": "Trạm y tế Xã Minh Tân , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Minh Tân , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3771821,
   "Latitude": 106.0552006
 },
 {
   "STT": 26,
   "Name": "Trạm y tế Xã Liên Bảo , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Liên Bảo , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3626947,
   "Latitude": 106.1169299
 },
 {
   "STT": 27,
   "Name": "Trạm y tế Xã Thành Lợi , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Thành Lợi , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.360476,
   "Latitude": 106.140451
 },
 {
   "STT": 28,
   "Name": "Trạm y tế Xã Kim Thái , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Kim Thái , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3544448,
   "Latitude": 106.0816536
 },
 {
   "STT": 29,
   "Name": "Trạm y tế Xã Liên Minh , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Liên Minh , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.340662,
   "Latitude": 106.1051705
 },
 {
   "STT": 30,
   "Name": "Trạm y tế Xã Đại Thắng , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Đại Thắng , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3257697,
   "Latitude": 106.140451
 },
 {
   "STT": 31,
   "Name": "Trạm y tế Xã Tam Thanh , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Tam Thanh , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3171144,
   "Latitude": 106.0787142
 },
 {
   "STT": 32,
   "Name": "Trạm y tế Xã Vĩnh Hào , Huyện Vụ Bản, Tỉnh Nam Định",
   "address": "Xã Vĩnh Hào , Huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.313247,
   "Latitude": 106.1198699
 },
 {
   "STT": 33,
   "Name": "Trạm y tế Xã Trung Yên , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Trung Yên , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.4106639,
   "Latitude": 105.9758633
 },
 {
   "STT": 34,
   "Name": "Trạm y tế Xã Yên Thành , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Thành , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3981149,
   "Latitude": 105.9552997
 },
 {
   "STT": 35,
   "Name": "Trạm y tế Xã Yên Tân , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Tân , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3884806,
   "Latitude": 105.9964291
 },
 {
   "STT": 36,
   "Name": "Trạm y tế Xã Yên Lợi , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Lợi , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3796647,
   "Latitude": 106.0125649
 },
 {
   "STT": 37,
   "Name": "Trạm y tế Xã Yên Thọ , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Thọ , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3823928,
   "Latitude": 105.9376756
 },
 {
   "STT": 38,
   "Name": "Trạm y tế Xã Yên Nghĩa , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Nghĩa , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3854401,
   "Latitude": 105.9670501
 },
 {
   "STT": 39,
   "Name": "Trạm y tế Xã Yên Minh , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Minh , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.379389,
   "Latitude": 106.0316898
 },
 {
   "STT": 40,
   "Name": "Trạm y tế Xã Yên Phương , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Phương , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3697196,
   "Latitude": 105.9494248
 },
 {
   "STT": 41,
   "Name": "Trạm y tế Xã Yên Chính , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Chính , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3736721,
   "Latitude": 105.9807959
 },
 {
   "STT": 42,
   "Name": "Trạm y tế Xã Yên Bình , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Bình , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3521056,
   "Latitude": 106.0140586
 },
 {
   "STT": 43,
   "Name": "Trạm y tế Xã Yên Phú , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Phú , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3507104,
   "Latitude": 105.9670501
 },
 {
   "STT": 44,
   "Name": "Trạm y tế Xã Yên Mỹ , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Mỹ , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3566538,
   "Latitude": 106.0581397
 },
 {
   "STT": 45,
   "Name": "Trạm y tế Xã Yên Dương , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Dương , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3441172,
   "Latitude": 106.0375673
 },
 {
   "STT": 46,
   "Name": "Trạm y tế Xã Yên Xá , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Xá , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.329717,
   "Latitude": 106.0214048
 },
 {
   "STT": 47,
   "Name": "Trạm y tế Xã Yên Hưng , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Hưng , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3344413,
   "Latitude": 105.9552997
 },
 {
   "STT": 48,
   "Name": "Trạm y tế Xã Yên Khánh , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Khánh , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3363922,
   "Latitude": 105.9964291
 },
 {
   "STT": 49,
   "Name": "Trạm y tế Xã Yên Phong , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Phong , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.318055,
   "Latitude": 105.9758633
 },
 {
   "STT": 50,
   "Name": "Trạm y tế Xã Yên Ninh , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Ninh , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.320421,
   "Latitude": 106.0434449
 },
 {
   "STT": 51,
   "Name": "Trạm y tế Xã Yên Lương , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Lương , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3060949,
   "Latitude": 106.0728356
 },
 {
   "STT": 52,
   "Name": "Trạm y tế Xã Yên Hồng , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Hồng , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3005719,
   "Latitude": 106.0081819
 },
 {
   "STT": 53,
   "Name": "Trạm y tế Xã Yên Quang , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Quang , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.2906424,
   "Latitude": 105.9905529
 },
 {
   "STT": 54,
   "Name": "Trạm y tế Xã Yên Tiến , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Tiến , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.2894216,
   "Latitude": 106.0346285
 },
 {
   "STT": 55,
   "Name": "Trạm y tế Xã Yên Thắng , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Thắng , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.2891034,
   "Latitude": 106.0743989
 },
 {
   "STT": 56,
   "Name": "Trạm y tế Xã Yên Phúc , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Phúc , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.2890031,
   "Latitude": 106.1316303
 },
 {
   "STT": 57,
   "Name": " Trạm y tế Xã Yên Cường , Huyện Ý Yên, Tỉnh Nam Định",
   "address": " Xã Yên Cường , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.2839168,
   "Latitude": 106.0934117
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Xã Yên Lộc, Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Lộc, Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.2848772,
   "Latitude": 106.11399
 },
 {
   "STT": 59,
   "Name": "Trạm y tế Xã Yên Bằng , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Bằng , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.268471,
   "Latitude": 106.0111203
 },
 {
   "STT": 60,
   "Name": "Trạm y tế Xã Yên Lộc,  Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Lộc,  Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.2848772,
   "Latitude": 106.11399
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Xã Yên Bằng , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Bằng , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.268471,
   "Latitude": 106.0111203
 },
 {
   "STT": 62,
   "Name": " Trạm y tế Xã Yên Đồng , Huyện Ý Yên, Tỉnh Nam Định",
   "address": " Xã Yên Đồng , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.26408,
   "Latitude": 106.0581397
 },
 {
   "STT": 63,
   "Name": "Trạm y tế Xã Yên Khang , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Khang , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.2662772,
   "Latitude": 106.0346285
 },
 {
   "STT": 64,
   "Name": " Trạm y tế Xã Yên Nhân , Huyện Ý Yên, Tỉnh Nam Định",
   "address": " Xã Yên Nhân , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.2618792,
   "Latitude": 106.0816536
 },
 {
   "STT": 65,
   "Name": "Trạm y tế Xã Yên Trị , Huyện Ý Yên, Tỉnh Nam Định",
   "address": "Xã Yên Trị , Huyện Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.229368,
   "Latitude": 106.0581397
 },
 {
   "STT": 66,
   "Name": "Trạm y tế Xã Nghĩa Đồng , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Đồng , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.2931264,
   "Latitude": 106.1492721
 },
 {
   "STT": 67,
   "Name": "Trạm y tế Xã Nghĩa Thịnh , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Thịnh , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.2668232,
   "Latitude": 106.1522126
 },
 {
   "STT": 68,
   "Name": "Trạm y tế Xã Nghĩa Minh , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Minh , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.2611878,
   "Latitude": 106.1198699
 },
 {
   "STT": 69,
   "Name": "Trạm y tế Xã Nghĩa Thái , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Thái , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.2394151,
   "Latitude": 106.1669156
 },
 {
   "STT": 70,
   "Name": "Trạm y tế Xã Hoàng Nam , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Hoàng Nam , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.2354354,
   "Latitude": 106.1169299
 },
 {
   "STT": 71,
   "Name": "Trạm y tế Xã Nghĩa Châu , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Châu , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.2358437,
   "Latitude": 106.1433913
 },
 {
   "STT": 72,
   "Name": "Trạm y tế Xã Nghĩa Trung , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Trung , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.2162838,
   "Latitude": 106.1669156
 },
 {
   "STT": 73,
   "Name": "Trạm y tế Xã Nghĩa Sơn , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Sơn , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.1952157,
   "Latitude": 106.1757379
 },
 {
   "STT": 74,
   "Name": "Trạm y tế Xã Nghĩa Lạc , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Lạc , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.1142679,
   "Latitude": 106.1757379
 },
 {
   "STT": 75,
   "Name": "Trạm y tế Xã Nghĩa Hồng , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Hồng , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0948581,
   "Latitude": 106.1669156
 },
 {
   "STT": 76,
   "Name": "Trạm y tế Xã Nghĩa Phong , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Phong , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0889386,
   "Latitude": 106.199266
 },
 {
   "STT": 77,
   "Name": "Trạm y tế Xã Nghĩa Phú , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Phú , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0691162,
   "Latitude": 106.1639749
 },
 {
   "STT": 78,
   "Name": "Trạm y tế Xã Nghĩa Bình , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Bình , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0542545,
   "Latitude": 106.199266
 },
 {
   "STT": 79,
   "Name": "Thị trấn Quỹ Nhất , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Thị trấn Quỹ Nhất , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0570452,
   "Latitude": 106.1518013
 },
 {
   "STT": 80,
   "Name": "Trạm y tế Xã Nghĩa Tân , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Tân , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 81,
   "Name": "Trạm y tế Xã Nghĩa Hùng , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Hùng , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0377167,
   "Latitude": 106.1286901
 },
 {
   "STT": 82,
   "Name": "Trạm y tế Xã Nghĩa Lâm , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Lâm , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0040027,
   "Latitude": 106.1492721
 },
 {
   "STT": 83,
   "Name": "Trạm y tế Xã Nghĩa Thành , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Thành , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0318124,
   "Latitude": 106.1610343
 },
 {
   "STT": 84,
   "Name": "Trạm y tế Xã Nghĩa Thắng , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Thắng , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0206713,
   "Latitude": 106.1875016
 },
 {
   "STT": 85,
   "Name": "Trạm y tế Xã Nghĩa Lợi , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Lợi , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0081403,
   "Latitude": 106.1669156
 },
 {
   "STT": 86,
   "Name": "Trạm y tế Xã Nghĩa Hải , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Hải , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0041192,
   "Latitude": 106.1169299
 },
 {
   "STT": 87,
   "Name": "Trạm y tế Xã Nghĩa Phúc , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nghĩa Phúc , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 19.9949342,
   "Latitude": 106.1845606
 },
 {
   "STT": 88,
   "Name": "Trạm y tế Xã Nam Điền , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "address": "Xã Nam Điền , Huyện Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 19.9776105,
   "Latitude": 106.1381064
 },
 {
   "STT": 89,
   "Name": "Trạm y tế Xã Nam Mỹ , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Mỹ , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.4032351,
   "Latitude": 106.2080897
 },
 {
   "STT": 90,
   "Name": "Trạm y tế Xã Điền Xá , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Điền Xá , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3952155,
   "Latitude": 106.2316216
 },
 {
   "STT": 91,
   "Name": "Trạm y tế Xã Nghĩa An , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nghĩa An , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3698219,
   "Latitude": 106.1639749
 },
 {
   "STT": 92,
   "Name": "Trạm y tế Xã Nam Thắng , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Thắng , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3944654,
   "Latitude": 106.2698669
 },
 {
   "STT": 93,
   "Name": "Trạm y tế Xã Nam Toàn , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Toàn , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3864433,
   "Latitude": 106.2022072
 },
 {
   "STT": 94,
   "Name": "Trạm y tế Xã Hồng Quang , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Hồng Quang , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3679773,
   "Latitude": 106.2139724
 },
 {
   "STT": 95,
   "Name": "Trạm y tế Xã Tân Thịnh , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Tân Thịnh , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3735776,
   "Latitude": 106.2463305
 },
 {
   "STT": 96,
   "Name": "Trạm y tế Xã Nam Cường , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Cường , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3625495,
   "Latitude": 106.1930051
 },
 {
   "STT": 97,
   "Name": "Trạm y tế Xã Nam Hồng , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Hồng , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3420547,
   "Latitude": 106.2433886
 },
 {
   "STT": 98,
   "Name": "Trạm y tế Xã Nam Hùng , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Hùng , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3474607,
   "Latitude": 106.2167419
 },
 {
   "STT": 99,
   "Name": "Trạm y tế Xã Nam Hoa , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Hoa , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.33375,
   "Latitude": 106.2242676
 },
 {
   "STT": 100,
   "Name": "Trạm y tế Xã Nam Dương , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Dương , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3239415,
   "Latitude": 106.1904426
 },
 {
   "STT": 101,
   "Name": "Trạm y tế Xã Nam Thanh , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Thanh , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3235913,
   "Latitude": 106.2551563
 },
 {
   "STT": 102,
   "Name": "Trạm y tế Xã Nam Lợi , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Lợi , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.3053082,
   "Latitude": 106.2345633
 },
 {
   "STT": 103,
   "Name": "Trạm y tế Xã Bình Minh , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Bình Minh , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.316489,
   "Latitude": 106.2080897
 },
 {
   "STT": 104,
   "Name": "Trạm y tế Xã Đồng Sơn , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Đồng Sơn , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.2877399,
   "Latitude": 106.1757379
 },
 {
   "STT": 105,
   "Name": "Trạm y tế Xã Nam Tiến , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Tiến , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.2959716,
   "Latitude": 106.2110311
 },
 {
   "STT": 106,
   "Name": "Trạm y tế Xã Nam Hải , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Hải , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.2821804,
   "Latitude": 106.2345633
 },
 {
   "STT": 107,
   "Name": "Trạm y tế Xã Nam Thái , Huyện Nam Trực, Tỉnh Nam Định",
   "address": "Xã Nam Thái , Huyện Nam Trực, Tỉnh Nam Định",
   "Longtitude": 20.2718951,
   "Latitude": 106.1904426
 },
 {
   "STT": 108,
   "Name": "Trạm y tế Xã Phương Định , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Phương Định , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.310163,
   "Latitude": 106.3051766
 },
 {
   "STT": 109,
   "Name": "Trạm y tế Xã Trực Chính , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Chính , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.3312364,
   "Latitude": 106.2963486
 },
 {
   "STT": 110,
   "Name": "Trạm y tế Xã Trung Đông , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trung Đông , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.298789,
   "Latitude": 106.2728091
 },
 {
   "STT": 111,
   "Name": "Trạm y tế Xã Liêm Hải , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Liêm Hải , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.2870411,
   "Latitude": 106.3051766
 },
 {
   "STT": 112,
   "Name": "Trạm y tế Xã Trực Tuấn , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Tuấn , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 113,
   "Name": "Trạm y tế Xã Việt Hùng , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Việt Hùng , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.26392,
   "Latitude": 106.3051766
 },
 {
   "STT": 114,
   "Name": "Trạm y tế Xã Trực Đạo , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Đạo , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.2651636,
   "Latitude": 106.2557838
 },
 {
   "STT": 115,
   "Name": "Trạm y tế Xã Trực Hưng , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Hưng , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.1793782,
   "Latitude": 106.1904426
 },
 {
   "STT": 116,
   "Name": "Trạm y tế Xã Trực Nội , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Nội , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.254634,
   "Latitude": 106.2266356
 },
 {
   "STT": 117,
   "Name": "Thị trấn Cát Thành , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Thị trấn Cát Thành , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.2557105,
   "Latitude": 106.2698669
 },
 {
   "STT": 118,
   "Name": "Trạm y tế Xã Trực Thanh , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Thanh , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.2500104,
   "Latitude": 106.2461371
 },
 {
   "STT": 119,
   "Name": "Trạm y tế Xã Trực Khang , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Khang , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.24821,
   "Latitude": 106.1963248
 },
 {
   "STT": 120,
   "Name": "Trạm y tế Xã Trực Thuận , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Thuận , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.2478476,
   "Latitude": 106.1822609
 },
 {
   "STT": 121,
   "Name": "Trạm y tế Xã Trực Mỹ , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Mỹ , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.229199,
   "Latitude": 106.2139724
 },
 {
   "STT": 122,
   "Name": "Trạm y tế Xã Trực Đại , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Đại , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.2154146,
   "Latitude": 106.237505
 },
 {
   "STT": 123,
   "Name": "Trạm y tế Xã Trực Cường , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Cường , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.1945089,
   "Latitude": 106.2139724
 },
 {
   "STT": 124,
   "Name": "Trạm y tế Xã Trực Thái , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Thái , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.1876187,
   "Latitude": 106.2257384
 },
 {
   "STT": 125,
   "Name": "Trạm y tế Xã Trực Hùng , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Hùng , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.1793782,
   "Latitude": 106.1904426
 },
 {
   "STT": 126,
   "Name": "Trạm y tế Xã Trực Thắng , Huyện Trực Ninh, Tỉnh Nam Định",
   "address": "Xã Trực Thắng , Huyện Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.1749475,
   "Latitude": 106.237505
 },
 {
   "STT": 127,
   "Name": "Trạm y tế Xã Xuân Châu , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Châu , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3619881,
   "Latitude": 106.3375493
 },
 {
   "STT": 128,
   "Name": "Trạm y tế Xã Xuân Hồng , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Hồng , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3289924,
   "Latitude": 106.3198908
 },
 {
   "STT": 129,
   "Name": "Trạm y tế Xã Xuân Thành , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Thành , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3487388,
   "Latitude": 106.3552092
 },
 {
   "STT": 130,
   "Name": "Trạm y tế Xã Xuân Thượng , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Thượng , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3394295,
   "Latitude": 106.3316629
 },
 {
   "STT": 131,
   "Name": "Trạm y tế Xã Xuân Phong , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Phong , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3250571,
   "Latitude": 106.3610962
 },
 {
   "STT": 132,
   "Name": "Trạm y tế Xã Xuân Đài , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Đài , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3170246,
   "Latitude": 106.3846459
 },
 {
   "STT": 133,
   "Name": "Trạm y tế Xã Xuân Tân , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Tân , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3242827,
   "Latitude": 106.3993658
 },
 {
   "STT": 134,
   "Name": "Trạm y tế Xã Xuân Thủy , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Thủy , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.326745,
   "Latitude": 106.3434358
 },
 {
   "STT": 135,
   "Name": "Trạm y tế Xã Xuân Ngọc , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Ngọc , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3041875,
   "Latitude": 106.3375493
 },
 {
   "STT": 136,
   "Name": "Trạm y tế Xã Xuân Bắc , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Bắc , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3082813,
   "Latitude": 106.3552092
 },
 {
   "STT": 137,
   "Name": "Trạm y tế Xã Xuân Phương , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Phương , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.301377,
   "Latitude": 106.3669834
 },
 {
   "STT": 138,
   "Name": "Trạm y tế Xã Thọ Nghiệp , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Thọ Nghiệp , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.2907385,
   "Latitude": 106.3875898
 },
 {
   "STT": 139,
   "Name": "Trạm y tế Xã Xuân Phú , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Phú , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.2974333,
   "Latitude": 106.4081983
 },
 {
   "STT": 140,
   "Name": "Trạm y tế Xã Xuân Trung , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Trung , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.2903811,
   "Latitude": 106.3610962
 },
 {
   "STT": 141,
   "Name": "Trạm y tế Xã Xuân Vinh , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Vinh , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.2698733,
   "Latitude": 106.3640398
 },
 {
   "STT": 142,
   "Name": "Trạm y tế Xã Xuân Kiên , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Kiên , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.2585109,
   "Latitude": 106.3316629
 },
 {
   "STT": 143,
   "Name": "Trạm y tế Xã Xuân Tiến , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Tiến , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.2689486,
   "Latitude": 106.3434358
 },
 {
   "STT": 144,
   "Name": "Trạm y tế Xã Xuân Ninh , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Ninh , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.2396816,
   "Latitude": 106.3169479
 },
 {
   "STT": 145,
   "Name": "Trạm y tế Xã Xuân Hòa , Huyện Xuân Trường, Tỉnh Nam Định",
   "address": "Xã Xuân Hòa , Huyện Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.25105,
   "Latitude": 106.3493224
 },
 {
   "STT": 146,
   "Name": "Trạm y tế Xã Giao Hương , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Hương , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.3013988,
   "Latitude": 106.5171626
 },
 {
   "STT": 147,
   "Name": "Trạm y tế Xã Hồng Thuận , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Hồng Thuận , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2816996,
   "Latitude": 106.4818167
 },
 {
   "STT": 148,
   "Name": "Trạm y tế Xã Giao Thiện , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Thiện , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.232755,
   "Latitude": 106.5701927
 },
 {
   "STT": 149,
   "Name": "Trạm y tế Xã Giao Thanh , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Thanh , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2762595,
   "Latitude": 106.5083256
 },
 {
   "STT": 150,
   "Name": "Trạm y tế Xã Hoành Sơn , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Hoành Sơn , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2784066,
   "Latitude": 106.4258643
 },
 {
   "STT": 151,
   "Name": "Trạm y tế Xã Bình Hòa , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Bình Hòa , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2692394,
   "Latitude": 106.4612009
 },
 {
   "STT": 152,
   "Name": "Trạm y tế Xã Giao Tiến , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Tiến , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2702322,
   "Latitude": 106.3905338
 },
 {
   "STT": 153,
   "Name": "Trạm y tế Xã Giao Hà , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Hà , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.258251,
   "Latitude": 106.4553111
 },
 {
   "STT": 154,
   "Name": "Trạm y tế Xã Giao Nhân , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Nhân , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2599453,
   "Latitude": 106.4376425
 },
 {
   "STT": 155,
   "Name": "Trạm y tế Xã Giao An , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao An , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.254058,
   "Latitude": 106.528946
 },
 {
   "STT": 156,
   "Name": "Trạm y tế Xã Giao Lạc , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Lạc , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2205413,
   "Latitude": 106.5171626
 },
 {
   "STT": 157,
   "Name": "Trạm y tế Xã Giao Châu , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Châu , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2526894,
   "Latitude": 106.4229199
 },
 {
   "STT": 158,
   "Name": "Trạm y tế Xã Giao Tân , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Tân , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2534596,
   "Latitude": 106.3846459
 },
 {
   "STT": 159,
   "Name": "Trạm y tế Xã Giao Yến , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Yến , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2402162,
   "Latitude": 106.40231
 },
 {
   "STT": 160,
   "Name": "Trạm y tế Xã Giao Xuân , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Xuân , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2112548,
   "Latitude": 106.493598
 },
 {
   "STT": 161,
   "Name": "Trạm y tế Xã Giao Thịnh , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Thịnh , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2262522,
   "Latitude": 106.3669834
 },
 {
   "STT": 162,
   "Name": " Trạm y tế Xã Giao Hải , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": " Xã Giao Hải , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2224617,
   "Latitude": 106.4670909
 },
 {
   "STT": 163,
   "Name": "Trạm y tế Xã Bạch Long , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Bạch Long , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2191505,
   "Latitude": 106.4111425
 },
 {
   "STT": 164,
   "Name": "Trạm y tế Xã Giao Long , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Long , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2273253,
   "Latitude": 106.4464766
 },
 {
   "STT": 165,
   "Name": "Trạm y tế Xã Giao Phong , Huyện Giao Thủy, Tỉnh Nam Định",
   "address": "Xã Giao Phong , Huyện Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.212451,
   "Latitude": 106.3905338
 },
 {
   "STT": 166,
   "Name": " Trạm y tế Xã Hải Nam , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": " Xã Hải Nam , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.2284941,
   "Latitude": 106.3434358
 },
 {
   "STT": 167,
   "Name": "Trạm y tế Xã Hải Trung , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Trung , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.2225231,
   "Latitude": 106.2845785
 },
 {
   "STT": 168,
   "Name": "Trạm y tế Xã Hải Vân , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Vân , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.2296137,
   "Latitude": 106.3316629
 },
 {
   "STT": 169,
   "Name": "Trạm y tế Xã Hải Minh , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Minh , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.2337015,
   "Latitude": 106.2580983
 },
 {
   "STT": 170,
   "Name": "Trạm y tế Xã Hải Anh , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Anh , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 171,
   "Name": "Trạm y tế Xã Hải Hưng , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Hưng , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.2087316,
   "Latitude": 106.3081194
 },
 {
   "STT": 172,
   "Name": "Trạm y tế Xã Hải Bắc , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Bắc , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.2972495
 },
 {
   "STT": 173,
   "Name": "Trạm y tế Xã Hải Phúc , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Phúc , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.2027686,
   "Latitude": 106.3404925
 },
 {
   "STT": 174,
   "Name": "Trạm y tế Xã Hải Thanh , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Thanh , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1830026,
   "Latitude": 106.3051766
 },
 {
   "STT": 175,
   "Name": "Trạm y tế Xã Hải Hà , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Hà , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1960561,
   "Latitude": 106.3198908
 },
 {
   "STT": 176,
   "Name": "Trạm y tế Xã Hải Long , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Long , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1837317,
   "Latitude": 106.2669247
 },
 {
   "STT": 177,
   "Name": "Trạm y tế Xã Hải Phương , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Phương , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1768392,
   "Latitude": 106.2786937
 },
 {
   "STT": 178,
   "Name": "Trạm y tế Xã Hải Đường , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Đường , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1680571,
   "Latitude": 106.2492724
 },
 {
   "STT": 179,
   "Name": "Trạm y tế Xã Hải Lộc,  Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Lộc,  Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1770436,
   "Latitude": 106.3375493
 },
 {
   "STT": 180,
   "Name": "Trạm y tế Xã Hải Quang , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Quang , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1682755,
   "Latitude": 106.3081194
 },
 {
   "STT": 181,
   "Name": "Trạm y tế Xã Hải Đông , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Đông , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1544872,
   "Latitude": 106.3316629
 },
 {
   "STT": 182,
   "Name": "Trạm y tế Xã Hải Sơn , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Sơn , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.143826,
   "Latitude": 106.2610404
 },
 {
   "STT": 183,
   "Name": "Trạm y tế Xã Hải Tân , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Tân , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1484956,
   "Latitude": 106.2728091
 },
 {
   "STT": 184,
   "Name": "Trạm y tế Xã Hải Toàn , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Toàn , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1430302,
   "Latitude": 106.2080897
 },
 {
   "STT": 185,
   "Name": "Trạm y tế Xã Hải Phong , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Phong , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1413708,
   "Latitude": 106.2257384
 },
 {
   "STT": 186,
   "Name": "Trạm y tế Xã Hải An , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải An , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1441355,
   "Latitude": 106.1963248
 },
 {
   "STT": 187,
   "Name": "Trạm y tế Xã Hải Tây , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Tây , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1378811,
   "Latitude": 106.293406
 },
 {
   "STT": 188,
   "Name": "Trạm y tế Xã Hải Lý , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Lý , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.127822,
   "Latitude": 106.3081194
 },
 {
   "STT": 189,
   "Name": "Trạm y tế Xã Hải Phú , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Phú , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1171417,
   "Latitude": 106.237505
 },
 {
   "STT": 190,
   "Name": "Trạm y tế Xã Hải Giang , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Giang , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1248769,
   "Latitude": 106.203934
 },
 {
   "STT": 191,
   "Name": "Trạm y tế Xã Hải Cường , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Cường , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1097007,
   "Latitude": 106.2551563
 },
 {
   "STT": 192,
   "Name": "Trạm y tế Xã Hải Ninh , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Ninh , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1020113,
   "Latitude": 106.2139724
 },
 {
   "STT": 193,
   "Name": "Trạm y tế Xã Hải Chính , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Chính , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1121525,
   "Latitude": 106.2904635
 },
 {
   "STT": 194,
   "Name": "Trạm y tế Xã Hải Xuân , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Xuân , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1054255,
   "Latitude": 106.2698669
 },
 {
   "STT": 195,
   "Name": "Trạm y tế Xã Hải Châu , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Châu , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.0777854,
   "Latitude": 106.2257384
 },
 {
   "STT": 196,
   "Name": "Trạm y tế Xã Hải Triều , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Triều , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.0838913,
   "Latitude": 106.2683958
 },
 {
   "STT": 197,
   "Name": "Trạm y tế Xã Hải Hòa , Huyện Hải Hậu, Tỉnh Nam Định",
   "address": "Xã Hải Hòa , Huyện Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.0729622,
   "Latitude": 106.2463305
 }
];
