var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Ban Bảo vệ CSSK cán bộ",
   "icon": "maps/map-images/ttkn.png",
   "address": "TP Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4388225,
   "Latitude": 106.1621053
 },
 {
   "STT": 2,
   "Name": "Trung tâm phòng chống HIV/AIDS tỉnh Nam Định",
   "icon": "maps/map-images/hiv.png",
   "address": "Số 162, Trần Nhật Duật, phường Trần Tế Xương, TP Nam Định",
   "Longtitude": 20.4377604,
   "Latitude": 106.1862033
 },
 {
   "STT": 1,
   "Name": "Trung tâm Y tế huyện Vụ Bản",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "TL 486, xã Cộng Hoà, huyện Vụ Bản, tỉnh Nam Định",
   "Longtitude": 20.3748079,
   "Latitude": 106.0676835
 },
 {
   "STT": 2,
   "Name": "Trung tâm Y tế huyện Ý Yên",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Đường 57, thị trấn Lâm, huyện Ý Yên, tỉnh Nam Định",
   "Longtitude": 20.324752,
   "Latitude": 106.01666
 },
 {
   "STT": 3,
   "Name": "Trung tâm Y tế huyện Trực Ninh",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Hữu Nghị, thị trấn Cổ Lễ, huyện Trực Ninh, tỉnh Nam Định",
   "Longtitude": 20.3203941,
   "Latitude": 106.2684597
 },
 {
   "STT": 4,
   "Name": "Trung tâm Y tế huyện Mỹ Lộc",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "QL21A, thị trấn Mỹ Lộc, huyện Mỹ Lộc, tỉnh Nam Định",
   "Longtitude": 20.4399058,
   "Latitude": 106.1084158
 },
 {
   "STT": 5,
   "Name": "Trung tâm Y tế huyện Xuân Trường",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "xã Xuân Ngọc, huyện Xuân Trường, tỉnh Nam Định",
   "Longtitude": 20.2932342,
   "Latitude": 106.3329738
 },
 {
   "STT": 6,
   "Name": "Trung tâm Y tế huyện Giao Thủy",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Tỉnh Lộ 490, thị trấn Ngô Đồng, huyện Giao Thuỷ, tỉnh Nam Định",
   "Longtitude": 20.2856667,
   "Latitude": 106.4444357
 },
 {
   "STT": 7,
   "Name": "Trung tâm Y tế huyện Nghĩa Hưng",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "ĐT490C, thị trấn Liễu Đề, huyện Nghĩa Hưng, tỉnh Nam Định",
   "Longtitude": 20.2163195,
   "Latitude": 106.1828774
 },
 {
   "STT": 8,
   "Name": "Trung tâm Y tế huyện Nam Trực",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Tỉnh Lộ 490, xã Nam Giang, huyện Nam Trực, tỉnh Nam Định",
   "Longtitude": 20.327384,
   "Latitude": 106.1894767
 },
 {
   "STT": 9,
   "Name": "Trung tâm Y tế thành phố Nam Định",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "64 Nguyễn Du, phường Nguyễn Du, thành phố Nam Định, tỉnh Nam Định",
   "Longtitude": 20.4295417,
   "Latitude": 106.1757083
 },
 {
   "STT": 10,
   "Name": "Trung tâm Y tế huyện Hải Hậu",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Quốc Lộ 21B, thị trấn Yên Định, huyện Hải Hậu, tỉnh Nam Định",
   "Longtitude": 20.1950346,
   "Latitude": 106.2923866
 }
];