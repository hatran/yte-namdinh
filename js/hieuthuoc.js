var data2 = [
 {
   "STT": 1,
   "Name": "Công ty dược TNHH Hưng Bình Hường",
   "address": "386, Đường Hoàng Văn Thụ, Phường Quang Trung, Vị Hoàng, Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4313939,
   "Latitude": 106.1707973
 },
 {
   "STT": 2,
   "Name": "Quầy thuốc số 6 -DS. Trần Kim Dung",
   "address": "125 Điện Biên, Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4293788,
   "Latitude": 106.1639923
 },
 {
   "STT": 3,
   "Name": "Nhà thuốc Bình Hường 2 ",
   "address": "134 Hàng Sắt, Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4296834,
   "Latitude": 106.175039
 },
 {
   "STT": 4,
   "Name": "Quầy thuốc Mỹ Nhung",
   "address": "48 Mạc Thị Bưởi, Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4323136,
   "Latitude": 106.1763711
 },
 {
   "STT": 5,
   "Name": "Quầy thuốc công ty CP DP Nam Định",
   "address": "66 Trần Phú, Phường Trần Hương Đạo, Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4255846,
   "Latitude": 106.1734814
 },
 {
   "STT": 6,
   "Name": "Nhà thuốc Mỹ Nhung 2",
   "address": "384 Điện Biên, Phường Lộc Hòa,  Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4307184,
   "Latitude": 106.1593843
 },
 {
   "STT": 7,
   "Name": "Nhà thuốc Tùng Vân",
   "address": "151 Trần Nhật Duật, Tỉnh Nam Định",
   "Longtitude": 20.4361873,
   "Latitude": 106.1845914
 },
 {
   "STT": 8,
   "Name": "Quầy thuốc Bán lẻ Đỗ Quyên",
   "address": "74 Tô Hiệu, Tỉnh Nam Định",
   "Longtitude": 20.4207138,
   "Latitude": 106.1721402
 },
 {
   "STT": 9,
   "Name": "Công ty trách nhiệm hữu hạn Sơn Trường",
   "address": "64 Thành Chung",
   "Longtitude": 20.4288875,
   "Latitude": 106.1670259
 },
 {
   "STT": 10,
   "Name": "Quầy thuốc công ty dược phẩm Hoa Sen ",
   "address": "43 Phù Nghĩa, Tỉnh Nam Định",
   "Longtitude": 20.4410364,
   "Latitude": 106.1831035
 },
 {
   "STT": 11,
   "Name": "vũ thanh nga qt 01",
   "address": "Số 1 Trần Đăng Ninh",
   "Longtitude": 20.421903,
   "Latitude": 106.1626534
 },
 {
   "STT": 12,
   "Name": "Quầy thuốc Phong Vân ",
   "address": "209 Phù Nghĩa, Hạ Long, Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4447271,
   "Latitude": 106.1817229
 },
 {
   "STT": 13,
   "Name": "Quầy thuốc công ty dược phẩm Nam Hà",
   "address": "270 Hàn Thuyên",
   "Longtitude": 20.4390427,
   "Latitude": 106.1838596
 },
 {
   "STT": 14,
   "Name": "Quầy thuốc tân dược Soái Dung",
   "address": "949 Trần Huy Liệu, Tỉnh Nam Định",
   "Longtitude": 20.4105233,
   "Latitude": 106.1456488
 },
 {
   "STT": 15,
   "Name": "Công ty TNHH Tuyên Thạc",
   "address": "10A/29 Văn Cao, Năng Tĩnh, Tỉnh Nam Định",
   "Longtitude": 20.4196228,
   "Latitude": 106.1657387
 },
 {
   "STT": 16,
   "Name": "Quầy thuốc công ty dược phẩm Nam Hà",
   "address": "Phù Nghĩa, Lộc Vượng, Thành Phố Nam Định, Tỉnh Nam Định, Việt Nam",
   "Longtitude": 20.449611,
   "Latitude": 106.1798604
 },
 {
   "STT": 17,
   "Name": "Nhà thuốc Mỹ Nhung 3",
   "address": "218 Trần Hưng Đạo, Tỉnh Nam Định",
   "Longtitude": 20.4274652,
   "Latitude": 106.1726796
 },
 {
   "STT": 18,
   "Name": "Quầy thuốc công ty dược phẩm Nam Định ",
   "address": "124 Nguyễn Văn Trỗi, TP Nam Định",
   "Longtitude": 20.4134861,
   "Latitude": 106.1665563
 },
 {
   "STT": 19,
   "Name": "Quầy thuốc Vân Trưởng",
   "address": "Mỹ Hà, Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4816155,
   "Latitude": 106.1032102
 },
 {
   "STT": 20,
   "Name": "Nhà thuốc Liên Hậu",
   "address": "199 Nguyễn Văn Trỗi, Tỉnh Nam Định",
   "Longtitude": 20.4159796,
   "Latitude": 106.1673043
 },
 {
   "STT": 21,
   "Name": "Quầy thuốc công ty dược phẩm Nam Định ",
   "address": "415 Hàn Thuyên, Tỉnh Nam Định",
   "Longtitude": 20.4401486,
   "Latitude": 106.1854019
 },
 {
   "STT": 22,
   "Name": "Nhà thuốc Mai Anh Dưỡng",
   "address": "760 Vũ Hữu Lợi, Tỉnh Nam Định",
   "Longtitude": 20.3992562,
   "Latitude": 106.1780755
 },
 {
   "STT": 23,
   "Name": "Nhà thuốc Kim Anh",
   "address": "Máy Chai, Ngô Quyền, Thành Phố Nam Định, Tỉnh Nam Định, Việt Nam",
   "Longtitude": 20.4206189,
   "Latitude": 106.1741925
 },
 {
   "STT": 24,
   "Name": "Quầy thuốc Trần Thanh Hồng",
   "address": "Lộc Hoà, Thành Phố Nam Định, Tỉnh Nam Định, Việt Nam",
   "Longtitude": 20.4347694,
   "Latitude": 106.1429256
 },
 {
   "STT": 25,
   "Name": "Quầy thuốc công ty dược phẩm Nam Hà",
   "address": "108 Bến Thóc, Tỉnh Nam Định",
   "Longtitude": 20.4220057,
   "Latitude": 106.1739558
 },
 {
   "STT": 26,
   "Name": "Nhà thuốc Tân Thịnh Phát",
   "address": "Số 369 Giải Phóng, Tỉnh Nam Định",
   "Longtitude": 20.4252282,
   "Latitude": 106.1567688
 },
 {
   "STT": 27,
   "Name": "Quầy thuốc công ty dược phẩm Nam Hà ",
   "address": "164 Trần Nhật Duật",
   "Longtitude": 20.4380836,
   "Latitude": 106.1839755
 },
 {
   "STT": 28,
   "Name": "Nhà thuốc Linh Ánh",
   "address": "188 Tức Mạ, Lộc Vượng, Tỉnh Nam Định",
   "Longtitude": 20.4439997,
   "Latitude": 106.1664249
 },
 {
   "STT": 29,
   "Name": "Nhà thuốc Tuân Lương",
   "address": "417 Hàn Thuyên, Trần Tế Xương, Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4401028,
   "Latitude": 106.1854899
 },
 {
   "STT": 30,
   "Name": "Nhà thuốc Sơn Bảo Anh",
   "address": "Số 63 Nguyễn Đức Thuận",
   "Longtitude": 20.4387571,
   "Latitude": 106.1768022
 },
 {
   "STT": 31,
   "Name": "Công ty dược phẩm Trâm Anh",
   "address": "Trần Đại Nghĩa, Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4392994,
   "Latitude": 106.1568537
 },
 {
   "STT": 32,
   "Name": "Nhà thuốc Mỹ Nhung 4",
   "address": "32 Trần Phú, Năng Tĩnh, TP Nam ĐỊnh",
   "Longtitude": 20.4207743,
   "Latitude": 106.1666527
 },
 {
   "STT": 33,
   "Name": "Quầy thuốc Hà Nam",
   "address": "92 Chu Văn, Hạ Long, Thành Phố Nam Định, Tỉnh Nam Định",
   "Longtitude": 20.4447331,
   "Latitude": 106.1826282
 },
 {
   "STT": 34,
   "Name": "Quầy thuốc Ngọc Hà",
   "address": "177 Hùng Vương, Tỉnh Nam Định",
   "Longtitude": 20.435564,
   "Latitude": 106.1798518
 },
 {
   "STT": 35,
   "Name": "Quầy thuốc bệnh viện Công An",
   "address": "162 Trần Đăng Ninh, Cửa Bắc, Thành Phố Nam Định, Tỉnh Nam Định, Việt Nam",
   "Longtitude": 20.4268519,
   "Latitude": 106.168372
 },
 {
   "STT": 36,
   "Name": "Quầy thuốc công ty DP Nam Hà",
   "address": "314 Trần Huy Liệu, Tỉnh Nam Định",
   "Longtitude": 20.4171561,
   "Latitude": 106.1570312
 },
 {
   "STT": 37,
   "Name": "Công ty TNHH và thương mại Hoàng Minh ",
   "address": "33 Hoàng Hoa Thám, Tỉnh Nam Định",
   "Longtitude": 20.4238557,
   "Latitude": 106.1694824
 },
 {
   "STT": 38,
   "Name": "Quầy thuốc công ty DP Nam Hà chu thống ",
   "address": "Cầu Họ, Mỹ Lộc, Tỉnh Nam Định",
   "Longtitude": 20.4662764,
   "Latitude": 106.0474554
 },
 {
   "STT": 39,
   "Name": "Quầy thuốc Huyền Đức",
   "address": "chợ hầu gần huyện Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3523478,
   "Latitude": 106.0844817
 },
 {
   "STT": 40,
   "Name": "Nhà thuốc Thu Hà",
   "address": "chợ Dần, Quang Trung, Vụ Bản, Tỉnh Nam Định",
   "Longtitude": 20.3524446,
   "Latitude": 106.0582167
 },
 {
   "STT": 41,
   "Name": "Quầy thuốc Việt An",
   "address": "TT Lâm, Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3213161,
   "Latitude": 105.9967653
 },
 {
   "STT": 42,
   "Name": "Quầy Thuốc Quốc Khánh",
   "address": "Cầu Bo, Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3677318,
   "Latitude": 105.9706699
 },
 {
   "STT": 43,
   "Name": "Quầy Thuốc Ngân Tú",
   "address": "Đường 57 Yên Chính, Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.342055,
   "Latitude": 105.9978671
 },
 {
   "STT": 44,
   "Name": "Quầy thuốc Thu Lộc",
   "address": "57A Thị Trấn Lâm, Ý Yên, Tỉnh Nam Định",
   "Longtitude": 20.3245929,
   "Latitude": 106.0141069
 },
 {
   "STT": 45,
   "Name": "Quầy thuốc Thảo Thuấn ",
   "address": "Đội 16, Nghĩa Bình, Tỉnh Nam Định",
   "Longtitude": 20.0566118,
   "Latitude": 106.1888937
 },
 {
   "STT": 46,
   "Name": "nhà thuốc Minh Nhài",
   "address": "Xóm 3, Thị trấn Qũy Nhất, Nghĩa Hưng, Tỉnh Nam Định",
   "Longtitude": 20.0538744,
   "Latitude": 106.1446432
 },
 {
   "STT": 47,
   "Name": "Quầy thuốc Như Luyến",
   "address": "53 Hải Đông, Nghĩa Trung, Nghĩa Hưng, Tỉnh Nam Định, Việt Nam",
   "Longtitude": 20.21996,
   "Latitude": 106.1767458
 },
 {
   "STT": 48,
   "Name": "Quầy thuốc tân dược",
   "address": "Đồng Sơn, Nam Trực, Tỉnh Nam Định, Việt Nam",
   "Longtitude": 20.2933122,
   "Latitude": 106.164969
 },
 {
   "STT": 49,
   "Name": "Công ty DP Thành Đức",
   "address": "K4 Cỗ Lễ, Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.3133246,
   "Latitude": 106.2635957
 },
 {
   "STT": 50,
   "Name": "Nhà thuốc Tâm Hùng",
   "address": "Chợ TT Cổ Lễ, Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.3233948,
   "Latitude": 106.2641806
 },
 {
   "STT": 51,
   "Name": "Quầy thuốc DP Thành Đức",
   "address": "ĐT488B, Trự Đạo, Trực Ninh, Tỉnh Nam Định, Việt Nam",
   "Longtitude": 20.2625571,
   "Latitude": 106.2535359
 },
 {
   "STT": 52,
   "Name": "Quầy thuốc Doanh Mơ",
   "address": "Chợ Đền, Trực Hưng, Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.2572092,
   "Latitude": 106.2048496
 },
 {
   "STT": 53,
   "Name": "Công ty DP Thành Đức",
   "address": "TT Cát Thành, Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.253187,
   "Latitude": 106.2565038
 },
 {
   "STT": 54,
   "Name": "Quầy thuốc công ty DP Thành đức",
   "address": "Xóm 1, Trực Thái, Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.2068054,
   "Latitude": 106.2228048
 },
 {
   "STT": 55,
   "Name": "Quầy thuốc công ty DP Nam Hà",
   "address": "Cổng Chợ Cổ Lễ, Trực Ninh, Tỉnh Nam Định",
   "Longtitude": 20.3149468,
   "Latitude": 106.2583707
 },
 {
   "STT": 56,
   "Name": "Quầy thuốc công ty DP Nam Hà",
   "address": "Số 8 TT Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.3151333,
   "Latitude": 106.2058392
 },
 {
   "STT": 57,
   "Name": "Quầy thuốc công ty dược phẩm thành đức ",
   "address": "Chợ Cầu Cụ, Xóm 7, Xuân Tiến, Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.2682747,
   "Latitude": 106.3231728
 },
 {
   "STT": 58,
   "Name": "Nhà thuốc Bạch Mai ",
   "address": "Xuân Tiến, Xuân Trường, Tỉnh Nam Định",
   "Longtitude": 20.2685024,
   "Latitude": 106.3228964
 },
 {
   "STT": 59,
   "Name": "Nhà thuốc Hưng Oanh",
   "address": "Chợ Ngô Đồng, Giao Thủy, Tỉnh Nam Định",
   "Longtitude": 20.2804979,
   "Latitude": 106.440515
 },
 {
   "STT": 60,
   "Name": "Nhà thuốc Mai Năm",
   "address": "Khu 3, TT Yên Định, Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1927635,
   "Latitude": 106.2861801
 },
 {
   "STT": 61,
   "Name": "Quầy thuốc Na Hùng",
   "address": "Khu 3, TT Yên Định, Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1927635,
   "Latitude": 106.2861801
 },
 {
   "STT": 62,
   "Name": "Nhà thuốc Trường Cổn ",
   "address": "226, Khu 3, TT Yên Định, Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.197231,
   "Latitude": 106.2938149
 },
 {
   "STT": 63,
   "Name": "Quầy thuốc Hồng Nhung",
   "address": "Xóm 1 Phú Lễ, Chợ Hải Châu, Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.0796514,
   "Latitude": 106.2071834
 },
 {
   "STT": 64,
   "Name": "Quầy thuốc tân dược Xuân Chiểu",
   "address": "Đội 6, Chợ Cầu, Hai Thanh, Tỉnh Nam Định",
   "Longtitude": 20.1868239,
   "Latitude": 106.2955512
 },
 {
   "STT": 65,
   "Name": "Nhà thuốc Khánh Lý",
   "address": "100 Khu 5, TT Cồn, Hải Hậu, Tỉnh Nam Định",
   "Longtitude": 20.1237254,
   "Latitude": 106.2750291
 }
];