var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng Khám Bệnh Nội Tổng Hợp - Trần Dung The",
   "address": "Đường Hữu Nghị, Nghĩa Lộc, tổ dân phố Đình Cựu, thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 21.0768878,
   "Latitude": 105.978379
 },
 {
   "STT": 2,
   "Name": "Phòng Khám Bệnh Nha Khoa Hà Nội - Đỗ Công Đoàn",
   "address": "Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 3,
   "Name": "Phòng Khám Bệnh Nội Tổng Hợp - Trần Quốc Phú",
   "address": "Xã Nam Lợi, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3053082,
   "Latitude": 106.2345633
 },
 {
   "STT": 4,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Chẩn Đoán Hình Ảnh - Vũ Đình Năng",
   "address": "Số 98, đường Hữu Nghị, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166427,
   "Latitude": 106.2698125
 },
 {
   "STT": 5,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội Tổng Hợp - Phan Đình Phô",
   "address": "Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 6,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Sản - Trần Thị Bích Thu",
   "address": "Số 120, đường Hữu Nghị, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166427,
   "Latitude": 106.2698125
 },
 {
   "STT": 7,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Mắt, Nội Soi Tai Mũi Họng - Bùi Thị Kim Anh",
   "address": "Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 8,
   "Name": "Phòng Khám Chuyên Khoa Nội - Phạm Văn Hoàng",
   "address": "Thôn Tân An, xã Liêm Hải, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2870411,
   "Latitude": 106.3051766
 },
 {
   "STT": 9,
   "Name": "Phòng Khám Chuyên Khoa Nội - Nguyễn Văn Đức",
   "address": "Xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.310163,
   "Latitude": 106.3051766
 },
 {
   "STT": 10,
   "Name": "Phòng Khám Chuyên Khoa Nội - Bùi Ngọc Cầm",
   "address": "Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 11,
   "Name": "Phòng Khám Chuyên Khoa Nội - Bùi Văn Lịch",
   "address": "Chợ Già, xã Trực Đạo, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2625521,
   "Latitude": 106.2557246
 },
 {
   "STT": 12,
   "Name": "Phòng Khám Chuyên Khoa Nội - Tống Xuân Dương",
   "address": "Đội 6, xã Trực Khang, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.24821,
   "Latitude": 106.1963248
 },
 {
   "STT": 13,
   "Name": "Phòng Khám Chuyên Khoa Nội - Vũ Như Hải",
   "address": "Xã Trực Thanh, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2500104,
   "Latitude": 106.2461371
 },
 {
   "STT": 14,
   "Name": "Phòng Khám Chuyên Khoa Nội - Trần Văn Nghĩa",
   "address": "Tổ DP Bắc Bình, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 11.2551834,
   "Latitude": 108.3801314
 },
 {
   "STT": 15,
   "Name": "Phòng Khám Chuyên Khoa Nội - Vũ Đình Tuất",
   "address": "Thị trấnCổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 16,
   "Name": "Phòng Khám Bệnh Cát Thành - Vũ Văn Sự",
   "address": "Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2557105,
   "Latitude": 106.2698669
 },
 {
   "STT": 17,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi - Chu Thành Cơ",
   "address": "Xã Trực Đaị, huyện Trực Ninh, , Nam Định",
   "Longtitude": 20.2154146,
   "Latitude": 106.237505
 },
 {
   "STT": 18,
   "Name": "Phòng Khám Chuyên Khoa Nội - Nguyễn Văn Sinh",
   "address": "Xã Trực Thái, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1876187,
   "Latitude": 106.2257384
 },
 {
   "STT": 19,
   "Name": "Phòng Khám Chuyên Khoa Nội - Lương Ngọc Liễn",
   "address": "Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 20,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản - Phạm Thị Nhài",
   "address": "Xã Trực Phú, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1840534,
   "Latitude": 106.2022072
 },
 {
   "STT": 21,
   "Name": "Phòng Khám Chuyên Khoa Nội - Hoàng Trung Văn",
   "address": "Xã Trực Cường, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1945089,
   "Latitude": 106.2139724
 },
 {
   "STT": 22,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Vũ Đức nguyện",
   "address": "Xóm 8, xã Hải Phương, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1877513,
   "Latitude": 106.28936
 },
 {
   "STT": 23,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Nguyễn Xuân Tăng",
   "address": "Tổ dân phố Cao An, Thị trấn Cồn, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.123793,
   "Latitude": 106.2742802
 },
 {
   "STT": 24,
   "Name": "Phòng Khám Nha Khoa Đông Hải - Trần văn Thiệp",
   "address": "Xóm Hoàng Thức, xã Phú Hải, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1171417,
   "Latitude": 106.237505
 },
 {
   "STT": 25,
   "Name": "Phòng Khám Chuyên Khoa Nội Minh Ngọc - Phạm Minh Ngọc",
   "address": "Tổ dân phố 15, Thị trấn Thịnh Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.040491,
   "Latitude": 106.2227968
 },
 {
   "STT": 26,
   "Name": "Phòng Khám Chuyên Khoa Nội (Tim Mạch) 219 - Đinh Quang Huy",
   "address": "Số 219, khu 4, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1950239,
   "Latitude": 106.2926704
 },
 {
   "STT": 27,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Nguyễn Thị Xuân",
   "address": "Xã Hải Hà, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1960561,
   "Latitude": 106.3198908
 },
 {
   "STT": 28,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Lưu Văn Lượng",
   "address": "Khu 4B, Thị trấn Cồn, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1187156,
   "Latitude": 106.2783259
 },
 {
   "STT": 29,
   "Name": "Phòng Khám Chuyên Khoa Mắt - Cao Thị Hồng",
   "address": "Số 422, đường Giải Phóng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4334134,
   "Latitude": 106.1630425
 },
 {
   "STT": 30,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Lưu Công Chủ",
   "address": "Xóm 2, xã Giao Yến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2396176,
   "Latitude": 106.4538386
 },
 {
   "STT": 31,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội Tuyết Huỳnh - Nguyễn Bội Huỳnh",
   "address": "Khu 5, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2884171,
   "Latitude": 106.43874
 },
 {
   "STT": 32,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Doãn Đức Thiện",
   "address": "Xã Hoành Sơn, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2784066,
   "Latitude": 106.4258643
 },
 {
   "STT": 33,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Lương Công Thành",
   "address": "Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2599453,
   "Latitude": 106.4376425
 },
 {
   "STT": 34,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Phạm Đức Thịnh",
   "address": "Xóm 18, xã Giao Thiện, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2508925,
   "Latitude": 106.4943343
 },
 {
   "STT": 35,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Hoàng Xuân Sỹ",
   "address": "Khu 5B Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.287773,
   "Latitude": 106.4380534
 },
 {
   "STT": 36,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Nguyễn Thành Vương",
   "address": "Khu 4A, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.287129,
   "Latitude": 106.4473231
 },
 {
   "STT": 37,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Trần Văn Bảng",
   "address": "Thị trấn Quất Lâm, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.188973,
   "Latitude": 106.3640398
 },
 {
   "STT": 38,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn Thế Hiệp",
   "address": "Xã Hồng Thuận, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2816996,
   "Latitude": 106.4818167
 },
 {
   "STT": 39,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Bùi Hồng Đoan",
   "address": "Khu 4A, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.287129,
   "Latitude": 106.4473231
 },
 {
   "STT": 40,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Phan Đình Nguyên",
   "address": "Xã Giao Tiến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2702322,
   "Latitude": 106.3905338
 },
 {
   "STT": 41,
   "Name": "Phòng Khám Chuyên Khoa Nội - Bùi Văn Dung",
   "address": "Xã Giao Thiện, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.232755,
   "Latitude": 106.5701927
 },
 {
   "STT": 42,
   "Name": "Phòng Khám Chuyên Khoa Nội - Phạm Văn Tuyên",
   "address": "Xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.212451,
   "Latitude": 106.3905338
 },
 {
   "STT": 43,
   "Name": "Phòng Khám Chuyên Khoa Sản - Trần Thị Thơm",
   "address": "Khu 4, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.287129,
   "Latitude": 106.4473231
 },
 {
   "STT": 44,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Đoàn Hồng Trường",
   "address": "Khu 4B, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2903492,
   "Latitude": 106.4408
 },
 {
   "STT": 45,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Vũ Thị Liễu",
   "address": "Xã Giao Tiến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2702322,
   "Latitude": 106.3905338
 },
 {
   "STT": 46,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Cao Thị Sim",
   "address": "Xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.212451,
   "Latitude": 106.3905338
 },
 {
   "STT": 47,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Vũ Quang Ánh",
   "address": "Xóm 9, Giao Thịnh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2266527,
   "Latitude": 106.4685634
 },
 {
   "STT": 48,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - Hoàng Khắc Được",
   "address": "Khu 4A, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.287129,
   "Latitude": 106.4473231
 },
 {
   "STT": 49,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Mai Ngọc Vinh",
   "address": "Xã Xuân Ninh, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2396816,
   "Latitude": 106.3169479
 },
 {
   "STT": 50,
   "Name": "Phòng Khám Nha Khoa Hồng Minh - Phạm Thị Hồng Minh",
   "address": "Xóm 14 xã Xuân Kiên, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2660658,
   "Latitude": 106.3299641
 },
 {
   "STT": 51,
   "Name": "Phòng Khám Chẩn Đoán Hình Ảnh - Phạm Văn Lý",
   "address": "Xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3289924,
   "Latitude": 106.3198908
 },
 {
   "STT": 52,
   "Name": "Phòng Khám Chuyên Khoa Nội - Nguyễn Hải Nam",
   "address": "Xã Xuân Hồng, huyện Xuân Trường , Nam Định",
   "Longtitude": 20.3289924,
   "Latitude": 106.3198908
 },
 {
   "STT": 53,
   "Name": "Phòng Khám Chuyên Khoa Sản - Phạm Thị Bình",
   "address": "Tổ 18, Thị trấn Xuân Trường, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.289987,
   "Latitude": 106.3339072
 },
 {
   "STT": 54,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Trần Phú Phòng",
   "address": "Tổ 18, Thị trấn Xuân Trường, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.289987,
   "Latitude": 106.3339072
 },
 {
   "STT": 55,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Mai Ngọc Chương",
   "address": "Lạc Quần, xã Xuân Ninh, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2664358,
   "Latitude": 106.3243053
 },
 {
   "STT": 56,
   "Name": "Phòng Khám Chuyên Khoa Nội - Phạm Văn Tú",
   "address": "Tổ 18, Thị trấn Xuân Trường, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.289987,
   "Latitude": 106.3339072
 },
 {
   "STT": 57,
   "Name": "Phòng Khám Chuyên Khoa Nội - Vũ Quốc Trình",
   "address": "Hành Thiện, xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3357967,
   "Latitude": 106.3243053
 },
 {
   "STT": 58,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Thái Bình - Nguyễn Đình Bình",
   "address": "Tổ DP 11, Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3424845,
   "Latitude": 106.1812536
 },
 {
   "STT": 59,
   "Name": "Phòng Khám Chuyên Khoa Nội - Ngô Ngọc Đạt",
   "address": "Thôn Bái Dương, xã Nam Dương, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3239415,
   "Latitude": 106.1904426
 },
 {
   "STT": 60,
   "Name": "Phòng Khám Chuyên Khoa Ngoại - Lương Ngọc Liễn",
   "address": "Thôn An Nông, xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2986294,
   "Latitude": 106.2196274
 },
 {
   "STT": 61,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Phạm Văn Kiểm",
   "address": "Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.342404,
   "Latitude": 106.1786787
 },
 {
   "STT": 62,
   "Name": "Phòng Khám Chuyên Khoa Nội Thúy Sơn - Trần Ngọc Sơn",
   "address": " Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.342404,
   "Latitude": 106.1786787
 },
 {
   "STT": 63,
   "Name": "Phòng Khám Chuyên Khoa Nội Phúc Minh - Trần Thái Sơn",
   "address": "Cổ Giả, xã Nam Hùng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3405059,
   "Latitude": 106.2124075
 },
 {
   "STT": 64,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Chẩn Đoán Hình Ảnh - Vũ Hữu Tân",
   "address": "Xóm C, xã Thành Lợi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3703959,
   "Latitude": 106.1418046
 },
 {
   "STT": 65,
   "Name": "Phòng Khám Chuyên Khoa Nội - Trần Văn Niên",
   "address": "Thôn Rư Duệ, Xã Tam Thanh, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3199589,
   "Latitude": 106.0819834
 },
 {
   "STT": 66,
   "Name": "Phòng Khám Chuyên Khoa Chẩn Đoán Hình Ảnh - Hoàng Văn Cường",
   "address": "Xã Trung Thành, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3859903,
   "Latitude": 106.0845931
 },
 {
   "STT": 67,
   "Name": "Phòng Khám Bệnh 368 - Lê Thị Quế",
   "address": "Số 100, đường Lương Thế Vinh, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3338938,
   "Latitude": 106.0855816
 },
 {
   "STT": 68,
   "Name": "Phòng Khám Bệnh Thuỷ Lợi - Nguyễn Mạnh Tường",
   "address": "Thôn Phù Cầu, xã Yên Phương, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3697196,
   "Latitude": 105.9494248
 },
 {
   "STT": 69,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Vũ Đình lợi",
   "address": "Xã Yên Tiến, huyện Ý Yên, Nam Định",
   "Longtitude": 20.2894216,
   "Latitude": 106.0346285
 },
 {
   "STT": 70,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Sản - Phạm Văn Chính",
   "address": "Xã Yên Lương, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3060949,
   "Latitude": 106.0728356
 },
 {
   "STT": 71,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Trịnh Văn Hùng",
   "address": "Xã Yên Phương, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3697196,
   "Latitude": 105.9494248
 },
 {
   "STT": 72,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội Hoàng Bách - Hoàng Nam Thân",
   "address": "Xóm 10, khu Tân Lâm, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3684984,
   "Latitude": 105.993491
 },
 {
   "STT": 73,
   "Name": "Phòng Khám Chuyên Khoa Ngoại - Ngô Văn Thư",
   "address": "Xóm chợ Chanh, xã Yên Phúc, huyện Ý Yên., Nam Định",
   "Longtitude": 20.2927326,
   "Latitude": 106.1313978
 },
 {
   "STT": 74,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội Việt Đức - Nguyễn Văn Tình",
   "address": "Phố Cháy, Thị trấn Lâm, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3333296,
   "Latitude": 106.0059782
 },
 {
   "STT": 75,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Thúy Hiệp - Nguyễn Thị Bảy",
   "address": "Xã Yên Chính, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3736721,
   "Latitude": 105.9807959
 },
 {
   "STT": 76,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn Thị Đoan",
   "address": "Xã Yên Hưng, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3344413,
   "Latitude": 105.9552997
 },
 {
   "STT": 77,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Hoàng Hữu Túc",
   "address": "Xã Yên Thọ, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3823928,
   "Latitude": 105.9376756
 },
 {
   "STT": 78,
   "Name": "Phòng Khám Chuyên Khoa Nội Việt Pháp - Hà Nội - Nguyễn Văn Sáu",
   "address": "Thôn Ninh Xá Hạ, xã Yên Xá, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3131714,
   "Latitude": 106.0474858
 },
 {
   "STT": 79,
   "Name": "Phòng Khám Bệnh Nha Khoa Hà Nội - Trần Văn Tuấn",
   "address": "Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 80,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Vũ Quốc Uy",
   "address": "Khu 2, Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0506264,
   "Latitude": 106.1462398
 },
 {
   "STT": 81,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Vũ Hồng Duy",
   "address": "Đội 2, xã Trực Thuận, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2478476,
   "Latitude": 106.1822609
 },
 {
   "STT": 82,
   "Name": "Phòng Khám Chuyên Khoa Chẩn Đoán Hình Ảnh - Trần Văn Kế",
   "address": "Xã Nghĩa Phong, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0889386,
   "Latitude": 106.199266
 },
 {
   "STT": 83,
   "Name": "Phòng Khám Chuyên Khoa Chẩn Đoán Hình Ảnh Ngọc Anh - Đặng Bích Liên",
   "address": "Xóm Nam, Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 84,
   "Name": "Phòng Khám Chuyên Khoa Chẩn Đoán Hình Ảnh - Bùi Văn Tuynh",
   "address": "Thị trấn Rạng Đông, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9903705,
   "Latitude": 106.140451
 },
 {
   "STT": 85,
   "Name": "Phòng Khám Bệnh Đức Lương - Nguyễn Minh Đức",
   "address": "Xã Nghĩa Tân, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 86,
   "Name": "Phòng Khám Chuyên Khoa Nội - Đinh Văn Phùng",
   "address": "Xã Nghĩa Hải, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0041192,
   "Latitude": 106.1169299
 },
 {
   "STT": 87,
   "Name": "Phòng Khám Chuyên Khoa Nội - Nguyễn văn Lương",
   "address": "Xã Nghĩa Tân, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 88,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn thị Thanh Huyền",
   "address": "Khu 2, Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0506264,
   "Latitude": 106.1462398
 },
 {
   "STT": 89,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn Văn Viện",
   "address": "Xã Nghĩa Thịnh, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2668232,
   "Latitude": 106.1522126
 },
 {
   "STT": 90,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Tống Thị Nụ",
   "address": "Xã Nghĩa Minh, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2611878,
   "Latitude": 106.1198699
 },
 {
   "STT": 91,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Lê Văn Quất",
   "address": "Xã Hoàng Nam, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2354354,
   "Latitude": 106.1169299
 },
 {
   "STT": 92,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn Quang Thiều",
   "address": "Xã Nghĩa Sơn, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.1952157,
   "Latitude": 106.1757379
 },
 {
   "STT": 93,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Lương Văn Cường",
   "address": "Xã Nghĩa Hải, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0041192,
   "Latitude": 106.1169299
 },
 {
   "STT": 94,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Vũ Văn Sao",
   "address": "Xã Nghĩa Đồng, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2931264,
   "Latitude": 106.1492721
 },
 {
   "STT": 95,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Hà Ngọc Phong",
   "address": "Xã Nghĩa Bình, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0542545,
   "Latitude": 106.199266
 },
 {
   "STT": 96,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Đoàn Văn Thái",
   "address": "Xã Nghĩa Lâm, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0040027,
   "Latitude": 106.1492721
 },
 {
   "STT": 97,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn Văn Ký",
   "address": "Xã Nghĩa Sơn, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.1952157,
   "Latitude": 106.1757379
 },
 {
   "STT": 98,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Vũ Thị Phượng",
   "address": "Xã NghĩaTân, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 99,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Bùi Anh Tuấn",
   "address": "Xã Nam Điền, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9776105,
   "Latitude": 106.1381064
 },
 {
   "STT": 100,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Đinh Trường Kiên",
   "address": "Xã Nghĩa Hải, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0041192,
   "Latitude": 106.1169299
 },
 {
   "STT": 101,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn Văn Hiền",
   "address": "Xã Nghĩa Sơn, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.1952157,
   "Latitude": 106.1757379
 },
 {
   "STT": 102,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Phạm Văn Biên",
   "address": "Xã Nghĩa Trung, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2162838,
   "Latitude": 106.1669156
 },
 {
   "STT": 103,
   "Name": "Cơ Sở Dịch Vụ Nha Khoa - Nguyễn Đức Thành",
   "address": "Xã Nghĩa Thái, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2394151,
   "Latitude": 106.1669156
 },
 {
   "STT": 104,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn Văn Thanh",
   "address": "Xã Nghĩa Phong, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0889386,
   "Latitude": 106.199266
 },
 {
   "STT": 105,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn Văn khương",
   "address": "Xã Hoàng Nam, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2354354,
   "Latitude": 106.1169299
 },
 {
   "STT": 106,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Nguyễn Văn Thành",
   "address": "Xã Nghĩa Lợi, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0081403,
   "Latitude": 106.1669156
 },
 {
   "STT": 107,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội Tổng Hợp - Trịnh Thị Mai Hương",
   "address": "Xóm Thắng, xã Mỹ Xá, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4063375,
   "Latitude": 106.1472217
 },
 {
   "STT": 108,
   "Name": "Phòng Khám Đông A - Trần Quốc Việt",
   "address": "Số 399, đường Phù Nghĩa, phường Lộc Hạ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4487336,
   "Latitude": 106.1825274
 },
 {
   "STT": 109,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Bùi Thị Tuyết Anh",
   "address": "Số 427+429, đường Trường Chinh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4351875,
   "Latitude": 106.1771063
 },
 {
   "STT": 110,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Sản Phụ - Khhgđ - Mai Thị Hà",
   "address": "Đường 21B, Địch Lễ, xã Nam Vân, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4057031,
   "Latitude": 106.1897073
 },
 {
   "STT": 111,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi - Đặng Thị Nga",
   "address": "Khu Ga, thị trấn Mỹ Lộc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4419707,
   "Latitude": 106.1188593
 },
 {
   "STT": 112,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi - Nguyễn Thị Lan Anh",
   "address": "Số 236, Hoàng Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.428196,
   "Latitude": 106.1758439
 },
 {
   "STT": 113,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Đinh Mạnh Cường",
   "address": "Số 240, đường Văn Cao, phường Năng Tĩnh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.419097,
   "Latitude": 106.167439
 },
 {
   "STT": 114,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Đặng Quế Dương",
   "address": "Số 60, đường Hùng Vương, phường Vị Xuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4313407,
   "Latitude": 106.1863354
 },
 {
   "STT": 115,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Sản Phụ - Khhgđ - Bùi Thị Kim Liên",
   "address": "Số 61 (53 cũ) Thành Chung, phường Cửa Bắc, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4285869,
   "Latitude": 106.1702658
 },
 {
   "STT": 116,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Sản Phụ - Khhgđ - Nguyễn Xuân Thu",
   "address": "Số 9, Nguyễn Thi, phường Thống Nhất, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4393724,
   "Latitude": 106.1793361
 },
 {
   "STT": 117,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Sản Phụ - Khhgđ - Nguyễn Thị Thu",
   "address": "Số 461, đường Trần Nhân Tông, phường Phan Đình Phùng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4258074,
   "Latitude": 106.1834713
 },
 {
   "STT": 118,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Sản Phụ - Khhgđ - Trần Thị Bích Hường",
   "address": "Số 69, Hùng Vương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4312658,
   "Latitude": 106.1861958
 },
 {
   "STT": 119,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi - Đặng Đình Triển",
   "address": "Số 220, đường Hùng Vương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4342132,
   "Latitude": 106.1835072
 },
 {
   "STT": 120,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Muội Thị Dung",
   "address": "Số 1, Phúc Châu, phường Ngô Quyền, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4196193,
   "Latitude": 106.1749567
 },
 {
   "STT": 121,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi - Nguyễn Quang Hưng",
   "address": "Số 79, đường Văn Cao, phường Năng Tĩnh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4172256,
   "Latitude": 106.1659739
 },
 {
   "STT": 122,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi - Đỗ Thị Hải Yến",
   "address": "61A, đường Phù Nghĩa, phường Hạ Long, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4449886,
   "Latitude": 106.1837411
 },
 {
   "STT": 123,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội Tổng Hợp - Nguyễn Thị Kim Thu",
   "address": "Số 233, đường Giải Phóng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4251259,
   "Latitude": 106.1588999
 },
 {
   "STT": 124,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Nguyễn Thị Kim Dung",
   "address": "Lô 3, Trần Đại Nghĩa, khu đô thị Hoà Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4330063,
   "Latitude": 106.1711903
 },
 {
   "STT": 125,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi - Đinh Công Minh",
   "address": "Số 145, phố Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4331268,
   "Latitude": 106.1750027
 },
 {
   "STT": 126,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội Tổng Hợp - Nguyễn Văn Hạnh",
   "address": "Đường Đặng Xuân Bảng, thôn Địch Lễ, Nam Vân, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4107782,
   "Latitude": 106.190455
 },
 {
   "STT": 127,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nhi - Đoàn Văn Tinh",
   "address": "Số 38, đường Trần Thái Tông, phường Lộc Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4445706,
   "Latitude": 106.1737547
 },
 {
   "STT": 128,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Sản Phụ - Khhgđ - Lê Thanh Tùng",
   "address": "Số 144, Song Hào, phường Trần Quang Khải, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4126373,
   "Latitude": 106.1643916
 },
 {
   "STT": 129,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Hoàng Ngọc Long",
   "address": "Lô 12, đường Nguyễn Thế Rục, xã Lộc An, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4122461,
   "Latitude": 106.1572169
 },
 {
   "STT": 130,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Trần Văn San",
   "address": "Số 1/74 đường Hưng Yên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.433164,
   "Latitude": 106.172858
 },
 {
   "STT": 131,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Đặng Quang Sinh",
   "address": "Số 145, Đặng Xuân Bảng, xã Nam Vân, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4144791,
   "Latitude": 106.1862659
 },
 {
   "STT": 132,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Tai Mũi Họng - Phạm Thị Kim Dung",
   "address": "Số 341, Lê Hồng Phong, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4332077,
   "Latitude": 106.1870043
 },
 {
   "STT": 133,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Tâm Thần - Nguyễn Thanh Tùng",
   "address": "Tổ 4, Đông Mạc, phường Lộc Hạ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4453104,
   "Latitude": 106.1796068
 },
 {
   "STT": 134,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Phạm Văn Biên",
   "address": "Số 113, Mạc Thị Bưởi, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4331715,
   "Latitude": 106.1769529
 },
 {
   "STT": 135,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Phạm Thúy Mai",
   "address": "Số 6, Bế Văn Đàn, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4387643,
   "Latitude": 106.178423
 },
 {
   "STT": 136,
   "Name": "Phòng Khám Nha Khoa Nụ Cười Việt - Nguyễn Hữu Bản",
   "address": "Km 3, đường 10, xã Lộc An, thành phố Nam Định, Nam Định",
   "Longtitude": 20.39553,
   "Latitude": 106.1482
 },
 {
   "STT": 137,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Mắt - Trần Thị Kim Thục",
   "address": "Số 125, Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4315678,
   "Latitude": 106.1760436
 },
 {
   "STT": 138,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Phạm Đức Phiếm",
   "address": "Số 12B - Ô 20, đường Lê Ngọc Hân, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4443686,
   "Latitude": 106.1817229
 },
 {
   "STT": 139,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Đỗ Quốc Anh",
   "address": "Số 104, Vị Xuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4356439,
   "Latitude": 106.186103
 },
 {
   "STT": 140,
   "Name": "Phòng Khám Chuyên Khoa Mắt - Nguyễn Văn Liên",
   "address": "Số 48, Hà Huy Tập, phường Bà Triệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4283524,
   "Latitude": 106.1714177
 },
 {
   "STT": 141,
   "Name": "Phòng Khám Chuyên Khoa Mắt - Đinh Văn Hưng",
   "address": "Số 363, đường Trần Huy Liệu, xã Mỹ Xá, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4153294,
   "Latitude": 106.1576164
 },
 {
   "STT": 142,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - Nguyễn Thúy Hường",
   "address": "Số 15, Bắc Ninh, phường Bà Triệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4276416,
   "Latitude": 106.1755922
 },
 {
   "STT": 143,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - Phạm Thị Thu Hà",
   "address": "Số 18, Hùng Vương, phường Vị Xuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.430595,
   "Latitude": 106.187068
 },
 {
   "STT": 144,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Da Liễu - Nguyễn Ngọc Chính",
   "address": "Số 190, Trần Nhật Duật, phường Trần Tế Xương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.438421,
   "Latitude": 106.1859938
 },
 {
   "STT": 145,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Sản Phụ Khoa - Khhgđ - Nguyễn Thị Bắc",
   "address": "Số 25, ngõ Quang Trung, phường Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4319682,
   "Latitude": 106.1741428
 },
 {
   "STT": 146,
   "Name": "Phòng Khám Bệnh Hồng Tâm - Trương Thị Thu Hồng",
   "address": "Số 388, đường Trường Chinh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4351875,
   "Latitude": 106.1771063
 },
 {
   "STT": 147,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Trần Văn Thành",
   "address": "Số 9, tổ 4, khu Quân Nhân, phường Trường Thị, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4276764,
   "Latitude": 106.1704695
 },
 {
   "STT": 148,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Tai Mũi Họng - Đỗ Hồng Phong",
   "address": "Số 214, Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4316779,
   "Latitude": 106.1764951
 },
 {
   "STT": 149,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Mắt - Nguyễn Văn Cương",
   "address": "Số 22B - Ô 18, phường Hạ Long, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4426563,
   "Latitude": 106.1836981
 },
 {
   "STT": 150,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ -Khhgđ - Trịnh Thị Kim Bình",
   "address": "Số 49 Đường Hùng Vương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4349479,
   "Latitude": 106.1827129
 },
 {
   "STT": 151,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Ngô Văn Khanh",
   "address": "Số 99, Trần Huy Liệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4186302,
   "Latitude": 106.1638924
 },
 {
   "STT": 152,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Vũ Trọng Hưởng",
   "address": "Số 370 đường Trần Huy Liệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4152151,
   "Latitude": 106.1577386
 },
 {
   "STT": 153,
   "Name": "Phòng Siêu Âm Quốc Việt - Nguyễn Quốc Việt",
   "address": "Số 166, Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4250221,
   "Latitude": 106.1679429
 },
 {
   "STT": 154,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Trần Quang Quý",
   "address": "Số 522, đường Văn Cao, Lộc An, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4065977,
   "Latitude": 106.1553124
 },
 {
   "STT": 155,
   "Name": "Phòng Khám 70 Chuyên Khoa Phụ Sản - Phạm Văn Oánh",
   "address": "Số 214/114 Mạc Thị Bưởi (Đ. Võ Nguyên Giáp), phường Thống Nhất, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4355316,
   "Latitude": 106.1742175
 },
 {
   "STT": 156,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản - Lương Thị Nữ",
   "address": "Số 258, đường Trường Chinh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4336057,
   "Latitude": 106.1750967
 },
 {
   "STT": 157,
   "Name": "Phòng Khám Bệnh Thành Nam - Phạm Thành Nam",
   "address": "Số 181, Nguyễn Trãi, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4278198,
   "Latitude": 106.1792931
 },
 {
   "STT": 158,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu Thiên Trường - Nguyễn Tiến Thành",
   "address": "Số 361, ngõ 660, đường Điện Biên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4330203,
   "Latitude": 106.1517087
 },
 {
   "STT": 159,
   "Name": "Phòng Khám Chuyên Khoa Tai Mũi Họng - Đặng Thị Thư",
   "address": "Số 58, đường Chu Văn, phường Hạ Long, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4448826,
   "Latitude": 106.1856651
 },
 {
   "STT": 160,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Trần Thị Loan",
   "address": "Số 40, Phan Đình Giót, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4193313,
   "Latitude": 106.1698449
 },
 {
   "STT": 161,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Nội - Thần Kinh - Nguyễn Thị Thanh",
   "address": "Số 33, đường Văn Cao, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4102876,
   "Latitude": 106.1583636
 },
 {
   "STT": 162,
   "Name": "Phòng Xét Nghiệm - Trần Thị Núi",
   "address": "Số 283, Hàn Thuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.437715,
   "Latitude": 106.1843264
 },
 {
   "STT": 163,
   "Name": "Phòng Xét Nghiệm Hóa Sinh - Trần Thị Tâm",
   "address": "Số 228, Minh Khai, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4335787,
   "Latitude": 106.1813023
 },
 {
   "STT": 164,
   "Name": "Phòng Khám Chuyên Khoa Nhi - Phan Thị Minh Hạnh",
   "address": "Số 36, Lý Thường Kiệt, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4302201,
   "Latitude": 106.1731919
 },
 {
   "STT": 165,
   "Name": "Phòng Khám Chuyên Khoa Nhi - Vũ Văn Chuyển",
   "address": "Số 110, Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4310987,
   "Latitude": 106.1788499
 },
 {
   "STT": 166,
   "Name": "Phòng Khám Chuyên Khoa Nhi - Lâm Thị Thắng",
   "address": "Số 2/272, Hàn Thuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4390038,
   "Latitude": 106.186074
 },
 {
   "STT": 167,
   "Name": "Phòng Khám Chuyên Khoa Mắt - Cù Thị Định",
   "address": "Số 260, Hàn Thuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.43879,
   "Latitude": 106.1859169
 },
 {
   "STT": 168,
   "Name": "Phòng Khám Bệnh Sản Phụ Khoa - Nguyễn Thị Lan",
   "address": "Số 68A, Hà Huy Tập, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4285209,
   "Latitude": 106.1713814
 },
 {
   "STT": 169,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - Đỗ Thị Dung",
   "address": "Số 76, đường Phù Nghĩa, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4475902,
   "Latitude": 106.1829392
 },
 {
   "STT": 170,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Nguyễn Văn Thảo",
   "address": "sỐ 131, Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4246356,
   "Latitude": 106.1673997
 },
 {
   "STT": 171,
   "Name": "Pk Nha Khoa Thăng Long - Nguyễn Quốc Cường",
   "address": "Số 264, Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4283179,
   "Latitude": 106.174373
 },
 {
   "STT": 172,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Lê Văn Chuyên",
   "address": "Số 35, Mạc Thị Bưởi, thành phố Nam Định, Nam Định",
   "Longtitude": 20.432205,
   "Latitude": 106.1786428
 },
 {
   "STT": 173,
   "Name": "Phòng Khám Chuyên Khoa Nhi - Đặng Thị Nguyệt",
   "address": "Số 164, Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4307004,
   "Latitude": 106.1751578
 },
 {
   "STT": 174,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Mắt Hoàng Anh - Vũ Hoàng Anh",
   "address": "Số 109, Lương Văn Can, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4420476,
   "Latitude": 106.185626
 },
 {
   "STT": 175,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Bảo Anh - Đinh Quốc Bảo",
   "address": "Số 88, Vũ Trọng Phụng, phường Thống Nhất, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4474073,
   "Latitude": 106.1743986
 },
 {
   "STT": 176,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Mắt Cửa Nam - Hoàng Kim Quế",
   "address": "Số 301, Vũ Hữu Lợi, Cửa Nam, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4105783,
   "Latitude": 106.1803002
 },
 {
   "STT": 177,
   "Name": "Phòng Khám Chuyên Khoa Mắt - Trần Văn Thắng",
   "address": "Số 81, Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4309324,
   "Latitude": 106.17458
 },
 {
   "STT": 178,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Vũ Thị Hà Giang",
   "address": "Số 518, Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4332887,
   "Latitude": 106.1676442
 },
 {
   "STT": 179,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Phương Anh - Trần Văn Việt",
   "address": "Số 494, Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4332738,
   "Latitude": 106.1701788
 },
 {
   "STT": 180,
   "Name": "Phòng Khám Chuyên Khoa Sản Phụ Khoa - Khhgđ - Vũ Thị Thanh Hoan",
   "address": "Số 162, Văn Cao, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4172186,
   "Latitude": 106.1657495
 },
 {
   "STT": 181,
   "Name": "Phòng Khám Chuyên Khoa Nội - Nguyễn Thị Ánh Hồng",
   "address": "Số 24, Lê Ngọc Hân, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4448483,
   "Latitude": 106.1825222
 },
 {
   "STT": 182,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản - Nguyễn Thị Kim Oanh",
   "address": "Số 29, Nguyễn Đức Thuận, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4382888,
   "Latitude": 106.1793675
 },
 {
   "STT": 183,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản - Phạm Văn Bằng",
   "address": "Số 121, Trần Nhật Duật, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4355709,
   "Latitude": 106.1871512
 },
 {
   "STT": 184,
   "Name": "Phòng Khám Chuyên Khoa Nội - Nguyễn Văn Hưng",
   "address": "Xóm 9, xã Nam Toàn, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3864433,
   "Latitude": 106.2022072
 },
 {
   "STT": 185,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Thủ Đô - Ngô Việt Thắng",
   "address": "Số 26, Tân Giang, Nam Thanh, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3183681,
   "Latitude": 106.2492724
 },
 {
   "STT": 186,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Hà Việt Hồng",
   "address": "Số 3, đường Trường Chinh, phường Cửa Bắc, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4304518,
   "Latitude": 106.1708098
 },
 {
   "STT": 187,
   "Name": "Phòng Khám Chuyên Khoa Nội - Lâm Văn Quang",
   "address": "Đại An, xã Nam Thắng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.40082,
   "Latitude": 106.277227
 },
 {
   "STT": 188,
   "Name": "Phòng Khám Chuyên Khoa Nội - Thần Kinh - Trương Tuấn Anh",
   "address": "Số 24, Vũ Ngọc Phan, phường Hạ Long, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4444149,
   "Latitude": 106.1827711
 },
 {
   "STT": 189,
   "Name": "Phòng Khám Chuyên Khoa Sản - Đỗ Văn Lưu",
   "address": "Thôn Hùng Sơn, xã Yên Chính, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3310426,
   "Latitude": 106.0188336
 },
 {
   "STT": 190,
   "Name": "Phòng Khám Chuyên Khoa Nội - Nguyễn Quang Tuyến",
   "address": "Số 7, Trần Huy Liệu, Thị trấn Gôi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3328444,
   "Latitude": 106.0761812
 },
 {
   "STT": 191,
   "Name": "Phòng Khám Chẩn Đoán Hình Ảnh - Trần Minh Tú",
   "address": "Số 360B, Đ. Trường Chinh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4384334,
   "Latitude": 106.181225
 },
 {
   "STT": 192,
   "Name": "Phòng Khám Chẩn Đoán Hình Ảnh - Phan Văn Lợi",
   "address": "Khu 4A, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.287129,
   "Latitude": 106.4473231
 },
 {
   "STT": 193,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Vũ Công Đoàn",
   "address": "Khu A2, thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3154123,
   "Latitude": 106.2757755
 },
 {
   "STT": 194,
   "Name": "Phòng Khám Bệnh Chuyên Khoa Răng Hàm Mặt - Phạm Văn Kiểm",
   "address": "Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.342404,
   "Latitude": 106.1786787
 },
 {
   "STT": 195,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản - Khhgđ - Nguyễn Thị Minh Nguyệt",
   "address": "Số 409, Hàn Thuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4400432,
   "Latitude": 106.1875305
 },
 {
   "STT": 196,
   "Name": "Phòng Khám Chuyên Khoa Nội An Phú - Bùi Mạnh Hải",
   "address": "Thị trấn Thịnh Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.040491,
   "Latitude": 106.2227968
 },
 {
   "STT": 197,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Đỗ Văn Thanh",
   "address": "Số 232, khu 2, thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2044416,
   "Latitude": 106.3000269
 },
 {
   "STT": 198,
   "Name": "Phòng Khám Chuyên Khoa Nội Trung Phúc - Lê Thị Duyên",
   "address": "Đội 8, thôn Hữu Bị, xã Mỹ Trung, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4717069,
   "Latitude": 106.1875016
 },
 {
   "STT": 199,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Phạm Ngọc Hoàn",
   "address": "Số 140, Đặng Xuân Bảng, phường Cửa Nam, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4153329,
   "Latitude": 106.1849792
 },
 {
   "STT": 200,
   "Name": "Phòng Khám Chuyên Khoa Sản - Phạm Thị Dần",
   "address": "Xóm 3, xã Hải Phương, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.195777,
   "Latitude": 106.2854156
 },
 {
   "STT": 201,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt Khiết Tâm - Nguyễn Việt Bắc",
   "address": "Số 337, đường Trường Chinh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4343069,
   "Latitude": 106.1758178
 },
 {
   "STT": 202,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt (Nha Khoa Hiền Vy) - Trần Văn Quý",
   "address": "Số 163, đường Trần Huy Liệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4186234,
   "Latitude": 106.1631628
 },
 {
   "STT": 203,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt (Phòng Khám Trúc Diễm) - Phạm Thị Thư",
   "address": "Số 11, Bùi Huy Đáp, phường Hạ Long, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4439412,
   "Latitude": 106.183583
 },
 {
   "STT": 204,
   "Name": "Phòng Khám Liên Cơ - Vũ Đăng Ngự",
   "address": "Số 71, Nguyễn Văn Trỗi, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4181087,
   "Latitude": 106.1708559
 },
 {
   "STT": 205,
   "Name": "Phòng Khám Chuyên Khoa Răng Hàm Mặt - Phạm Trung Du",
   "address": "Xóm Đông Thắng, xã Xuân Ninh, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.24369,
   "Latitude": 106.322904
 },
 {
   "STT": 206,
   "Name": "Nha Khoa Trường An I - Trần Thị Hải",
   "address": "Số 685, đường Giải Phóng, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4251259,
   "Latitude": 106.1588999
 },
 {
   "STT": 207,
   "Name": "Phòng Khám Chuyên Khoa Nội - Nguyễn Viết Đoán",
   "address": "Xóm 19, xã Hải Nam, huyện hải Hậu, Nam Định",
   "Longtitude": 20.2501137,
   "Latitude": 106.362936
 },
 {
   "STT": 208,
   "Name": "Phòng Khám Chuyên Khoa Da Liễu - Thẩm Mỹ Seoul - Nguyễn Đức Cường",
   "address": "Tầng 1+2, số nhà 530, Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4332818,
   "Latitude": 106.1671911
 },
 {
   "STT": 209,
   "Name": "Phòng Khám Chuyên Khoa Nhi - Nguyễn Thị Khoát",
   "address": "Số 296, khu A2, đường 21B, tổ dân phố Trần Phú, thi trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 210,
   "Name": "Phòng Khám Chuyên Khoa Phụ Sản - Lê Thị Loan",
   "address": "Phố Sở, xã Liên Minh, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.348891,
   "Latitude": 106.1106828
 },
 {
   "STT": 211,
   "Name": "Phòng Khám Sơn Y - Vũ Đình Tùng",
   "address": "Số 154, Lê Hồng Phong, phường Vỵ Xuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4331642,
   "Latitude": 106.1868632
 },
 {
   "STT": 212,
   "Name": "Phòng Khám Chuyên Khoa Nhi - Nguyễn Đức Trọng",
   "address": "Số 191, Trần Huy Liệu, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4181531,
   "Latitude": 106.1620789
 },
 {
   "STT": 213,
   "Name": "Phòng Khám Chuyên Khoa Nội - TrẦN Thị Minh Yến",
   "address": "Cầu Đen, xã Trực Nội, huyện trực Ninh, Nam Định",
   "Longtitude": 20.2489266,
   "Latitude": 106.277241
 },
 {
   "STT": 214,
   "Name": "Phòng Khám Đa Khoa Đình Cự - Phạm Minh Sự",
   "address": "Khu 4, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1986111,
   "Latitude": 106.2944444
 },
 {
   "STT": 215,
   "Name": "Phòng Khám Đa Khoa 248 - Đỗ Trọng Nhĩ",
   "address": "Khu II, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2044416,
   "Latitude": 106.3000269
 },
 {
   "STT": 216,
   "Name": "Phòng Khám Đa Khoa Trường Phúc - Phạm Đức Tăng",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 217,
   "Name": "Phòng Khám Đa Khoa Nam Âu - Vũ Đức Hoàn",
   "address": "khu 4, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1986111,
   "Latitude": 106.2944444
 },
 {
   "STT": 218,
   "Name": "Phòng Khám Đa Khoa Hoành Sơn - Hoàng Văn Đông",
   "address": "Xã Hoành Sơn, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2784066,
   "Latitude": 106.4258643
 },
 {
   "STT": 219,
   "Name": "Phòng Khám Đa Khoa Đại Đồng - Phùng Thiện Quý",
   "address": "Xã Giao Thanh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2762595,
   "Latitude": 106.5083256
 },
 {
   "STT": 220,
   "Name": "Phòng Khám Bệnh Đa Khoa Hữu Nghị Hà Nội - Xuân Trường - Đinh Văn Tiếp",
   "address": "Xóm 14, xã Xuân Kiên, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2660658,
   "Latitude": 106.3299641
 },
 {
   "STT": 221,
   "Name": "Phòng Khám Đa Khoa S.A.R.A Phú Nhai - Phạm Đức Phụng",
   "address": "Xóm Nam, Xuân Phương, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3025442,
   "Latitude": 106.3664684
 },
 {
   "STT": 222,
   "Name": "Phòng Khám Đa Khoa Công Ty Cổ Phần Dịch Vụ Đức Sinh - Vũ Việt Hùng",
   "address": "Km số 3+500 đường 490C, xã Nghĩa An, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2590948,
   "Latitude": 106.1751329
 },
 {
   "STT": 223,
   "Name": "Phòng Khám Đa Khoa Thái Thịnh - Hoàng Quang Vinh",
   "address": "Quốc lộ 37B, xã Tam Thanh, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3186823,
   "Latitude": 106.0834826
 },
 {
   "STT": 224,
   "Name": "Phòng Khám Đa Khoa 568 Cát Đằng - Nguyễn Viết Văn",
   "address": "Thôn Tân Lập, xã Yên Tiến, huyện Ý Yên, Nam Định",
   "Longtitude": 20.2898797,
   "Latitude": 106.0308351
 },
 {
   "STT": 225,
   "Name": "Phòng Khám Đa Khoa Tâm Đức - Đỗ Đại Nghĩa",
   "address": "Số 68, khu 3, Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 226,
   "Name": "Phòng Khám Đa Khoa 105 - Lã Việt Vinh",
   "address": "Số 11, khu II, Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2200799,
   "Latitude": 106.1871388
 },
 {
   "STT": 227,
   "Name": "Phòng Khám Đa Khoa 108 Chi Nhánh Ctcp Y Tế Việt Nam - Lê Văn Tuyển",
   "address": "Số 2, Hoàng Hoa Thám, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4234904,
   "Latitude": 106.1718596
 },
 {
   "STT": 228,
   "Name": "Phòng Khám Đa Khoa Sông Hồng - Trần Gia Khánh",
   "address": "Số 102, Tô Hiệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4215655,
   "Latitude": 106.1737193
 },
 {
   "STT": 229,
   "Name": "Phòng Khám Bệnh Đa Khoa Minh Đức - Trần Thị Kim Liên",
   "address": "Số 60, đường Thái Bình, phường Trần Tế Xương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4417786,
   "Latitude": 106.1915695
 },
 {
   "STT": 230,
   "Name": "Phòng Khám Bệnh Đa Khoa Đông Đô - Hà Nội - Vũ Thị Chăm",
   "address": "Số 39, đường Đông A, khu đô thị Hòa Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.434837,
   "Latitude": 106.161279
 },
 {
   "STT": 231,
   "Name": "Phòng Khám Đa Khoa Hồng Phúc - Trần Thị Vinh",
   "address": "Số 139, Đặng Xuân Bảng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4148905,
   "Latitude": 106.1855992
 },
 {
   "STT": 232,
   "Name": "Phòng Khám Đa Khoa Việt Mỹ - Nguyễn Văn Thạch",
   "address": "Số 1, Hàng Thao, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4236631,
   "Latitude": 106.1780937
 },
 {
   "STT": 233,
   "Name": "Phòng Khám Đa Khoa Sơn Hải - Phạm Văn Hải",
   "address": "Số 138, Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4242624,
   "Latitude": 106.1668433
 },
 {
   "STT": 234,
   "Name": "Phòng Khám Đa Khoa Nam Đô - Nguyễn Văn Liên",
   "address": "Số 51B, Đ. Điện Biên, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3224781,
   "Latitude": 106.2654919
 },
 {
   "STT": 235,
   "Name": "Phòng Khám Đa Khoa Việt Pháp - Đới Văn Huỳnh",
   "address": "Số 2, Tổ 11, Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3424845,
   "Latitude": 106.1812536
 },
 {
   "STT": 236,
   "Name": "Phòng Khám Đa Khoa 568 - Bùi Quang Hợp",
   "address": "Số 100, khu phố 3, thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2188493,
   "Latitude": 106.1881882
 },
 {
   "STT": 237,
   "Name": "Phòng Khám Đa Khoa Hồng Ngọc - Ngô Thị Tâm Oanh",
   "address": "Số 120, Lê Hồng Phong, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4280283,
   "Latitude": 106.1794468
 },
 {
   "STT": 238,
   "Name": "Phòng Khám Đa Khoa An Nhiên - Nguyễn Thị Kim Oanh",
   "address": "Thôn Hậu Bồi Tây, xã Mỹ Phúc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4668918,
   "Latitude": 106.1603691
 },
 {
   "STT": 239,
   "Name": "Phòng Khám Đa Khoa Huy Liệu - Trần Huy Liệu",
   "address": "Khu II, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2044416,
   "Latitude": 106.3000269
 },
 {
   "STT": 240,
   "Name": "Phòng Khám Đa Khoa Trí Đức - Trần Văn Thiệu",
   "address": "Số 507, Trần Huy Liệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4137569,
   "Latitude": 106.1547829
 },
 {
   "STT": 241,
   "Name": "Phòng Khám Đa Khoa Minh Tâm - Trần Thị Hiền",
   "address": "Km 5, thôn Trình Xuyên, xã Liên Bảo, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3740372,
   "Latitude": 106.1231775
 },
 {
   "STT": 242,
   "Name": "Phòng Khám Đa Khoa Giao Phong - Cao Văn Vượng",
   "address": "Xóm Lâm Hồ, xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2216418,
   "Latitude": 106.3785229
 },
 {
   "STT": 243,
   "Name": "Phòng Khám Đa Khoa Hà Thành - Trần Anh Túc",
   "address": "Đội 16, xã Nghĩa Bình, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0542545,
   "Latitude": 106.199266
 },
 {
   "STT": 244,
   "Name": "Phòng Khám Đa Khoa Ý Yên - Hà Nội - Nguyễn Văn Thành",
   "address": "Thôn Hùng Vương, xã Yên Tiến, huyện Ý Yên, Nam Định",
   "Longtitude": 20.29693,
   "Latitude": 106.0358026
 },
 {
   "STT": 245,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Phạm Tất Thắng",
   "address": "Ngọc Đông, xã Trực Thanh, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2534647,
   "Latitude": 106.2478014
 },
 {
   "STT": 246,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Nguyễn Thị Mơ",
   "address": "Chợ Đền, xã Trực Hưng, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2584026,
   "Latitude": 106.2080897
 },
 {
   "STT": 247,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Trịnh Thị Phượng",
   "address": "Trung Lao, xã Trung Đông, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3041028,
   "Latitude": 106.2625114
 },
 {
   "STT": 248,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Nguyễn Văn Thơ",
   "address": "Xã Trực Thái, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1876187,
   "Latitude": 106.2257384
 },
 {
   "STT": 249,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Trần Thị Thanh Duy",
   "address": "Xóm Xuân Lập, xã Hải Xuân, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.0971817,
   "Latitude": 106.2768548
 },
 {
   "STT": 250,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Bùi Thị Lý",
   "address": "Khu 4A, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.287129,
   "Latitude": 106.4473231
 },
 {
   "STT": 251,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Trần Quang Chiêm",
   "address": "Xã Giao Lạc, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2205413,
   "Latitude": 106.5171626
 },
 {
   "STT": 252,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Vũ Thị Tính",
   "address": "Thôn Quyết Thắng, xã Giao Tiến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2702322,
   "Latitude": 106.3905338
 },
 {
   "STT": 253,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Vũ Thị Nhường",
   "address": "Xã Giao Tiến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2702322,
   "Latitude": 106.3905338
 },
 {
   "STT": 254,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Phan Văn Chinh",
   "address": "Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.288267,
   "Latitude": 106.4435319
 },
 {
   "STT": 255,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Nguyễn Văn Luận",
   "address": "Xóm 5, xã Hồng Thuận, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2679945,
   "Latitude": 106.4891799
 },
 {
   "STT": 256,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Doãn Thế Lực",
   "address": "Tổ dân phố 3, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.288267,
   "Latitude": 106.4435319
 },
 {
   "STT": 257,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Phan Thị Ngoan",
   "address": "Xã Bạch Long, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2191505,
   "Latitude": 106.4111425
 },
 {
   "STT": 258,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Vũ Thị Hà",
   "address": "Xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3289924,
   "Latitude": 106.3198908
 },
 {
   "STT": 259,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Cao Thị Thủy",
   "address": "Xã Hồng Quang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3679773,
   "Latitude": 106.2139724
 },
 {
   "STT": 260,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Hoàng Lưu Khoái",
   "address": "Xóm Đông, xã Mỹ Thịnh, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4254084,
   "Latitude": 106.0980049
 },
 {
   "STT": 261,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Đặng Công Lơi",
   "address": "Xóm 5, xã Mỹ Hưng, huyện Mỹ Lộc,, Nam Định",
   "Longtitude": 20.4449491,
   "Latitude": 106.1236074
 },
 {
   "STT": 262,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Trần Ngọc Dũng",
   "address": "Số 79, đường Non Côi, Thị trấn Gôi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3344711,
   "Latitude": 106.0787142
 },
 {
   "STT": 263,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Vũ Văn Dũng",
   "address": "Thị Tứ, Xã Yên Thắng, Huyện Ý Yên, , Nam Định",
   "Longtitude": 20.2881883,
   "Latitude": 106.0629462
 },
 {
   "STT": 264,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Nguyễn Thị Phượng",
   "address": "Xã Yên Chính, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3736721,
   "Latitude": 105.9807959
 },
 {
   "STT": 265,
   "Name": "Cơ Sở Dịch Vụ Y Tế - Phạm Thị Hoan",
   "address": "Khu 8, xã Nghĩa Phúc, huyện Nghĩa Hưng , Nam Định",
   "Longtitude": 19.9947729,
   "Latitude": 106.1843889
 },
 {
   "STT": 266,
   "Name": "Cơ Sở Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà - Vũ Thị xuyến",
   "address": "Số 6F, đường Văn Cao, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4139585,
   "Latitude": 106.1623874
 },
 {
   "STT": 267,
   "Name": "Cơ Sở Dịch Vụ Nha Khoa - Phạm Xuân Sáng",
   "address": "Số 133, Máy Tơ, phường Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4232032,
   "Latitude": 106.1764332
 },
 {
   "STT": 268,
   "Name": "Cơ Sở Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà - Chu Thị Mão",
   "address": "Số 29, Tràng Thi, phường Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 21.0264261,
   "Latitude": 105.8497162
 },
 {
   "STT": 269,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Nguyễn Ngọc Đức",
   "address": "Số 132, Hàn Thuyên, phường Vị Hoàng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4353062,
   "Latitude": 106.1818122
 },
 {
   "STT": 270,
   "Name": "Trung Tâm Vận Chuyển Cấp Cứu 115 - Vũ Thị Chang",
   "address": "Số 4/90, đường Văn Cao, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4139585,
   "Latitude": 106.1623874
 },
 {
   "STT": 271,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Vũ Đức Hiếu",
   "address": "Số 269, đường Phù Nghĩa, phường Lộc Hạ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4473231,
   "Latitude": 106.182803
 },
 {
   "STT": 272,
   "Name": "Cơ Sở Dịch Vụ Cấp Cứu, Hỗ Trợ Vận Chuyển Người Bệnh An Lạc - Lê Huy Ngọc",
   "address": "Số 64, đường Đặng Xuân Bảng, phường Cửa Nam, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4166562,
   "Latitude": 106.1835608
 },
 {
   "STT": 273,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Đinh Văn Chu",
   "address": "Tổ 18, Thị trấn Xuân trường, Nam Định",
   "Longtitude": 20.2913826,
   "Latitude": 106.3340521
 },
 {
   "STT": 274,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Nguyễn Thị Hòa",
   "address": "Xóm 3, Xuân Đài, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3554511,
   "Latitude": 106.3422221
 },
 {
   "STT": 275,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Trần Như Thủy",
   "address": "Đường 10, phố Tây Sơn, thị trấn Gôi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3290959,
   "Latitude": 106.0676
 },
 {
   "STT": 276,
   "Name": "Cơ Sở Dịch Vụ Thay Băng, Đếm Mạch, Đo Huyết Áp, Đo Nhiệt Độ - Phạm Thị Thu Hiền",
   "address": "Thôn Nội, xã Nam Thanh, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3270109,
   "Latitude": 106.254246
 },
 {
   "STT": 277,
   "Name": " Cơ Sở Dịch Vụ 108 Phương Nam - Phạm Văn Nam",
   "address": "Số 326, đường 21B, thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 278,
   "Name": "Dịch Vụ Y Tế Đức Tiến - Nguyễn Đức Tiến",
   "address": "Phố mới, Cầu Cao, thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2557105,
   "Latitude": 106.2698669
 },
 {
   "STT": 279,
   "Name": "Cơ Sở Dịch Vụ 108 Triệu Phương - Lê Văn Triệu",
   "address": "Xóm Thanh Nhân, xã Giao Thanh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2762595,
   "Latitude": 106.5083256
 },
 {
   "STT": 280,
   "Name": "Cơ Sở Dịch Vụ Y Tế - PhạmThị Kim Thoa",
   "address": "Đông Cầu, xã Nam Hùng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3383927,
   "Latitude": 106.2448021
 },
 {
   "STT": 281,
   "Name": "Cơ Sở Dịch Vụ Trồng Răng Giả - Vũ Ngọc Chiển",
   "address": "Chợ Mụa, xã Yên Dương, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3544765,
   "Latitude": 106.0402292
 },
 {
   "STT": 282,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả - Hoàng Văn Hải",
   "address": "Xóm 2, xã Giao Yến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2396176,
   "Latitude": 106.4538386
 },
 {
   "STT": 283,
   "Name": "Cơ Sở Dịch Vụ Trồng Răng Giả - Mai Hòa Bình",
   "address": "Số 305, Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4299675,
   "Latitude": 106.1728288
 },
 {
   "STT": 284,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả - Trần Thế Hùng",
   "address": "Khu 5, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2884171,
   "Latitude": 106.43874
 },
 {
   "STT": 285,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả - Nguyễn Duy Hoàn",
   "address": "Số 3, Lê Hồng Phong, thành phố Nam Định, Nam Định",
   "Longtitude": 20.426213,
   "Latitude": 106.1764116
 },
 {
   "STT": 286,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả - Trần Công Bằng",
   "address": "Xóm 32, xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3289924,
   "Latitude": 106.3209208
 },
 {
   "STT": 287,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Chu Thị Sâm - Chu Thị Sâm",
   "address": "Số 140, ngõ 2 khu A2, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 288,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Phúc Tuy - Hồng Lụa - Vũ Thị Lụa",
   "address": "Đội 12, xã Trực Đạo, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2651636,
   "Latitude": 106.2557838
 },
 {
   "STT": 289,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Xuân Phương Thảo - Vũ Thế Kỷ",
   "address": "Thôn Nhự Nương, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166902,
   "Latitude": 106.2945474
 },
 {
   "STT": 290,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền - Vũ Văn Kỷ",
   "address": "Đội 6, Trực Tuấn, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2750257,
   "Latitude": 106.2778354
 },
 {
   "STT": 291,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Hồng Thịnh",
   "address": "Tổ Bắc Giang, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2561836,
   "Latitude": 106.259656
 },
 {
   "STT": 292,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Nguyễn Hoàng Đoan - Nguyễn Hoàng Đoan",
   "address": "Xóm 7, xã Trực Đại, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1886481,
   "Latitude": 106.2301508
 },
 {
   "STT": 293,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Phạm Ngọc Phương - Phạm Ngọc Phương ",
   "address": "Thôn Phú Ninh, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3142662,
   "Latitude": 106.3000269
 },
 {
   "STT": 294,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Biên",
   "address": "Đội 7, thôn Nam Lạng, xã Trực Tuấn, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2726668,
   "Latitude": 106.2783229
 },
 {
   "STT": 295,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị lan",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 296,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Thị Phi",
   "address": "Thôn Nhật Tân, xã Trực Hưng, huyện Trực Ninh, Nam Định",
   "Longtitude": 21.0764202,
   "Latitude": 105.8260943
 },
 {
   "STT": 297,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Minh",
   "address": "Bắc Trung, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2557105,
   "Latitude": 106.2698669
 },
 {
   "STT": 298,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Hoàng",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 299,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Điệp",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 300,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hải Tân - Đoàn Công Chất",
   "address": "Chợ Đền, xã Trực Hưng, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2584026,
   "Latitude": 106.2080897
 },
 {
   "STT": 301,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Chương",
   "address": "Xóm Vị Nghĩa, xã Trực Phú, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1840534,
   "Latitude": 106.2022072
 },
 {
   "STT": 302,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bùi Văn Sơn - Bùi Văn Sơn",
   "address": "Xóm 4, xã Trực Thái, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.203656,
   "Latitude": 106.2242676
 },
 {
   "STT": 303,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Hữu Văn - Nguyễn Hữu Văn",
   "address": "Xóm 2, xã Trực Thái, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1880526,
   "Latitude": 106.2441241
 },
 {
   "STT": 304,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Lạng - Trần Khắc Viện",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 305,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Lạng Dược Phòng - Vũ Đức Huynh",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 306,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phạm Văn Phòng - Phạm Văn Phòng",
   "address": "Xã Trực Đại, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2154146,
   "Latitude": 106.237505
 },
 {
   "STT": 307,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Chiến",
   "address": "Bắc Đại II, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2557105,
   "Latitude": 106.2698669
 },
 {
   "STT": 308,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Dũng",
   "address": "Xóm An Trạch, Xã Trực Chính, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3310478,
   "Latitude": 106.3059123
 },
 {
   "STT": 309,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Quang Hiếu ",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 310,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Chính",
   "address": "Thôn Tân Lục, xã Liêm Hải, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2870411,
   "Latitude": 106.3051766
 },
 {
   "STT": 311,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đinh Quang Tuyến",
   "address": "Xã Trực Chính, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3312364,
   "Latitude": 106.2963486
 },
 {
   "STT": 312,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Minh Tuấn",
   "address": "Số 54 khu A1, đường Điện Biên, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3224781,
   "Latitude": 106.2654919
 },
 {
   "STT": 313,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Văn Tiêu",
   "address": "Đại Thắng, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2873583,
   "Latitude": 106.3044499
 },
 {
   "STT": 314,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Minh Tân Đường - Vũ Minh Tân",
   "address": "Chợ Giá, xã Trực Đạo, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2625521,
   "Latitude": 106.2557246
 },
 {
   "STT": 315,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Trí Hiếu",
   "address": "Số 47, đường Hữu Nghị, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166427,
   "Latitude": 106.2698125
 },
 {
   "STT": 316,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đoàn Thị Thu",
   "address": "Số 95, đường Hữu Nghị, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166427,
   "Latitude": 106.2698125
 },
 {
   "STT": 317,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Đức Bằng",
   "address": "Số 18, khu A2, đường Hữu Nghị, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166427,
   "Latitude": 106.2698125
 },
 {
   "STT": 318,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Hùng",
   "address": "Chợ Sòng, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3078,
   "Latitude": 106.304573
 },
 {
   "STT": 319,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Cầu",
   "address": "Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 320,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Đại Dương",
   "address": " xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.310163,
   "Latitude": 106.3051766
 },
 {
   "STT": 321,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Minh",
   "address": "Thôn Bắc Trung, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.257643,
   "Latitude": 106.2691803
 },
 {
   "STT": 322,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đoàn Văn Tích",
   "address": "Tổ dân phố số 7, Thị trấn Nam Giang, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3326896,
   "Latitude": 106.1779385
 },
 {
   "STT": 323,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đinh Công Trình",
   "address": "Xã Trực Chính, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3312364,
   "Latitude": 106.2963486
 },
 {
   "STT": 324,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Thị Hoài",
   "address": "Thôn Bằng Trang, xã Trực Thanh, huyện Trực Ninh , Nam Định",
   "Longtitude": 20.243376,
   "Latitude": 106.2169139
 },
 {
   "STT": 325,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đức Thương",
   "address": "Thôn Nhị Nương, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.310163,
   "Latitude": 106.3051766
 },
 {
   "STT": 326,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Bảy",
   "address": "Tổ Bắc Giang, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2561836,
   "Latitude": 106.259656
 },
 {
   "STT": 327,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Chiến",
   "address": "Tổ Bắc Đại 2, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2635402,
   "Latitude": 106.2789881
 },
 {
   "STT": 328,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Đức ngọc",
   "address": "Xóm 13, xã Trực Thanh, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2533842,
   "Latitude": 106.2473722
 },
 {
   "STT": 329,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Quang Huy",
   "address": "Số 140, ngõ 2, khu A2, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 330,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Kim Ngọc Sinh - Kim Văn Sinh",
   "address": "Xã Trực Thái, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1876187,
   "Latitude": 106.2257384
 },
 {
   "STT": 331,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Sinh Đường - Trần Thanh Duẩn",
   "address": "xã Trực Cường, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1945089,
   "Latitude": 106.2139724
 },
 {
   "STT": 332,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Đệ",
   "address": "Xã Liêm Hải, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2870411,
   "Latitude": 106.3051766
 },
 {
   "STT": 333,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Hạnh",
   "address": "Xã Hải Tân, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1484956,
   "Latitude": 106.2728091
 },
 {
   "STT": 334,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Dương Minh Phong",
   "address": "Số 214, Khu 3, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1947452,
   "Latitude": 106.2956129
 },
 {
   "STT": 335,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lợi Thị Mai Son",
   "address": "Xóm 2, xã Hải Tây, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1619171,
   "Latitude": 106.2683958
 },
 {
   "STT": 336,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Văn Sơn",
   "address": "Xã Hải Phong, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1413708,
   "Latitude": 106.2257384
 },
 {
   "STT": 337,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Nghệ",
   "address": "Khu 3, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1947452,
   "Latitude": 106.2956129
 },
 {
   "STT": 338,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Đắc",
   "address": "Số 160, khu 5, Thị trấn Cồn, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1237204,
   "Latitude": 106.2772178
 },
 {
   "STT": 339,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Công Tính",
   "address": "Số 118, khu phố I, Thị trấn Cồn, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.123793,
   "Latitude": 106.2742802
 },
 {
   "STT": 340,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Huệ",
   "address": "Xã Hải Chính, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1121525,
   "Latitude": 106.2904635
 },
 {
   "STT": 341,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Bính",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 342,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Văn Thiệm",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 343,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Chuyên",
   "address": "Xã Hải Trung, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2225231,
   "Latitude": 106.2845785
 },
 {
   "STT": 344,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Ngọc thủy",
   "address": "Xã Hải Quang, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1682755,
   "Latitude": 106.3081194
 },
 {
   "STT": 345,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Oanh",
   "address": "Xã Hải Quang, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1682755,
   "Latitude": 106.3081194
 },
 {
   "STT": 346,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thế Hội",
   "address": "Xã Hải Ninh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1020113,
   "Latitude": 106.2139724
 },
 {
   "STT": 347,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Xuân Đoàn",
   "address": "Xã Hải Phú, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1171417,
   "Latitude": 106.237505
 },
 {
   "STT": 348,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lại Văn Thắng",
   "address": "Xã Hải Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1837317,
   "Latitude": 106.2669247
 },
 {
   "STT": 349,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Thị Sinh",
   "address": "Xã Hải Xuân, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1054255,
   "Latitude": 106.2698669
 },
 {
   "STT": 350,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Hồng Khánh",
   "address": "Xã Hải Tây, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1378811,
   "Latitude": 106.293406
 },
 {
   "STT": 351,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Luyện",
   "address": "Xã Hải Bắc, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.2972495
 },
 {
   "STT": 352,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ văn Thường",
   "address": "Thị trấn Cồn, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.123793,
   "Latitude": 106.2742802
 },
 {
   "STT": 353,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lại Văn Hưng",
   "address": "Xã Hải Hưng, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2087316,
   "Latitude": 106.3081194
 },
 {
   "STT": 354,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lương Văn Duyên",
   "address": "Chợ Trâu, xã Hải Nam, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2136389,
   "Latitude": 106.3217273
 },
 {
   "STT": 355,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Trần mai",
   "address": "Xã Hải Minh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2337015,
   "Latitude": 106.2580983
 },
 {
   "STT": 356,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Lục",
   "address": "Xã Hải Minh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2337015,
   "Latitude": 106.2580983
 },
 {
   "STT": 357,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Thị Oanh",
   "address": "Xã Hải Xuân, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1054255,
   "Latitude": 106.2698669
 },
 {
   "STT": 358,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Quyến",
   "address": "Xã Hải Bắc, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.2972495
 },
 {
   "STT": 359,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Hoan",
   "address": "Xã Hải Châu, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.0777854,
   "Latitude": 106.2257384
 },
 {
   "STT": 360,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đặng Gia Khương",
   "address": "Xã Hải Lý, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.127822,
   "Latitude": 106.3081194
 },
 {
   "STT": 361,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đức Quang",
   "address": "Xã Hải Trung, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2225231,
   "Latitude": 106.2845785
 },
 {
   "STT": 362,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Thị Thu Phương",
   "address": "Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1998722,
   "Latitude": 106.2948773
 },
 {
   "STT": 363,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Viết Cân",
   "address": "Chợ Cồn, xã Hải Hưng, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2087316,
   "Latitude": 106.3081194
 },
 {
   "STT": 364,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Mạnh Thoan",
   "address": "Xã Hải Hà, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1960561,
   "Latitude": 106.3198908
 },
 {
   "STT": 365,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thế Triển",
   "address": "Thị trấn Thịnh Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.040491,
   "Latitude": 106.2227968
 },
 {
   "STT": 366,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Hán",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 367,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Văn Minh",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 368,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lại Văn Tiến",
   "address": "Xã Hải Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1837317,
   "Latitude": 106.2669247
 },
 {
   "STT": 369,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Nhâm",
   "address": "Xã Hải Nam, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2284941,
   "Latitude": 106.3434358
 },
 {
   "STT": 370,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Xuân Tiến",
   "address": "Xã Hải Phú, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1171417,
   "Latitude": 106.237505
 },
 {
   "STT": 371,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Ngũ",
   "address": "Xã Hải Minh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2337015,
   "Latitude": 106.2580983
 },
 {
   "STT": 372,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Xuân Phi",
   "address": "Thị trấn Thịnh Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.040491,
   "Latitude": 106.2227968
 },
 {
   "STT": 373,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Đình Toản",
   "address": "Thị trấn Thịnh Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.040491,
   "Latitude": 106.2227968
 },
 {
   "STT": 374,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đặng Thị Duyên",
   "address": "Số 7, khu I, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1950239,
   "Latitude": 106.2926704
 },
 {
   "STT": 375,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Văn Kế",
   "address": "Xóm 24, xã. Hải Đường, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1947995,
   "Latitude": 106.2606726
 },
 {
   "STT": 376,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Mạnh Đạt",
   "address": "Xã hải Bắc, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.2972495
 },
 {
   "STT": 377,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Sinh Đường - Nguyễn Minh Kha",
   "address": "Khu 3, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2830471,
   "Latitude": 106.4419433
 },
 {
   "STT": 378,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Quốc Hùng",
   "address": "Khu 3, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2830471,
   "Latitude": 106.4419433
 },
 {
   "STT": 379,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Mùi",
   "address": "Chợ Bến, Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2264579,
   "Latitude": 106.3790342
 },
 {
   "STT": 380,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Tình",
   "address": " Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.288267,
   "Latitude": 106.4435319
 },
 {
   "STT": 381,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Định",
   "address": "Khu 3, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2830471,
   "Latitude": 106.4419433
 },
 {
   "STT": 382,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Hồng Đương",
   "address": "Xóm 24, xã Giao Thiện, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2814683,
   "Latitude": 106.5368512
 },
 {
   "STT": 383,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Minh",
   "address": "Xã Giao Châu, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2526894,
   "Latitude": 106.4229199
 },
 {
   "STT": 384,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Duy Phong",
   "address": "Đội 4, Thành Thắng, Xã Giao Châu, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2321124,
   "Latitude": 106.4379491
 },
 {
   "STT": 385,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn hải",
   "address": "Xóm 2, xã Giao Yến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2396176,
   "Latitude": 106.4538386
 },
 {
   "STT": 386,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đức Lợi",
   "address": "Xóm 16, xã Giao Thiện, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2815268,
   "Latitude": 106.5360596
 },
 {
   "STT": 387,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Quyết Tiến",
   "address": "Khu Đình Vuông, Xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.223231,
   "Latitude": 106.3814306
 },
 {
   "STT": 388,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Đình Trưởng",
   "address": "Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2599453,
   "Latitude": 106.4376425
 },
 {
   "STT": 389,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Phụng",
   "address": "Đội 1, Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2612314,
   "Latitude": 106.4358614
 },
 {
   "STT": 390,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Huấn",
   "address": "Xóm 14, xã Hoành Sơn, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2776563,
   "Latitude": 106.4272783
 },
 {
   "STT": 391,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Vũ Phương",
   "address": "Khu 5B, Thị trấn Ngô Đồng, huyện Giao Thủy , Nam Định",
   "Longtitude": 20.287773,
   "Latitude": 106.4380534
 },
 {
   "STT": 392,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Phí",
   "address": "Xã Giao Châu, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2526894,
   "Latitude": 106.4229199
 },
 {
   "STT": 393,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Hảo",
   "address": "Xã Giao Thiện, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.232755,
   "Latitude": 106.5701927
 },
 {
   "STT": 394,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Doãn Đình Pháp",
   "address": "Xón 8, xã Giao An, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2413791,
   "Latitude": 106.5031708
 },
 {
   "STT": 395,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Ký",
   "address": "Xã Giao Lạc, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2205413,
   "Latitude": 106.5171626
 },
 {
   "STT": 396,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đặng Đức Hiển",
   "address": "Đội 3, Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2595934,
   "Latitude": 106.4339335
 },
 {
   "STT": 397,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Cao Thị Sim",
   "address": "Xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.212451,
   "Latitude": 106.3905338
 },
 {
   "STT": 398,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Thắng",
   "address": "Xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.212451,
   "Latitude": 106.3905338
 },
 {
   "STT": 399,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Tân",
   "address": "Xã Giao Thịnh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2262522,
   "Latitude": 106.3669834
 },
 {
   "STT": 400,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Lượn",
   "address": "Xã Giao Thanh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2762595,
   "Latitude": 106.5083256
 },
 {
   "STT": 401,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Xuyến",
   "address": "Khu 5, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2884171,
   "Latitude": 106.43874
 },
 {
   "STT": 402,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Quốc Đạt",
   "address": "Xóm 3, xã Bình Hòa, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2729145,
   "Latitude": 106.4532908
 },
 {
   "STT": 403,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thành Hoan",
   "address": "Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2599453,
   "Latitude": 106.4376425
 },
 {
   "STT": 404,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Huy Vương",
   "address": "Thị trấn Quất Lâm, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.188973,
   "Latitude": 106.3640398
 },
 {
   "STT": 405,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Hùng",
   "address": "Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2599453,
   "Latitude": 106.4376425
 },
 {
   "STT": 406,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Quảng",
   "address": "Xóm Liêm Hải, xã Bạch Long, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2870411,
   "Latitude": 106.3051766
 },
 {
   "STT": 407,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thái Bình",
   "address": "Thị trấn Quất Lâm, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.188973,
   "Latitude": 106.3640398
 },
 {
   "STT": 408,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Hồng Then",
   "address": "Xã Giao Thịnh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2262522,
   "Latitude": 106.3669834
 },
 {
   "STT": 409,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Võ Anh Tuấn",
   "address": "Xóm 2, xã Xuân Châu, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3556838,
   "Latitude": 106.3408576
 },
 {
   "STT": 410,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Hạnh",
   "address": "Xóm 22, xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3296363,
   "Latitude": 106.3205774
 },
 {
   "STT": 411,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Xuân Bào",
   "address": "Xóm 27, xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3289924,
   "Latitude": 106.3198908
 },
 {
   "STT": 412,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Quận",
   "address": "Tổ 10, Thị trấn Xuân Trường, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2876391,
   "Latitude": 106.3372064
 },
 {
   "STT": 413,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Hồng",
   "address": "Xóm 6, xã Xuân Tiến, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2447091,
   "Latitude": 106.3501828
 },
 {
   "STT": 414,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Ngọc Thoan",
   "address": "Xóm 7, xã Xuân Vinh, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2755961,
   "Latitude": 106.3597047
 },
 {
   "STT": 415,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Thành Tuy",
   "address": "Xóm 6, xã Xuân Tiến, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2447091,
   "Latitude": 106.3501828
 },
 {
   "STT": 416,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Năm",
   "address": "Tổ 17, Thị trấn Xuân Trường, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2868482,
   "Latitude": 106.3375493
 },
 {
   "STT": 417,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Tuấn Lợi",
   "address": "Xóm 7, xã Xuân Tiến, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2441291,
   "Latitude": 106.3462114
 },
 {
   "STT": 418,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Quốc Bảo",
   "address": "Xóm 2, Xuân Trung, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2903811,
   "Latitude": 106.3610962
 },
 {
   "STT": 419,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đặng Văn Hiển",
   "address": "Xóm 10, xã Xuân Bắc, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3323395,
   "Latitude": 106.3269707
 },
 {
   "STT": 420,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Quốc Trịnh",
   "address": "Xóm 18, xã Xuân Phong, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3198253,
   "Latitude": 106.3728012
 },
 {
   "STT": 421,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thanh Bình - Nguyễn Thị Nguyệt",
   "address": "Xóm 8, xã Xuân Ninh, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2700081,
   "Latitude": 106.3372765
 },
 {
   "STT": 422,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phan Duy Đạt",
   "address": "Xóm 2, xã Xuân Ninh, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2383741,
   "Latitude": 106.2926704
 },
 {
   "STT": 423,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Công Trứ",
   "address": "Đội 5, xã Xuân Thượng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.339349,
   "Latitude": 106.3313196
 },
 {
   "STT": 424,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Hưng Long - Trần Xuân Đỗ",
   "address": "Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2959716,
   "Latitude": 106.2110311
 },
 {
   "STT": 425,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Mạnh Thắng",
   "address": "Cầu vòi, xã, Hồng Quang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3820049,
   "Latitude": 106.2191695
 },
 {
   "STT": 426,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Văn Lân",
   "address": "Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2959716,
   "Latitude": 106.2110311
 },
 {
   "STT": 427,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Tư",
   "address": "Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2959716,
   "Latitude": 106.2110311
 },
 {
   "STT": 428,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Xuân Tình",
   "address": "Xã Điền Xá, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3952155,
   "Latitude": 106.2316216
 },
 {
   "STT": 429,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đào Duy Chánh",
   "address": "Xã Nam Thái, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2718951,
   "Latitude": 106.1904426
 },
 {
   "STT": 430,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Quốc Huệ",
   "address": "Thôn An Đông, Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2967766,
   "Latitude": 106.2105161
 },
 {
   "STT": 431,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Chí Thanh",
   "address": "Thôn Bình Yên, xã Nam Thanh, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3174595,
   "Latitude": 106.2408877
 },
 {
   "STT": 432,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Thị Thu Hà",
   "address": "Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.342404,
   "Latitude": 106.1786787
 },
 {
   "STT": 433,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Hiền",
   "address": "Xóm 6, thôn Phù Đổng, xã Nam Mỹ, huyện Nam Trực, Nam Định",
   "Longtitude": 20.4031747,
   "Latitude": 106.1987866
 },
 {
   "STT": 434,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trường Sinh - Nguyễn Văn Hội",
   "address": "260 Tân Giang, xã Nam Thanh, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3281256,
   "Latitude": 106.25748
 },
 {
   "STT": 435,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Tiến Quyết",
   "address": " Tân Giang, xã Nam Thanh, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3248791,
   "Latitude": 106.2568729
 },
 {
   "STT": 436,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Thịnh - Nguyễn Thị Thanh Vân",
   "address": "Xóm Hồng Cát, xã Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3480237,
   "Latitude": 106.2452273
 },
 {
   "STT": 437,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Đức Hậu",
   "address": "Xã Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3420547,
   "Latitude": 106.2433886
 },
 {
   "STT": 438,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vạn An Đường - Nguyễn Văn Thanh",
   "address": "Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.342404,
   "Latitude": 106.1786787
 },
 {
   "STT": 439,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Mão",
   "address": "Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2959716,
   "Latitude": 106.2110311
 },
 {
   "STT": 440,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tế Sinh - Nguyễn Văn Bá",
   "address": "Xã Đồng Sơn, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2877399,
   "Latitude": 106.1757379
 },
 {
   "STT": 441,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bảo Phúc Đường - Phạm Minh Tiến",
   "address": " xã Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3420547,
   "Latitude": 106.2433886
 },
 {
   "STT": 442,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Văn Thêm",
   "address": " xã Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3420547,
   "Latitude": 106.2433886
 },
 {
   "STT": 443,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Thái",
   "address": "Thôn 3,Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3375505,
   "Latitude": 106.1871038
 },
 {
   "STT": 444,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Sinh Đường - Bùi Đức Toàn",
   "address": "Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.342404,
   "Latitude": 106.1786787
 },
 {
   "STT": 445,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Lạng - Vũ Quang Huy",
   "address": "Số 140 ngõ 2, khu A2, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 446,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Sơn",
   "address": "Thôn 4, Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3375505,
   "Latitude": 106.1871038
 },
 {
   "STT": 447,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Đức Thắng",
   "address": "Thôn Phú Thụi, xã Nam Thái, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2733443,
   "Latitude": 106.1904426
 },
 {
   "STT": 448,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Tuyết Thanh",
   "address": "Xã nam Mỹ, huyện Nam, Trực, Nam Định",
   "Longtitude": 20.4032351,
   "Latitude": 106.2080897
 },
 {
   "STT": 449,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Chiều",
   "address": "Đông phố Cầu, Nam Hùng, Nam Trực, Nam Định",
   "Longtitude": 20.3405163,
   "Latitude": 106.2125091
 },
 {
   "STT": 450,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Minh Tuấn",
   "address": "Liên Tỉnh, Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3278335,
   "Latitude": 106.2485369
 },
 {
   "STT": 451,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Đình Quang",
   "address": "Xã Mỹ Trung, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4717069,
   "Latitude": 106.1875016
 },
 {
   "STT": 452,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Xuân khắc",
   "address": "Xã Mỹ Trung, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4717069,
   "Latitude": 106.1875016
 },
 {
   "STT": 453,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Xuân Mừng",
   "address": "Xã Mỹ Trung, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4717069,
   "Latitude": 106.1875016
 },
 {
   "STT": 454,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lương Xuân Mười",
   "address": "Thôn Tam Đoài, xã Mỹ Phúc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.465766,
   "Latitude": 106.1622574
 },
 {
   "STT": 455,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Lưu Khoái",
   "address": "Thôn Tam Đông, xã Mỹ Phúc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4655047,
   "Latitude": 106.1613132
 },
 {
   "STT": 456,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Đức Nhân",
   "address": "Thôn Liêm Thôn, xã Mỹ Thịnh, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4293362,
   "Latitude": 106.0812862
 },
 {
   "STT": 457,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Quốc Khánh",
   "address": "Thôn Liêm Trại, xã Mỹ Thịnh, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4293362,
   "Latitude": 106.0812862
 },
 {
   "STT": 458,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Ngọc Luân",
   "address": "Thôn Quả Linh, xã Thành Lợi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.363051,
   "Latitude": 106.1401077
 },
 {
   "STT": 459,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Hữu Toản",
   "address": "Xóm Cầu Ngăm, xã Minh Tân, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3663742,
   "Latitude": 106.0657454
 },
 {
   "STT": 460,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Ngọc Chiêm",
   "address": "Xóm Chợ, xã Thành Lợi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.360476,
   "Latitude": 106.140451
 },
 {
   "STT": 461,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đình Duy",
   "address": "Thôn Thống Nhất, xã Đại Thắng, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3189657,
   "Latitude": 106.1474344
 },
 {
   "STT": 462,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Xuân Lai - Trần Ngọc Cải",
   "address": "Xóm Báng Già, xã Kim Thái, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3525467,
   "Latitude": 106.0941466
 },
 {
   "STT": 463,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Trọng Hiên",
   "address": "Thị trấn Gôi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3344711,
   "Latitude": 106.0787142
 },
 {
   "STT": 464,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Duy Chỉnh",
   "address": "Thôn Quả Linh, xã Thành Lợi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.363051,
   "Latitude": 106.1401077
 },
 {
   "STT": 465,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đoàn Hải Tĩnh",
   "address": "Số 135A, đường 10, Thị trấn Gôi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3320017,
   "Latitude": 106.075176
 },
 {
   "STT": 466,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Tất Đắc",
   "address": "Xã Liên Minh, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.340662,
   "Latitude": 106.1051705
 },
 {
   "STT": 467,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Thìn",
   "address": "Xã Minh Tân, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3771821,
   "Latitude": 106.0552006
 },
 {
   "STT": 468,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Minh",
   "address": "Số nhà 132 đường Lương Thế Vinh, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3338938,
   "Latitude": 106.0855816
 },
 {
   "STT": 469,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hải Nam - Đoàn Văn Nam",
   "address": "Chợ Si, xã Vĩnh Hào, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3176194,
   "Latitude": 106.1204812
 },
 {
   "STT": 470,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Ngọc Trân",
   "address": "Xóm Hội 2, xã Quang Trung, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3785433,
   "Latitude": 106.1022307
 },
 {
   "STT": 471,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Phương",
   "address": "Văn Thị 2, Thị trấn Non Côi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.331564,
   "Latitude": 106.074573
 },
 {
   "STT": 472,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Thanh Tú",
   "address": "Chợ Lời, Hiển Khánh, Vụ Bản, Nam Định",
   "Longtitude": 20.4365582,
   "Latitude": 106.0698963
 },
 {
   "STT": 473,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Tuấn Anh",
   "address": "Thôn Hạnh Lâm, Hiển Khánh, Vụ Bản, Nam Định",
   "Longtitude": 20.4365582,
   "Latitude": 106.0698963
 },
 {
   "STT": 474,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Nông",
   "address": "Thôn Bình Thượng, xã Yên Thọ, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3969666,
   "Latitude": 105.9334249
 },
 {
   "STT": 475,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Tuấn",
   "address": "Chợ Bo, xã Yên Chính, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3736863,
   "Latitude": 105.9819746
 },
 {
   "STT": 476,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Công",
   "address": "Bình Thượng, Yên Thọ, Ý Yên, Nam Định",
   "Longtitude": 20.3969666,
   "Latitude": 105.9334249
 },
 {
   "STT": 477,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đinh Văn Cần",
   "address": "Trung Cường, Yên Cường, huyện Ý Yên, Nam Định",
   "Longtitude": 20.2839168,
   "Latitude": 106.0934117
 },
 {
   "STT": 478,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Chuyên",
   "address": "Thị trấn Lâm, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3231704,
   "Latitude": 106.0140586
 },
 {
   "STT": 479,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Đình Đảm",
   "address": "Thôn Thụy Nội, xã Yên Lương, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3092633,
   "Latitude": 106.0698963
 },
 {
   "STT": 480,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hưng Thái - Nguyễn Văn Hán",
   "address": "Thôn Phúc Chỉ, xã Yên Thắng, huyện Ý Yên, Nam Định",
   "Longtitude": 20.2929222,
   "Latitude": 106.0648495
 },
 {
   "STT": 481,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Đĩnh",
   "address": "Xã Yên Thắng, huyện Ý Yên, Nam Định",
   "Longtitude": 20.2891034,
   "Latitude": 106.0743989
 },
 {
   "STT": 482,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đoàn Xuân Sang",
   "address": "Đội 10, xã Nghĩa Bình, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0213164,
   "Latitude": 106.1871583
 },
 {
   "STT": 483,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Viết Vân",
   "address": "Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0570452,
   "Latitude": 106.1518013
 },
 {
   "STT": 484,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Khắc Thường",
   "address": "Xã Nghĩa Lạc, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.1142679,
   "Latitude": 106.1757379
 },
 {
   "STT": 485,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn văn Viết",
   "address": "Thị trấn Qũy Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0570452,
   "Latitude": 106.1518013
 },
 {
   "STT": 486,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Đình Năm",
   "address": "Xã Nghĩa Hùng, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0377167,
   "Latitude": 106.1286901
 },
 {
   "STT": 487,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Lan",
   "address": "Khu Nội Thị, Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.1876733
 },
 {
   "STT": 488,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phan Thị Thanh Huê",
   "address": "Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0570452,
   "Latitude": 106.1518013
 },
 {
   "STT": 489,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Văn Tưởng",
   "address": "Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 490,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Tư",
   "address": "Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0570452,
   "Latitude": 106.1518013
 },
 {
   "STT": 491,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Bá Thư",
   "address": "Xã Nghĩa Hải, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0041192,
   "Latitude": 106.1169299
 },
 {
   "STT": 492,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đới Tiến Độ",
   "address": "Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 493,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Qốc Trưởng",
   "address": "Xã Nam Điền, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9776105,
   "Latitude": 106.1381064
 },
 {
   "STT": 494,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Quý Dân",
   "address": "Khu 2, Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0506264,
   "Latitude": 106.1462398
 },
 {
   "STT": 495,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Thảo",
   "address": "Thị trấn Rạng Đông, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9903705,
   "Latitude": 106.140451
 },
 {
   "STT": 496,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Vượng",
   "address": "Xã Nghĩa Tân, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 497,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Thái",
   "address": "Xã Nghĩa Tân, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 498,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Thu",
   "address": "Xã Nghĩa Hải, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0041192,
   "Latitude": 106.1169299
 },
 {
   "STT": 499,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Hữu Bộ",
   "address": "Khu 3, Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.1876733
 },
 {
   "STT": 500,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Thiết",
   "address": "Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 501,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Triệu",
   "address": "Xã Nam Điền, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9776105,
   "Latitude": 106.1381064
 },
 {
   "STT": 502,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Lý",
   "address": "Xã Nghĩa Hồng, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0948581,
   "Latitude": 106.1669156
 },
 {
   "STT": 503,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Đăng Doanh",
   "address": "Xã Nghĩa Thành, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0318124,
   "Latitude": 106.1610343
 },
 {
   "STT": 504,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Quang Y",
   "address": "Xã Nghĩa Lợi, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0081403,
   "Latitude": 106.1669156
 },
 {
   "STT": 505,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Viết Hưng",
   "address": "xã Nghĩa Thịnh, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2668232,
   "Latitude": 106.1522126
 },
 {
   "STT": 506,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Thị Hiền",
   "address": "Xã Nghĩa Bình, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0542545,
   "Latitude": 106.199266
 },
 {
   "STT": 507,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Thúy",
   "address": "Xã Nghĩa Bình, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0542545,
   "Latitude": 106.199266
 },
 {
   "STT": 508,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Thị Châu",
   "address": "Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 509,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trịnh Quang Hưng",
   "address": "Thị trấn Rạng Đông, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9903705,
   "Latitude": 106.140451
 },
 {
   "STT": 510,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Huyên",
   "address": "Xã Nghĩa Tân, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 511,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Quốc Thiên",
   "address": "Khu 2, Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0506264,
   "Latitude": 106.1462398
 },
 {
   "STT": 512,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Quảng Long - Vũ Văn Tái",
   "address": "Số 17, đường Bạch Đằng, phường Phan Đình Phùng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4277991,
   "Latitude": 106.1833666
 },
 {
   "STT": 513,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Quốc Tuấn",
   "address": "Số 5B, Thị trấn Trng Tâm Da Liễu, phường Lộc Hạ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4518914,
   "Latitude": 106.1813783
 },
 {
   "STT": 514,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Triệu Văn Đỗ",
   "address": "Km 3, đường 21B, xã Nam Vân, thành phố Nam Định, Nam Định",
   "Longtitude": 20.3974479,
   "Latitude": 106.1866496
 },
 {
   "STT": 515,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Xuân Thỏa",
   "address": "Số 450-452, đường Trường Chinh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4351875,
   "Latitude": 106.1771063
 },
 {
   "STT": 516,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Triệu Quốc Huy",
   "address": "Số 10, đường Tràng Thi, phường Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4202758,
   "Latitude": 106.1650771
 },
 {
   "STT": 517,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Nhàn",
   "address": "Ngõ 241, đường Trần Nhật Duật, thành phố Nam Định, Nam Định",
   "Longtitude": 20.436213,
   "Latitude": 106.1870419
 },
 {
   "STT": 518,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Ngọc Duy",
   "address": "Số 128, đường Trần Quang Khải, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4113174,
   "Latitude": 106.1646223
 },
 {
   "STT": 519,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Trần Hiếu",
   "address": "Số 6/60, Trần Huy Liệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4131104,
   "Latitude": 106.1530319
 },
 {
   "STT": 520,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Thanh",
   "address": "Số 50/109, đường Điện Biên, phường Cửa Bắc, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4297552,
   "Latitude": 106.1647903
 },
 {
   "STT": 521,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Hồng Quân",
   "address": "Số 231, phố Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.42893,
   "Latitude": 106.1749059
 },
 {
   "STT": 522,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Đông",
   "address": "Số 90, Hai Bà Trưng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4265872,
   "Latitude": 106.1760208
 },
 {
   "STT": 523,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Ba",
   "address": "Số 3/338, Hoang Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4304368,
   "Latitude": 106.1737579
 },
 {
   "STT": 524,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Vĩnh Tường",
   "address": "Số 21, Bắc Ninh, phường Bà Triệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4277089,
   "Latitude": 106.1756274
 },
 {
   "STT": 525,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trường Sinh - Bùi Văn Bắc",
   "address": "Số 248, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4292314,
   "Latitude": 106.1746791
 },
 {
   "STT": 526,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Bích Hảo",
   "address": "Số 327, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4299713,
   "Latitude": 106.1740022
 },
 {
   "STT": 527,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Duy Đệ",
   "address": "Số 217, Trần Nhật Duật, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4376618,
   "Latitude": 106.1861384
 },
 {
   "STT": 528,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Ngọc Hưng",
   "address": "Số 49, đường Hoàng Diệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4193803,
   "Latitude": 106.1699903
 },
 {
   "STT": 529,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Thu Hà",
   "address": "Số 238, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4282353,
   "Latitude": 106.1757926
 },
 {
   "STT": 530,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Bào",
   "address": "Số 372, Trần Huy Liệu, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4160512,
   "Latitude": 106.1585705
 },
 {
   "STT": 531,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Đức Hùng",
   "address": "Số 12A, phán Chương B, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4157213,
   "Latitude": 106.1602991
 },
 {
   "STT": 532,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Năm",
   "address": "Số 20, Cửa Trường, phường Ngô Quyền, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4202963,
   "Latitude": 106.1757541
 },
 {
   "STT": 533,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Sơn",
   "address": "Số 80A, Hai Bà Trưng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4262789,
   "Latitude": 106.1763811
 },
 {
   "STT": 534,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thành Đạt - Nguyễn Duy Thành",
   "address": "Số 70, Trần Huy Liệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4191219,
   "Latitude": 106.1653241
 },
 {
   "STT": 535,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thành Đạt - Nguyễn Văn Phòng",
   "address": "Số 134/142, Trần Huy Liệu, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4185764,
   "Latitude": 106.1631324
 },
 {
   "STT": 536,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Đức Anh",
   "address": "Số 66, Trần Phú, phường Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4255796,
   "Latitude": 106.1756701
 },
 {
   "STT": 537,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Năng Doanh",
   "address": "Số 131, đường Phù Long, phường Trần Tế Xương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4362112,
   "Latitude": 106.1902403
 },
 {
   "STT": 538,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Thịnh - Phùng Đình Lạc",
   "address": "Số 304, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4296714,
   "Latitude": 106.1744531
 },
 {
   "STT": 539,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Huy Tuấn",
   "address": "Số 73B, đường Phạm Ngọc Thạch, phường Lộc Hạ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4499894,
   "Latitude": 106.1804191
 },
 {
   "STT": 540,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Năng Biên",
   "address": "Thôn Gia Hòa, xã Lộc An, thành phố Nam Định, Nam Định",
   "Longtitude": 20.410528,
   "Latitude": 106.1512373
 },
 {
   "STT": 541,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Sơn Đường - Đỗ Đức Tâm",
   "address": "Số 292, Trần Huy Liệu, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4175058,
   "Latitude": 106.1594869
 },
 {
   "STT": 542,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Dương Văn Tất",
   "address": "Số 87, ngách 75, ngõ 75, đường Trần Thái Tông, phường Lộc Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4445706,
   "Latitude": 106.1737547
 },
 {
   "STT": 543,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Tuấn Anh",
   "address": "Số 199, đường Văn Cao, phường Năng Tĩnh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4172256,
   "Latitude": 106.1659739
 },
 {
   "STT": 544,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Học",
   "address": "Số 4/49/183, đường Bái, phường Lộc Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4362859,
   "Latitude": 106.1644906
 },
 {
   "STT": 545,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Quang Vinh",
   "address": "Số 219, Hàn Thuyên, phường Vị Xuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4356943,
   "Latitude": 106.1822015
 },
 {
   "STT": 546,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nhất Minh Đường - Trần Minh Thủy",
   "address": "Số 323, đường Trần Thái Tông, phường Lộc Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4550442,
   "Latitude": 106.1742904
 },
 {
   "STT": 547,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Đình Quý",
   "address": "Số 243, Hoàng Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4282746,
   "Latitude": 106.1755696
 },
 {
   "STT": 548,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Đình Thắng",
   "address": "Số 175, Hàng Tiện, phường Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4310391,
   "Latitude": 106.17677
 },
 {
   "STT": 549,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Thị Thúy Hải",
   "address": "Số 190, Trần Nhật Duật, phường Trần Tế Xương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.438421,
   "Latitude": 106.1859938
 },
 {
   "STT": 550,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Minh Ngọc",
   "address": "Số 40, Nguyễn Trãi, phường Vị Hoàng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4282657,
   "Latitude": 106.1847314
 },
 {
   "STT": 551,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Đình Phú",
   "address": "Số 225, Hoàng Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4279702,
   "Latitude": 106.1758435
 },
 {
   "STT": 552,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Mạnh Toàn",
   "address": "Số 266, Hoang Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.428617,
   "Latitude": 106.1752723
 },
 {
   "STT": 553,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Thị Xuân",
   "address": "Số 154, phố Hàng Thao, phường Ngô Quyền, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4220007,
   "Latitude": 106.1761445
 },
 {
   "STT": 554,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Thị Kim Nhung",
   "address": "Số 110, Hàng Tiện, phường Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4301779,
   "Latitude": 106.1759584
 },
 {
   "STT": 555,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Ngọc Thế",
   "address": "Số 155, đường Kênh, phường Cửa Bắc, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4499627,
   "Latitude": 106.1703192
 },
 {
   "STT": 556,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phú Hậu - Vũ Mạnh Tường",
   "address": "Số 127, Hoàng Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4267198,
   "Latitude": 106.1771609
 },
 {
   "STT": 557,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Thành",
   "address": "Số 95, phố Bà Triệu, phường Bà Triệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4291636,
   "Latitude": 106.1734771
 },
 {
   "STT": 558,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Tâm",
   "address": "Đền Vấn Khẩu, phường Cửa Nam, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4111641,
   "Latitude": 106.177631
 },
 {
   "STT": 559,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thái Sinh Đường - Nguyễn Đình Năm",
   "address": "Số 414B, đường Đê Tiền Phong, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4221637,
   "Latitude": 106.1802429
 },
 {
   "STT": 560,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Mùi",
   "address": "Thôn Gia Hòa, xã Lộc An, thành phố Nam Định, Nam Định",
   "Longtitude": 20.410528,
   "Latitude": 106.1512373
 },
 {
   "STT": 561,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Vạn Xuân - Hoàng Văn Thắng",
   "address": "Số 15, Nguyễn Khuyến, Mỹ Xá, thành phố Nam Định, Nam Định",
   "Longtitude": 20.422914,
   "Latitude": 106.1563007
 },
 {
   "STT": 562,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Ngọc Bích",
   "address": "Số 1/16, Phan Bội Châu, phường Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4205622,
   "Latitude": 106.1673688
 },
 {
   "STT": 563,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đình Chiên",
   "address": "Số 218, đường Trần Huy Liệu, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4156186,
   "Latitude": 106.1583978
 },
 {
   "STT": 564,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Bích Thủy",
   "address": "Số 287B, đường Trần Huy Liệu, xã Mỹ Xá, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4153294,
   "Latitude": 106.1576164
 },
 {
   "STT": 565,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Ngọc Quang",
   "address": "Số 172, đường Văn Cao, phường Năng Tĩnh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4172256,
   "Latitude": 106.1659739
 },
 {
   "STT": 566,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Thị Lâm",
   "address": "Số 168, đường Văn Cao, phường Năng Tĩnh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.419097,
   "Latitude": 106.167439
 },
 {
   "STT": 567,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Ngọc Bích",
   "address": "Số 68 Hà Huy Tập, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4285209,
   "Latitude": 106.1713814
 },
 {
   "STT": 568,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Chí Công",
   "address": "Số 782, đường Văn Cao, xã Lộc An, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4076848,
   "Latitude": 106.1559256
 },
 {
   "STT": 569,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Mai Phương",
   "address": "Số 245, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4283259,
   "Latitude": 106.1755198
 },
 {
   "STT": 570,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Nhung",
   "address": "Số 43, Tô Hiệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4200168,
   "Latitude": 106.1748251
 },
 {
   "STT": 571,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tùng Khánh - Nguyễn Quốc Tùng",
   "address": "Số 2, Hoàng Hữu Nam, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4287026,
   "Latitude": 106.1759426
 },
 {
   "STT": 572,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Thao",
   "address": "Số 327, đường Văn Cao, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4102876,
   "Latitude": 106.1583636
 },
 {
   "STT": 573,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lại Thị Thắm",
   "address": "Xóm 3, xã Nam Phong, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4230198,
   "Latitude": 106.1978733
 },
 {
   "STT": 574,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Như Giang",
   "address": "Số 103, Lương Thế Vinh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.424752,
   "Latitude": 106.1638326
 },
 {
   "STT": 575,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trịnh Thị Huệ",
   "address": "Số 82, Trần Bích San, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4115555,
   "Latitude": 106.167272
 },
 {
   "STT": 576,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Doanh",
   "address": "Số 69, Chu Văn, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4450839,
   "Latitude": 106.1863614
 },
 {
   "STT": 577,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Xuân Lâm",
   "address": "Số 115, Hàng Thao, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4215602,
   "Latitude": 106.1755887
 },
 {
   "STT": 578,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Văn Thành",
   "address": "số 4b/112, Trần Nhật Duật, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4353867,
   "Latitude": 106.1873717
 },
 {
   "STT": 579,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Bá Tưởng",
   "address": "Thôn Cấp Tiến, xã Mỹ Phúc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4644392,
   "Latitude": 106.161442
 },
 {
   "STT": 580,
   "Name": "Kcb Bằng Y Học Cổ Truyền - Vũ Công Tuy",
   "address": "Đội 12, xã Trực Đạo, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2651636,
   "Latitude": 106.2557838
 },
 {
   "STT": 581,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Kim Thanh",
   "address": "Số 20/45, đường Giải Phóng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4246702,
   "Latitude": 106.1585391
 },
 {
   "STT": 582,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Mẫn",
   "address": "Đường 57A, thị trấn Lâm, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3240802,
   "Latitude": 106.0170962
 },
 {
   "STT": 583,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Văn Mạnh - Trần Thị Khánh Hòa",
   "address": "Thôn Duyên Hưng, xã Nam Lợi, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2968252,
   "Latitude": 106.2408709
 },
 {
   "STT": 584,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Việt Tiệp",
   "address": "Xóm 14, xã Hoành Sơn, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2776563,
   "Latitude": 106.4272783
 },
 {
   "STT": 585,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Sơn",
   "address": "Số 152, Quán Chiền, xã Nam Dương, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3258478,
   "Latitude": 106.1743396
 },
 {
   "STT": 586,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Bào",
   "address": "Đội 8, xóm Thành Tiến, xã Bạch Long, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2191505,
   "Latitude": 106.4111425
 },
 {
   "STT": 587,
   "Name": "Phòng Khám Đa Khoa Ctcpdv Đức Sinh - Vũ Việt Hùng",
   "address": "Km 03+500, đường 490C, xã Nghĩa An, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2590948,
   "Latitude": 106.1751329
 },
 {
   "STT": 588,
   "Name": "Bệnh Viện Đa Khoa Sài Gòn - Nam Định - Đặng Đình Khiêm",
   "address": "Đường Đông A, đô thị Hòa Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4362624,
   "Latitude": 106.1572945
 },
 {
   "STT": 589,
   "Name": "Phòng Khám Đa Khoa Hoành Sơn - Hoàng Văn Đông",
   "address": "Xã Hoành Sơn, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2784066,
   "Latitude": 106.4258643
 },
 {
   "STT": 590,
   "Name": "Phòng Khám Đa Khoa An Nhiên - Nguyễn Thị Kim Oanh",
   "address": "Thôn Hậu Bồi, xã Mỹ Phúc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4668918,
   "Latitude": 106.1603691
 },
 {
   "STT": 591,
   "Name": "Phòng Khám Đa Khoa Nam Âu - Vũ Đức Hoàn",
   "address": "Khu 4, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1986111,
   "Latitude": 106.2944444
 },
 {
   "STT": 592,
   "Name": "Phòng Khám Đa Khoa Dình Cự - Phạm Minh Sự",
   "address": "Số 214B-215, khu 3, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1947452,
   "Latitude": 106.2956129
 },
 {
   "STT": 593,
   "Name": "Phòng Khám Đa Khoa Đình Cự - Phạm Minh Sự",
   "address": "Khu 4, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1986111,
   "Latitude": 106.2944444
 },
 {
   "STT": 594,
   "Name": "Phòng Khám Đa Khoa 248 - Đỗ Trọng Nhĩ",
   "address": "Khu II, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2044416,
   "Latitude": 106.3000269
 },
 {
   "STT": 595,
   "Name": "Phòng Khám Đa Khoa Trường Phúc - Phạm Đức Tăng",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 596,
   "Name": "Phòng Khám Đa Khoa Nam Âu - Vũ Đức Hoàn",
   "address": "khu 4, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1986111,
   "Latitude": 106.2944444
 },
 {
   "STT": 597,
   "Name": "Phòng Khám Đa Khoa Hoành Sơn - Hoàng Văn Đông",
   "address": "Xã Hoành Sơn, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2784066,
   "Latitude": 106.4258643
 },
 {
   "STT": 598,
   "Name": "Phòng Khám Đa Khoa Đại Đồng - Phùng Thiện Quý",
   "address": "Xã Giao Thanh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2762595,
   "Latitude": 106.5083256
 },
 {
   "STT": 599,
   "Name": "Phòng Khám Bệnh Đa Khoa Hữu Nghị Hà Nội - Xuân Trường - Đinh Văn Tiếp",
   "address": "Xóm 14, xã Xuân Kiên, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2660658,
   "Latitude": 106.3299641
 },
 {
   "STT": 600,
   "Name": "Phòng Khám Đa Khoa S.A.R.A Phú Nhai - Phạm Đức Phụng",
   "address": "Xóm Nam, Xuân Phương, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3025442,
   "Latitude": 106.3664684
 },
 {
   "STT": 601,
   "Name": "Phòng Khám Đa Khoa Công Ty Cổ Phần Dịch Vụ Đức Sinh - Vũ Việt Hùng",
   "address": "Km số 3+500 đường 490C, xã Nghĩa An, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2590948,
   "Latitude": 106.1751329
 },
 {
   "STT": 602,
   "Name": "Phòng Khám Đa Khoa Thái Thịnh - Hoàng Quang Vinh",
   "address": "Quốc lộ 37B, xã Tam Thanh, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3186823,
   "Latitude": 106.0834826
 },
 {
   "STT": 603,
   "Name": "Phòng Khám Đa Khoa 568 Cát Đằng - Nguyễn Viết Văn",
   "address": "Thôn Tân Lập, xã Yên Tiến, huyện Ý Yên, Nam Định",
   "Longtitude": 20.2898797,
   "Latitude": 106.0308351
 },
 {
   "STT": 604,
   "Name": "Phòng Khám Đa Khoa Tâm Đức - Đỗ Đại Nghĩa",
   "address": "Số 68, khu 3, Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 605,
   "Name": "Phòng Khám Đa Khoa 105 - Lã Việt Vinh",
   "address": "Số 11, khu II, Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2200799,
   "Latitude": 106.1871388
 },
 {
   "STT": 606,
   "Name": "Phòng Khám Đa Khoa 108 Chi Nhánh Ctcp Y Tế Việt Nam - Lê Văn Tuyển",
   "address": "Số 2, Hoàng Hoa Thám, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4234904,
   "Latitude": 106.1718596
 },
 {
   "STT": 607,
   "Name": "Phòng Khám Đa Khoa Sông Hồng - Trần Gia Khánh",
   "address": "Số 102, Tô Hiệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4215655,
   "Latitude": 106.1737193
 },
 {
   "STT": 608,
   "Name": "Phòng Khám Bệnh Đa Khoa Minh Đức - Trần Thị Kim Liên",
   "address": "Số 60, đường Thái Bình, phường Trần Tế Xương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4417786,
   "Latitude": 106.1915695
 },
 {
   "STT": 609,
   "Name": "Phòng Khám Bệnh Đa Khoa Đông Đô - Hà Nội - Vũ Thị Chăm",
   "address": "Số 39, đường Đông A, khu đô thị Hòa Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.434837,
   "Latitude": 106.161279
 },
 {
   "STT": 610,
   "Name": "Phòng Khám Đa Khoa Hồng Phúc - Trần Thị Vinh",
   "address": "Số 139, Đặng Xuân Bảng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4148905,
   "Latitude": 106.1855992
 },
 {
   "STT": 611,
   "Name": "Phòng Khám Đa Khoa Việt Mỹ - Nguyễn Văn Thạch",
   "address": "Số 1, Hàng Thao, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4236631,
   "Latitude": 106.1780937
 },
 {
   "STT": 612,
   "Name": "Phòng Khám Đa Khoa Sơn Hải - Phạm Văn Hải",
   "address": "Số 138, Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4242624,
   "Latitude": 106.1668433
 },
 {
   "STT": 613,
   "Name": "Phòng Khám Đa Khoa Nam Đô - Nguyễn Văn Liên",
   "address": "Số 51B, Đ. Điện Biên, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3224781,
   "Latitude": 106.2654919
 },
 {
   "STT": 614,
   "Name": "Phòng Khám Đa Khoa Việt Pháp - Đới Văn Huỳnh",
   "address": "Số 2, Tổ 11, Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3424845,
   "Latitude": 106.1812536
 },
 {
   "STT": 615,
   "Name": "Phòng Khám Đa Khoa 568 - Bùi Quang Hợp",
   "address": "Số 100, khu phố 3, thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2188493,
   "Latitude": 106.1881882
 },
 {
   "STT": 616,
   "Name": "Phòng Khám Đa Khoa Hồng Ngọc - Ngô Thị Tâm Oanh",
   "address": "Số 120, Lê Hồng Phong, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4280283,
   "Latitude": 106.1794468
 },
 {
   "STT": 617,
   "Name": "Phòng Khám Đa Khoa An Nhiên - Nguyễn Thị Kim Oanh",
   "address": "Thôn Hậu Bồi Tây, xã Mỹ Phúc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4668918,
   "Latitude": 106.1603691
 },
 {
   "STT": 618,
   "Name": "Phòng Khám Đa Khoa Huy Liệu - Trần Huy Liệu",
   "address": "Khu II, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2044416,
   "Latitude": 106.3000269
 },
 {
   "STT": 619,
   "Name": "Phòng Khám Đa Khoa Trí Đức - Trần Văn Thiệu",
   "address": "Số 507, Trần Huy Liệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4137569,
   "Latitude": 106.1547829
 },
 {
   "STT": 620,
   "Name": "Phòng Khám Đa Khoa Minh Tâm - Trần Thị Hiền",
   "address": "Km 5, thôn Trình Xuyên, xã Liên Bảo, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3740372,
   "Latitude": 106.1231775
 },
 {
   "STT": 621,
   "Name": "Phòng Khám Đa Khoa Giao Phong - Cao Văn Vượng",
   "address": "Xóm Lâm Hồ, xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2216418,
   "Latitude": 106.3785229
 },
 {
   "STT": 622,
   "Name": "Phòng Khám Đa Khoa Hà Thành - Trần Anh Túc",
   "address": "Đội 16, xã Nghĩa Bình, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0542545,
   "Latitude": 106.199266
 },
 {
   "STT": 623,
   "Name": "Phòng Khám Đa Khoa Ý Yên - Hà Nội - Nguyễn Văn Thành",
   "address": "Thôn Hùng Vương, xã Yên Tiến, huyện Ý Yên, Nam Định",
   "Longtitude": 20.29693,
   "Latitude": 106.0358026
 },
 {
   "STT": 624,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Phạm Tất Thắng",
   "address": "Ngọc Đông, xã Trực Thanh, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2534647,
   "Latitude": 106.2478014
 },
 {
   "STT": 625,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Nguyễn Thị Mơ",
   "address": "Chợ Đền, xã Trực Hưng, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2584026,
   "Latitude": 106.2080897
 },
 {
   "STT": 626,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Trịnh Thị Phượng",
   "address": "Trung Lao, xã Trung Đông, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3041028,
   "Latitude": 106.2625114
 },
 {
   "STT": 627,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Nguyễn Văn Thơ",
   "address": "Xã Trực Thái, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1876187,
   "Latitude": 106.2257384
 },
 {
   "STT": 628,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Trần Thị Thanh Duy",
   "address": "Xóm Xuân Lập, xã Hải Xuân, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.0971817,
   "Latitude": 106.2768548
 },
 {
   "STT": 629,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Bùi Thị Lý",
   "address": "Khu 4A, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.287129,
   "Latitude": 106.4473231
 },
 {
   "STT": 630,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Trần Quang Chiêm",
   "address": "Xã Giao Lạc, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2205413,
   "Latitude": 106.5171626
 },
 {
   "STT": 631,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Vũ Thị Tính",
   "address": "Thôn Quyết Thắng, xã Giao Tiến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2702322,
   "Latitude": 106.3905338
 },
 {
   "STT": 632,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Vũ Thị Nhường",
   "address": "Xã Giao Tiến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2702322,
   "Latitude": 106.3905338
 },
 {
   "STT": 633,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Phan Văn Chinh",
   "address": "Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.288267,
   "Latitude": 106.4435319
 },
 {
   "STT": 634,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Nguyễn Văn Luận",
   "address": "Xóm 5, xã Hồng Thuận, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2679945,
   "Latitude": 106.4891799
 },
 {
   "STT": 635,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Doãn Thế Lực",
   "address": "Tổ dân phố 3, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.288267,
   "Latitude": 106.4435319
 },
 {
   "STT": 636,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Phan Thị Ngoan",
   "address": "Xã Bạch Long, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2191505,
   "Latitude": 106.4111425
 },
 {
   "STT": 637,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Vũ Thị Hà",
   "address": "Xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3289924,
   "Latitude": 106.3198908
 },
 {
   "STT": 638,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Cao Thị Thủy",
   "address": "Xã Hồng Quang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3679773,
   "Latitude": 106.2139724
 },
 {
   "STT": 639,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Hoàng Lưu Khoái",
   "address": "Xóm Đông, xã Mỹ Thịnh, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4254084,
   "Latitude": 106.0980049
 },
 {
   "STT": 640,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Đặng Công Lơi",
   "address": "Xóm 5, xã Mỹ Hưng, huyện Mỹ Lộc,, Nam Định",
   "Longtitude": 20.4449491,
   "Latitude": 106.1236074
 },
 {
   "STT": 641,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Trần Ngọc Dũng",
   "address": "Số 79, đường Non Côi, Thị trấn Gôi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3344711,
   "Latitude": 106.0787142
 },
 {
   "STT": 642,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Vũ Văn Dũng",
   "address": "Thị Tứ, Xã Yên Thắng, Huyện Ý Yên, , Nam Định",
   "Longtitude": 20.2881883,
   "Latitude": 106.0629462
 },
 {
   "STT": 643,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Nguyễn Thị Phượng",
   "address": "Xã Yên Chính, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3736721,
   "Latitude": 105.9807959
 },
 {
   "STT": 644,
   "Name": "Cơ Sở Dịch Vụ Y Tế - Phạm Thị Hoan",
   "address": "Khu 8, xã Nghĩa Phúc, huyện Nghĩa Hưng , Nam Định",
   "Longtitude": 19.9947729,
   "Latitude": 106.1843889
 },
 {
   "STT": 645,
   "Name": "Cơ Sở Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà - Vũ Thị xuyến",
   "address": "Số 6F, đường Văn Cao, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4139585,
   "Latitude": 106.1623874
 },
 {
   "STT": 646,
   "Name": "Cơ Sở Dịch Vụ Nha Khoa - Phạm Xuân Sáng",
   "address": "Số 133, Máy Tơ, phường Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4232032,
   "Latitude": 106.1764332
 },
 {
   "STT": 647,
   "Name": "Cơ Sở Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà - Chu Thị Mão",
   "address": "Số 29, Tràng Thi, phường Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 21.0264261,
   "Latitude": 105.8497162
 },
 {
   "STT": 648,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Nguyễn Ngọc Đức",
   "address": "Số 132, Hàn Thuyên, phường Vị Hoàng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4353062,
   "Latitude": 106.1818122
 },
 {
   "STT": 649,
   "Name": "Trung Tâm Vận Chuyển Cấp Cứu 115 - Vũ Thị Chang",
   "address": "Số 4/90, đường Văn Cao, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4139585,
   "Latitude": 106.1623874
 },
 {
   "STT": 650,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Vũ Đức Hiếu",
   "address": "Số 269, đường Phù Nghĩa, phường Lộc Hạ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4473231,
   "Latitude": 106.182803
 },
 {
   "STT": 651,
   "Name": "Cơ Sở Dịch Vụ Cấp Cứu, Hỗ Trợ Vận Chuyển Người Bệnh An Lạc - Lê Huy Ngọc",
   "address": "Số 64, đường Đặng Xuân Bảng, phường Cửa Nam, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4166562,
   "Latitude": 106.1835608
 },
 {
   "STT": 652,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Đinh Văn Chu",
   "address": "Tổ 18, Thị trấn Xuân trường, Nam Định",
   "Longtitude": 20.2913826,
   "Latitude": 106.3340521
 },
 {
   "STT": 653,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Nguyễn Thị Hòa",
   "address": "Xóm 3, Xuân Đài, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3554511,
   "Latitude": 106.3422221
 },
 {
   "STT": 654,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích) Thay Băng, Đếm Mạch Đo Ha, Đo Nhiệt Độ - Trần Như Thủy",
   "address": "Đường 10, phố Tây Sơn, thị trấn Gôi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3290959,
   "Latitude": 106.0676
 },
 {
   "STT": 655,
   "Name": "Cơ Sở Dịch Vụ Thay Băng, Đếm Mạch, Đo Huyết Áp, Đo Nhiệt Độ - Phạm Thị Thu Hiền",
   "address": "Thôn Nội, xã Nam Thanh, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3270109,
   "Latitude": 106.254246
 },
 {
   "STT": 656,
   "Name": " Cơ Sở Dịch Vụ 108 Phương Nam - Phạm Văn Nam",
   "address": "Số 326, đường 21B, thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 657,
   "Name": "Dịch Vụ Y Tế Đức Tiến - Nguyễn Đức Tiến",
   "address": "Phố mới, Cầu Cao, thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2557105,
   "Latitude": 106.2698669
 },
 {
   "STT": 658,
   "Name": "Cơ Sở Dịch Vụ 108 Triệu Phương - Lê Văn Triệu",
   "address": "Xóm Thanh Nhân, xã Giao Thanh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2762595,
   "Latitude": 106.5083256
 },
 {
   "STT": 659,
   "Name": "Cơ Sở Dịch Vụ Y Tế - PhạmThị Kim Thoa",
   "address": "Đông Cầu, xã Nam Hùng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3383927,
   "Latitude": 106.2448021
 },
 {
   "STT": 660,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Chu Thị Sâm - Chu Thị Sâm",
   "address": "Số 140, ngõ 2 khu A2, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 661,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Phúc Tuy - Hồng Lụa - Vũ Thị Lụa",
   "address": "Đội 12, xã Trực Đạo, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2651636,
   "Latitude": 106.2557838
 },
 {
   "STT": 662,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Xuân Phương Thảo - Vũ Thế Kỷ",
   "address": "Thôn Nhự Nương, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166902,
   "Latitude": 106.2945474
 },
 {
   "STT": 663,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền - Vũ Văn Kỷ",
   "address": "Đội 6, Trực Tuấn, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 664,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Hồng Thịnh",
   "address": "Tổ Bắc Giang, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2561836,
   "Latitude": 106.259656
 },
 {
   "STT": 665,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Nguyễn Hoàng Đoan - Nguyễn Hoàng Đoan",
   "address": "Xóm 7, xã Trực Đại, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1886481,
   "Latitude": 106.2301508
 },
 {
   "STT": 666,
   "Name": "Phòng Chân Trị Y Học Cổ Truyền Phạm Ngọc Phương - Phạm Ngọc Phương ",
   "address": "Thôn Phú Ninh, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3142662,
   "Latitude": 106.3000269
 },
 {
   "STT": 667,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Biên",
   "address": "Đội 7, thôn Nam Lạng, xã Trực Tuấn, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2726668,
   "Latitude": 106.2783229
 },
 {
   "STT": 668,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị lan",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 669,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Thị Phi",
   "address": "Thôn Nhật Tân, xã Trực Hưng, huyện Trực Ninh, Nam Định",
   "Longtitude": 21.0764202,
   "Latitude": 105.8260943
 },
 {
   "STT": 670,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Minh",
   "address": "Bắc Trung, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2557105,
   "Latitude": 106.2698669
 },
 {
   "STT": 671,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Hoàng",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 672,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Điệp",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 673,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hải Tân - Đoàn Công Chất",
   "address": "Chợ Đền, xã Trực Hưng, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2584026,
   "Latitude": 106.2080897
 },
 {
   "STT": 674,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Chương",
   "address": "Xóm Vị Nghĩa, xã Trực Phú, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1840534,
   "Latitude": 106.2022072
 },
 {
   "STT": 675,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bùi Văn Sơn - Bùi Văn Sơn",
   "address": "Xóm 4, xã Trực Thái, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.203656,
   "Latitude": 106.2242676
 },
 {
   "STT": 676,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Hữu Văn - Nguyễn Hữu Văn",
   "address": "Xóm 2, xã Trực Thái, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1880526,
   "Latitude": 106.2441241
 },
 {
   "STT": 677,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Lạng - Trần Khắc Viện",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 678,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Lạng Dược Phòng - Vũ Đức Huynh",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 679,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phạm Văn Phòng - Phạm Văn Phòng",
   "address": "Xã Trực Đại, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2154146,
   "Latitude": 106.237505
 },
 {
   "STT": 680,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Chiến",
   "address": "Bắc Đại II, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2557105,
   "Latitude": 106.2698669
 },
 {
   "STT": 681,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Dũng",
   "address": "Xóm An Trạch, Xã Trực Chính, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3310478,
   "Latitude": 106.3059123
 },
 {
   "STT": 682,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Quang Hiếu ",
   "address": "Xã Trực Tuấn, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2751062,
   "Latitude": 106.2786937
 },
 {
   "STT": 683,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Chính",
   "address": "Thôn Tân Lục, xã Liêm Hải, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2870411,
   "Latitude": 106.3051766
 },
 {
   "STT": 684,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đinh Quang Tuyến",
   "address": "Xã Trực Chính, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3312364,
   "Latitude": 106.2963486
 },
 {
   "STT": 685,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Minh Tuấn",
   "address": "Số 54 khu A1, đường Điện Biên, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3224781,
   "Latitude": 106.2654919
 },
 {
   "STT": 686,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Văn Tiêu",
   "address": "Đại Thắng, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2873583,
   "Latitude": 106.3044499
 },
 {
   "STT": 687,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Minh Tân Đường - Vũ Minh Tân",
   "address": "Chợ Giá, xã Trực Đạo, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2625521,
   "Latitude": 106.2557246
 },
 {
   "STT": 688,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Trí Hiếu",
   "address": "Số 47, đường Hữu Nghị, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166427,
   "Latitude": 106.2698125
 },
 {
   "STT": 689,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đoàn Thị Thu",
   "address": "Số 95, đường Hữu Nghị, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166427,
   "Latitude": 106.2698125
 },
 {
   "STT": 690,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Đức Bằng",
   "address": "Số 18, khu A2, đường Hữu Nghị, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3166427,
   "Latitude": 106.2698125
 },
 {
   "STT": 691,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Hùng",
   "address": "Chợ Sòng, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3078,
   "Latitude": 106.304573
 },
 {
   "STT": 692,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Cầu",
   "address": "Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 693,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Đại Dương",
   "address": " xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.310163,
   "Latitude": 106.3051766
 },
 {
   "STT": 694,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Minh",
   "address": "Thôn Bắc Trung, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.257643,
   "Latitude": 106.2691803
 },
 {
   "STT": 695,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đoàn Văn Tích",
   "address": "Tổ dân phố số 7, Thị trấn Nam Giang, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3326896,
   "Latitude": 106.1779385
 },
 {
   "STT": 696,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đinh Công Trình",
   "address": "Xã Trực Chính, Huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3312364,
   "Latitude": 106.2963486
 },
 {
   "STT": 697,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Thị Hoài",
   "address": "Thôn Bằng Trang, xã Trực Thanh, huyện Trực Ninh , Nam Định",
   "Longtitude": 20.243376,
   "Latitude": 106.2169139
 },
 {
   "STT": 698,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đức Thương",
   "address": "Thôn Nhị Nương, xã Phương Định, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.310163,
   "Latitude": 106.3051766
 },
 {
   "STT": 699,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Bảy",
   "address": "Tổ Bắc Giang, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2561836,
   "Latitude": 106.259656
 },
 {
   "STT": 700,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Chiến",
   "address": "Tổ Bắc Đại 2, Thị trấn Cát Thành, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2635402,
   "Latitude": 106.2789881
 },
 {
   "STT": 701,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Đức ngọc",
   "address": "Xóm 13, xã Trực Thanh, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2533842,
   "Latitude": 106.2473722
 },
 {
   "STT": 702,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Quang Huy",
   "address": "Số 140, ngõ 2, khu A2, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 703,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Kim Ngọc Sinh - Kim Văn Sinh",
   "address": "Xã Trực Thái, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1876187,
   "Latitude": 106.2257384
 },
 {
   "STT": 704,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Sinh Đường - Trần Thanh Duẩn",
   "address": "xã Trực Cường, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.1945089,
   "Latitude": 106.2139724
 },
 {
   "STT": 705,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Đệ",
   "address": "Xã Liêm Hải, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2870411,
   "Latitude": 106.3051766
 },
 {
   "STT": 706,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Hạnh",
   "address": "Xã Hải Tân, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1484956,
   "Latitude": 106.2728091
 },
 {
   "STT": 707,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Dương Minh Phong",
   "address": "Số 214, Khu 3, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1947452,
   "Latitude": 106.2956129
 },
 {
   "STT": 708,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lợi Thị Mai Son",
   "address": "Xóm 2, xã Hải Tây, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1619171,
   "Latitude": 106.2683958
 },
 {
   "STT": 709,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Văn Sơn",
   "address": "Xã Hải Phong, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1413708,
   "Latitude": 106.2257384
 },
 {
   "STT": 710,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Nghệ",
   "address": "Khu 3, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1947452,
   "Latitude": 106.2956129
 },
 {
   "STT": 711,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Đắc",
   "address": "Số 160, khu 5, Thị trấn Cồn, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1237204,
   "Latitude": 106.2772178
 },
 {
   "STT": 712,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Công Tính",
   "address": "Số 118, khu phố I, Thị trấn Cồn, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.123793,
   "Latitude": 106.2742802
 },
 {
   "STT": 713,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Huệ",
   "address": "Xã Hải Chính, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1121525,
   "Latitude": 106.2904635
 },
 {
   "STT": 714,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Bính",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 715,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Văn Thiệm",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 716,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Chuyên",
   "address": "Xã Hải Trung, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2225231,
   "Latitude": 106.2845785
 },
 {
   "STT": 717,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Ngọc thủy",
   "address": "Xã Hải Quang, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1682755,
   "Latitude": 106.3081194
 },
 {
   "STT": 718,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Oanh",
   "address": "Xã Hải Quang, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1682755,
   "Latitude": 106.3081194
 },
 {
   "STT": 719,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thế Hội",
   "address": "Xã Hải Ninh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1020113,
   "Latitude": 106.2139724
 },
 {
   "STT": 720,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Xuân Đoàn",
   "address": "Xã Hải Phú, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1171417,
   "Latitude": 106.237505
 },
 {
   "STT": 721,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lại Văn Thắng",
   "address": "Xã Hải Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1837317,
   "Latitude": 106.2669247
 },
 {
   "STT": 722,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Thị Sinh",
   "address": "Xã Hải Xuân, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1054255,
   "Latitude": 106.2698669
 },
 {
   "STT": 723,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Hồng Khánh",
   "address": "Xã Hải Tây, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1378811,
   "Latitude": 106.293406
 },
 {
   "STT": 724,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Luyện",
   "address": "Xã Hải Bắc, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.2972495
 },
 {
   "STT": 725,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ văn Thường",
   "address": "Thị trấn Cồn, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.123793,
   "Latitude": 106.2742802
 },
 {
   "STT": 726,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lại Văn Hưng",
   "address": "Xã Hải Hưng, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2087316,
   "Latitude": 106.3081194
 },
 {
   "STT": 727,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lương Văn Duyên",
   "address": "Chợ Trâu, xã Hải Nam, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2136389,
   "Latitude": 106.3217273
 },
 {
   "STT": 728,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Trần mai",
   "address": "Xã Hải Minh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2337015,
   "Latitude": 106.2580983
 },
 {
   "STT": 729,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Lục",
   "address": "Xã Hải Minh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2337015,
   "Latitude": 106.2580983
 },
 {
   "STT": 730,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Thị Oanh",
   "address": "Xã Hải Xuân, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1054255,
   "Latitude": 106.2698669
 },
 {
   "STT": 731,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Quyến",
   "address": "Xã Hải Bắc, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.2972495
 },
 {
   "STT": 732,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Hoan",
   "address": "Xã Hải Châu, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.0777854,
   "Latitude": 106.2257384
 },
 {
   "STT": 733,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đặng Gia Khương",
   "address": "Xã Hải Lý, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.127822,
   "Latitude": 106.3081194
 },
 {
   "STT": 734,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đức Quang",
   "address": "Xã Hải Trung, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2225231,
   "Latitude": 106.2845785
 },
 {
   "STT": 735,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Thị Thu Phương",
   "address": "Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1998722,
   "Latitude": 106.2948773
 },
 {
   "STT": 736,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Viết Cân",
   "address": "Chợ Cồn, xã Hải Hưng, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2087316,
   "Latitude": 106.3081194
 },
 {
   "STT": 737,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Mạnh Thoan",
   "address": "Xã Hải Hà, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1960561,
   "Latitude": 106.3198908
 },
 {
   "STT": 738,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thế Triển",
   "address": "Thị trấn Thịnh Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.040491,
   "Latitude": 106.2227968
 },
 {
   "STT": 739,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Hán",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 740,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Văn Minh",
   "address": "Xã Hải Anh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2249697,
   "Latitude": 106.2750681
 },
 {
   "STT": 741,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lại Văn Tiến",
   "address": "Xã Hải Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1837317,
   "Latitude": 106.2669247
 },
 {
   "STT": 742,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Nhâm",
   "address": "Xã Hải Nam, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2284941,
   "Latitude": 106.3434358
 },
 {
   "STT": 743,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Xuân Tiến",
   "address": "Xã Hải Phú, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1171417,
   "Latitude": 106.237505
 },
 {
   "STT": 744,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Ngũ",
   "address": "Xã Hải Minh, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2337015,
   "Latitude": 106.2580983
 },
 {
   "STT": 745,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Xuân Phi",
   "address": "Thị trấn Thịnh Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.040491,
   "Latitude": 106.2227968
 },
 {
   "STT": 746,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Đình Toản",
   "address": "Thị trấn Thịnh Long, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.040491,
   "Latitude": 106.2227968
 },
 {
   "STT": 747,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đặng Thị Duyên",
   "address": "Số 7, khu I, Thị trấn Yên Định, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1950239,
   "Latitude": 106.2926704
 },
 {
   "STT": 748,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Văn Kế",
   "address": "Xóm 24, xã. Hải Đường, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.1947995,
   "Latitude": 106.2606726
 },
 {
   "STT": 749,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Mạnh Đạt",
   "address": "Xã hải Bắc, huyện Hải Hậu, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.2972495
 },
 {
   "STT": 750,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Sinh Đường - Nguyễn Minh Kha",
   "address": "Khu 3, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2830471,
   "Latitude": 106.4419433
 },
 {
   "STT": 751,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Quốc Hùng",
   "address": "Khu 3, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2830471,
   "Latitude": 106.4419433
 },
 {
   "STT": 752,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Mùi",
   "address": "Chợ Bến, Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2264579,
   "Latitude": 106.3790342
 },
 {
   "STT": 753,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Tình",
   "address": " Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.288267,
   "Latitude": 106.4435319
 },
 {
   "STT": 754,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Định",
   "address": "Khu 3, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2830471,
   "Latitude": 106.4419433
 },
 {
   "STT": 755,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Hồng Đương",
   "address": "Xóm 24, xã Giao Thiện, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2814683,
   "Latitude": 106.5368512
 },
 {
   "STT": 756,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Minh",
   "address": "Xã Giao Châu, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2526894,
   "Latitude": 106.4229199
 },
 {
   "STT": 757,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Duy Phong",
   "address": "Đội 4, Thành Thắng, Xã Giao Châu, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2321124,
   "Latitude": 106.4379491
 },
 {
   "STT": 758,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn hải",
   "address": "Xóm 2, xã Giao Yến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2396176,
   "Latitude": 106.4538386
 },
 {
   "STT": 759,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đức Lợi",
   "address": "Xóm 16, xã Giao Thiện, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2815268,
   "Latitude": 106.5360596
 },
 {
   "STT": 760,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Quyết Tiến",
   "address": "Khu Đình Vuông, Xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.223231,
   "Latitude": 106.3814306
 },
 {
   "STT": 761,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Đình Trưởng",
   "address": "Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2599453,
   "Latitude": 106.4376425
 },
 {
   "STT": 762,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Phụng",
   "address": "Đội 1, Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2612314,
   "Latitude": 106.4358614
 },
 {
   "STT": 763,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Huấn",
   "address": "Xóm 14, xã Hoành Sơn, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2776563,
   "Latitude": 106.4272783
 },
 {
   "STT": 764,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Vũ Phương",
   "address": "Khu 5B, Thị trấn Ngô Đồng, huyện Giao Thủy , Nam Định",
   "Longtitude": 20.287773,
   "Latitude": 106.4380534
 },
 {
   "STT": 765,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Phí",
   "address": "Xã Giao Châu, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2526894,
   "Latitude": 106.4229199
 },
 {
   "STT": 766,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Hảo",
   "address": "Xã Giao Thiện, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.232755,
   "Latitude": 106.5701927
 },
 {
   "STT": 767,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Doãn Đình Pháp",
   "address": "Xón 8, xã Giao An, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2413791,
   "Latitude": 106.5031708
 },
 {
   "STT": 768,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Ký",
   "address": "Xã Giao Lạc, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2205413,
   "Latitude": 106.5171626
 },
 {
   "STT": 769,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đặng Đức Hiển",
   "address": "Đội 3, Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2595934,
   "Latitude": 106.4339335
 },
 {
   "STT": 770,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Cao Thị Sim",
   "address": "Xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.212451,
   "Latitude": 106.3905338
 },
 {
   "STT": 771,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Thắng",
   "address": "Xã Giao Phong, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.212451,
   "Latitude": 106.3905338
 },
 {
   "STT": 772,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Tân",
   "address": "Xã Giao Thịnh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2262522,
   "Latitude": 106.3669834
 },
 {
   "STT": 773,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Lượn",
   "address": "Xã Giao Thanh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2762595,
   "Latitude": 106.5083256
 },
 {
   "STT": 774,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Xuyến",
   "address": "Khu 5, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2884171,
   "Latitude": 106.43874
 },
 {
   "STT": 775,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Quốc Đạt",
   "address": "Xóm 3, xã Bình Hòa, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2729145,
   "Latitude": 106.4532908
 },
 {
   "STT": 776,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thành Hoan",
   "address": "Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2599453,
   "Latitude": 106.4376425
 },
 {
   "STT": 777,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Huy Vương",
   "address": "Thị trấn Quất Lâm, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.188973,
   "Latitude": 106.3640398
 },
 {
   "STT": 778,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Hùng",
   "address": "Xã Giao Nhân, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2599453,
   "Latitude": 106.4376425
 },
 {
   "STT": 779,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Quảng",
   "address": "Xóm Liêm Hải, xã Bạch Long, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2177038,
   "Latitude": 106.4117289
 },
 {
   "STT": 780,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thái Bình",
   "address": "Thị trấn Quất Lâm, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.188973,
   "Latitude": 106.3640398
 },
 {
   "STT": 781,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Hồng Then",
   "address": "Xã Giao Thịnh, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2262522,
   "Latitude": 106.3669834
 },
 {
   "STT": 782,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Võ Anh Tuấn",
   "address": "Xóm 2, xã Xuân Châu, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3556838,
   "Latitude": 106.3408576
 },
 {
   "STT": 783,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Hạnh",
   "address": "Xóm 22, xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3296363,
   "Latitude": 106.3205774
 },
 {
   "STT": 784,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Xuân Bào",
   "address": "Xóm 27, xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3289924,
   "Latitude": 106.3198908
 },
 {
   "STT": 785,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Quận",
   "address": "Tổ 10, Thị trấn Xuân Trường, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2876391,
   "Latitude": 106.3372064
 },
 {
   "STT": 786,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Hồng",
   "address": "Xóm 6, xã Xuân Tiến, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2447091,
   "Latitude": 106.3501828
 },
 {
   "STT": 787,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Ngọc Thoan",
   "address": "Xóm 7, xã Xuân Vinh, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2755961,
   "Latitude": 106.3597047
 },
 {
   "STT": 788,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Thành Tuy",
   "address": "Xóm 6, xã Xuân Tiến, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2447091,
   "Latitude": 106.3501828
 },
 {
   "STT": 789,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Năm",
   "address": "Tổ 17, Thị trấn Xuân Trường, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2868482,
   "Latitude": 106.3375493
 },
 {
   "STT": 790,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Tuấn Lợi",
   "address": "Xóm 7, xã Xuân Tiến, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2441291,
   "Latitude": 106.3462114
 },
 {
   "STT": 791,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Quốc Bảo",
   "address": "Xóm 2, Xuân Trung, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2903811,
   "Latitude": 106.3610962
 },
 {
   "STT": 792,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đặng Văn Hiển",
   "address": "Xóm 10, xã Xuân Bắc, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3323395,
   "Latitude": 106.3269707
 },
 {
   "STT": 793,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Quốc Trịnh",
   "address": "Xóm 18, xã Xuân Phong, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3198253,
   "Latitude": 106.3728012
 },
 {
   "STT": 794,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thanh Bình - Nguyễn Thị Nguyệt",
   "address": "Xóm 8, xã Xuân Ninh, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2700081,
   "Latitude": 106.3372765
 },
 {
   "STT": 795,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phan Duy Đạt",
   "address": "Xóm 2, xã Xuân Ninh, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.2383741,
   "Latitude": 106.2926704
 },
 {
   "STT": 796,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Công Trứ",
   "address": "Đội 5, xã Xuân Thượng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.339349,
   "Latitude": 106.3313196
 },
 {
   "STT": 797,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Hưng Long - Trần Xuân Đỗ",
   "address": "Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2959716,
   "Latitude": 106.2110311
 },
 {
   "STT": 798,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Mạnh Thắng",
   "address": "Cầu vòi, xã, Hồng Quang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3820049,
   "Latitude": 106.2191695
 },
 {
   "STT": 799,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Văn Lân",
   "address": "Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2959716,
   "Latitude": 106.2110311
 },
 {
   "STT": 800,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Tư",
   "address": "Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2959716,
   "Latitude": 106.2110311
 },
 {
   "STT": 801,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Xuân Tình",
   "address": "Xã Điền Xá, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3952155,
   "Latitude": 106.2316216
 },
 {
   "STT": 802,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đào Duy Chánh",
   "address": "Xã Nam Thái, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2718951,
   "Latitude": 106.1904426
 },
 {
   "STT": 803,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Quốc Huệ",
   "address": "Thôn An Đông, Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2967766,
   "Latitude": 106.2105161
 },
 {
   "STT": 804,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Chí Thanh",
   "address": "Thôn Bình Yên, xã Nam Thanh, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3174595,
   "Latitude": 106.2408877
 },
 {
   "STT": 805,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Thị Thu Hà",
   "address": "Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.342404,
   "Latitude": 106.1786787
 },
 {
   "STT": 806,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Hiền",
   "address": "Xóm 6, thôn Phù Đổng, xã Nam Mỹ, huyện Nam Trực, Nam Định",
   "Longtitude": 20.4031747,
   "Latitude": 106.1987866
 },
 {
   "STT": 807,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trường Sinh - Nguyễn Văn Hội",
   "address": "260 Tân Giang, xã Nam Thanh, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3281256,
   "Latitude": 106.25748
 },
 {
   "STT": 808,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Tiến Quyết",
   "address": " Tân Giang, xã Nam Thanh, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3248791,
   "Latitude": 106.2568729
 },
 {
   "STT": 809,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Thịnh - Nguyễn Thị Thanh Vân",
   "address": "Xóm Hồng Cát, xã Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3480237,
   "Latitude": 106.2452273
 },
 {
   "STT": 810,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Đức Hậu",
   "address": "Xã Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3420547,
   "Latitude": 106.2433886
 },
 {
   "STT": 811,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vạn An Đường - Nguyễn Văn Thanh",
   "address": "Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.342404,
   "Latitude": 106.1786787
 },
 {
   "STT": 812,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Mão",
   "address": "Xã Nam Tiến, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2959716,
   "Latitude": 106.2110311
 },
 {
   "STT": 813,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tế Sinh - Nguyễn Văn Bá",
   "address": "Xã Đồng Sơn, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2877399,
   "Latitude": 106.1757379
 },
 {
   "STT": 814,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bảo Phúc Đường - Phạm Minh Tiến",
   "address": " xã Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3420547,
   "Latitude": 106.2433886
 },
 {
   "STT": 815,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Văn Thêm",
   "address": " xã Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3420547,
   "Latitude": 106.2433886
 },
 {
   "STT": 816,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Thái",
   "address": "Thôn 3,Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3375505,
   "Latitude": 106.1871038
 },
 {
   "STT": 817,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Sinh Đường - Bùi Đức Toàn",
   "address": "Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.342404,
   "Latitude": 106.1786787
 },
 {
   "STT": 818,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Lạng - Vũ Quang Huy",
   "address": "Số 140 ngõ 2, khu A2, Thị trấn Cổ Lễ, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.3155733,
   "Latitude": 106.2786937
 },
 {
   "STT": 819,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Sơn",
   "address": "Thôn 4, Thị trấn Nam Giang, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3375505,
   "Latitude": 106.1871038
 },
 {
   "STT": 820,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Đức Thắng",
   "address": "Thôn Phú Thụi, xã Nam Thái, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2733443,
   "Latitude": 106.1904426
 },
 {
   "STT": 821,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Tuyết Thanh",
   "address": "Xã nam Mỹ, huyện Nam, Trực, Nam Định",
   "Longtitude": 20.4032351,
   "Latitude": 106.2080897
 },
 {
   "STT": 822,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Chiều",
   "address": "Đông phố Cầu, Nam Hùng, Nam Trực, Nam Định",
   "Longtitude": 20.3405163,
   "Latitude": 106.2125091
 },
 {
   "STT": 823,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Minh Tuấn",
   "address": "Liên Tỉnh, Nam Hồng, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3278335,
   "Latitude": 106.2485369
 },
 {
   "STT": 824,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Đình Quang",
   "address": "Xã Mỹ Trung, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4717069,
   "Latitude": 106.1875016
 },
 {
   "STT": 825,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Xuân khắc",
   "address": "Xã Mỹ Trung, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4717069,
   "Latitude": 106.1875016
 },
 {
   "STT": 826,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Xuân Mừng",
   "address": "Xã Mỹ Trung, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4717069,
   "Latitude": 106.1875016
 },
 {
   "STT": 827,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lương Xuân Mười",
   "address": "Thôn Tam Đoài, xã Mỹ Phúc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.465766,
   "Latitude": 106.1622574
 },
 {
   "STT": 828,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Lưu Khoái",
   "address": "Thôn Tam Đông, xã Mỹ Phúc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4655047,
   "Latitude": 106.1613132
 },
 {
   "STT": 829,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Đức Nhân",
   "address": "Thôn Liêm Thôn, xã Mỹ Thịnh, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4293362,
   "Latitude": 106.0812862
 },
 {
   "STT": 830,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Quốc Khánh",
   "address": "Thôn Liêm Trại, xã Mỹ Thịnh, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4293362,
   "Latitude": 106.0812862
 },
 {
   "STT": 831,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Ngọc Luân",
   "address": "Thôn Quả Linh, xã Thành Lợi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.363051,
   "Latitude": 106.1401077
 },
 {
   "STT": 832,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Hữu Toản",
   "address": "Xóm Cầu Ngăm, xã Minh Tân, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3663742,
   "Latitude": 106.0657454
 },
 {
   "STT": 833,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Ngọc Chiêm",
   "address": "Xóm Chợ, xã Thành Lợi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.360476,
   "Latitude": 106.140451
 },
 {
   "STT": 834,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đình Duy",
   "address": "Thôn Thống Nhất, xã Đại Thắng, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3189657,
   "Latitude": 106.1474344
 },
 {
   "STT": 835,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Xuân Lai - Trần Ngọc Cải",
   "address": "Xóm Báng Già, xã Kim Thái, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3525467,
   "Latitude": 106.0941466
 },
 {
   "STT": 836,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Trọng Hiên",
   "address": "Thị trấn Gôi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3344711,
   "Latitude": 106.0787142
 },
 {
   "STT": 837,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Duy Chỉnh",
   "address": "Thôn Quả Linh, xã Thành Lợi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.363051,
   "Latitude": 106.1401077
 },
 {
   "STT": 838,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đoàn Hải Tĩnh",
   "address": "Số 135A, đường 10, Thị trấn Gôi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3320017,
   "Latitude": 106.075176
 },
 {
   "STT": 839,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Tất Đắc",
   "address": "Xã Liên Minh, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.340662,
   "Latitude": 106.1051705
 },
 {
   "STT": 840,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Thìn",
   "address": "Xã Minh Tân, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3771821,
   "Latitude": 106.0552006
 },
 {
   "STT": 841,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Minh",
   "address": "Số nhà 132 đường Lương Thế Vinh, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3338938,
   "Latitude": 106.0855816
 },
 {
   "STT": 842,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hải Nam - Đoàn Văn Nam",
   "address": "Chợ Si, xã Vĩnh Hào, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3176194,
   "Latitude": 106.1204812
 },
 {
   "STT": 843,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Ngọc Trân",
   "address": "Xóm Hội 2, xã Quang Trung, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.3785433,
   "Latitude": 106.1022307
 },
 {
   "STT": 844,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Phương",
   "address": "Văn Thị 2, Thị trấn Non Côi, huyện Vụ Bản, Nam Định",
   "Longtitude": 20.331564,
   "Latitude": 106.074573
 },
 {
   "STT": 845,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Thanh Tú",
   "address": "Chợ Lời, Hiển Khánh, Vụ Bản, Nam Định",
   "Longtitude": 20.4365582,
   "Latitude": 106.0698963
 },
 {
   "STT": 846,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Tuấn Anh",
   "address": "Thôn Hạnh Lâm, Hiển Khánh, Vụ Bản, Nam Định",
   "Longtitude": 20.4365582,
   "Latitude": 106.0698963
 },
 {
   "STT": 847,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Nông",
   "address": "Thôn Bình Thượng, xã Yên Thọ, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3969666,
   "Latitude": 105.9334249
 },
 {
   "STT": 848,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Tuấn",
   "address": "Chợ Bo, xã Yên Chính, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3736863,
   "Latitude": 105.9819746
 },
 {
   "STT": 849,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Công",
   "address": "Bình Thượng, Yên Thọ, Ý Yên, Nam Định",
   "Longtitude": 20.3969666,
   "Latitude": 105.9334249
 },
 {
   "STT": 850,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đinh Văn Cần",
   "address": "Trung Cường, Yên Cường, huyện Ý Yên, Nam Định",
   "Longtitude": 20.2839168,
   "Latitude": 106.0934117
 },
 {
   "STT": 851,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Chuyên",
   "address": "Thị trấn Lâm, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3231704,
   "Latitude": 106.0140586
 },
 {
   "STT": 852,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Đình Đảm",
   "address": "Thôn Thụy Nội, xã Yên Lương, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3092633,
   "Latitude": 106.0698963
 },
 {
   "STT": 853,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hưng Thái - Nguyễn Văn Hán",
   "address": "Thôn Phúc Chỉ, xã Yên Thắng, huyện Ý Yên, Nam Định",
   "Longtitude": 20.2929222,
   "Latitude": 106.0648495
 },
 {
   "STT": 854,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Đĩnh",
   "address": "Xã Yên Thắng, huyện Ý Yên, Nam Định",
   "Longtitude": 20.2891034,
   "Latitude": 106.0743989
 },
 {
   "STT": 855,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đoàn Xuân Sang",
   "address": "Đội 10, xã Nghĩa Bình, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0213164,
   "Latitude": 106.1871583
 },
 {
   "STT": 856,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Viết Vân",
   "address": "Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0570452,
   "Latitude": 106.1518013
 },
 {
   "STT": 857,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Khắc Thường",
   "address": "Xã Nghĩa Lạc, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.1142679,
   "Latitude": 106.1757379
 },
 {
   "STT": 858,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn văn Viết",
   "address": "Thị trấn Qũy Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0570452,
   "Latitude": 106.1518013
 },
 {
   "STT": 859,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Đình Năm",
   "address": "Xã Nghĩa Hùng, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0377167,
   "Latitude": 106.1286901
 },
 {
   "STT": 860,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Lan",
   "address": "Khu Nội Thị, Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.1876733
 },
 {
   "STT": 861,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phan Thị Thanh Huê",
   "address": "Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0570452,
   "Latitude": 106.1518013
 },
 {
   "STT": 862,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Văn Tưởng",
   "address": "Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 863,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Tư",
   "address": "Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0570452,
   "Latitude": 106.1518013
 },
 {
   "STT": 864,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Bá Thư",
   "address": "Xã Nghĩa Hải, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0041192,
   "Latitude": 106.1169299
 },
 {
   "STT": 865,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đới Tiến Độ",
   "address": "Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 866,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Qốc Trưởng",
   "address": "Xã Nam Điền, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9776105,
   "Latitude": 106.1381064
 },
 {
   "STT": 867,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Quý Dân",
   "address": "Khu 2, Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0506264,
   "Latitude": 106.1462398
 },
 {
   "STT": 868,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Thảo",
   "address": "Thị trấn Rạng Đông, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9903705,
   "Latitude": 106.140451
 },
 {
   "STT": 869,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Vượng",
   "address": "Xã Nghĩa Tân, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 870,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Thái",
   "address": "Xã Nghĩa Tân, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 871,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Thu",
   "address": "Xã Nghĩa Hải, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0041192,
   "Latitude": 106.1169299
 },
 {
   "STT": 872,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Hữu Bộ",
   "address": "Khu 3, Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2176815,
   "Latitude": 106.1876733
 },
 {
   "STT": 873,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Thiết",
   "address": "Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 874,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Triệu",
   "address": "Xã Nam Điền, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9776105,
   "Latitude": 106.1381064
 },
 {
   "STT": 875,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Văn Lý",
   "address": "Xã Nghĩa Hồng, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0948581,
   "Latitude": 106.1669156
 },
 {
   "STT": 876,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Đăng Doanh",
   "address": "Xã Nghĩa Thành, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0318124,
   "Latitude": 106.1610343
 },
 {
   "STT": 877,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Quang Y",
   "address": "Xã Nghĩa Lợi, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0081403,
   "Latitude": 106.1669156
 },
 {
   "STT": 878,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Viết Hưng",
   "address": "xã Nghĩa Thịnh, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2668232,
   "Latitude": 106.1522126
 },
 {
   "STT": 879,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Thị Hiền",
   "address": "Xã Nghĩa Bình, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0542545,
   "Latitude": 106.199266
 },
 {
   "STT": 880,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Thúy",
   "address": "Xã Nghĩa Bình, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0542545,
   "Latitude": 106.199266
 },
 {
   "STT": 881,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lê Thị Châu",
   "address": "Thị trấn Liễu Đề, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.2172385,
   "Latitude": 106.1875016
 },
 {
   "STT": 882,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trịnh Quang Hưng",
   "address": "Thị trấn Rạng Đông, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 19.9903705,
   "Latitude": 106.140451
 },
 {
   "STT": 883,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Huyên",
   "address": "Xã Nghĩa Tân, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0448926,
   "Latitude": 106.1757379
 },
 {
   "STT": 884,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Quốc Thiên",
   "address": "Khu 2, Thị trấn Quỹ Nhất, huyện Nghĩa Hưng, Nam Định",
   "Longtitude": 20.0506264,
   "Latitude": 106.1462398
 },
 {
   "STT": 885,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Quảng Long - Vũ Văn Tái",
   "address": "Số 17, đường Bạch Đằng, phường Phan Đình Phùng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4277991,
   "Latitude": 106.1833666
 },
 {
   "STT": 886,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Mai Quốc Tuấn",
   "address": "Số 5B, Thị trấn Trng Tâm Da Liễu, phường Lộc Hạ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4518914,
   "Latitude": 106.1813783
 },
 {
   "STT": 887,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Triệu Văn Đỗ",
   "address": "Km 3, đường 21B, xã Nam Vân, thành phố Nam Định, Nam Định",
   "Longtitude": 20.3974479,
   "Latitude": 106.1866496
 },
 {
   "STT": 888,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Xuân Thỏa",
   "address": "Số 450-452, đường Trường Chinh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4351875,
   "Latitude": 106.1771063
 },
 {
   "STT": 889,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Triệu Quốc Huy",
   "address": "Số 10, đường Tràng Thi, phường Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4202758,
   "Latitude": 106.1650771
 },
 {
   "STT": 890,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Thị Nhàn",
   "address": "Ngõ 241, đường Trần Nhật Duật, thành phố Nam Định, Nam Định",
   "Longtitude": 20.436213,
   "Latitude": 106.1870419
 },
 {
   "STT": 891,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Ngọc Duy",
   "address": "Số 128, đường Trần Quang Khải, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4113174,
   "Latitude": 106.1646223
 },
 {
   "STT": 892,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Trần Hiếu",
   "address": "Số 6/60, Trần Huy Liệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4131104,
   "Latitude": 106.1530319
 },
 {
   "STT": 893,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Thanh",
   "address": "Số 50/109, đường Điện Biên, phường Cửa Bắc, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4297552,
   "Latitude": 106.1647903
 },
 {
   "STT": 894,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Hồng Quân",
   "address": "Số 231, phố Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.42893,
   "Latitude": 106.1749059
 },
 {
   "STT": 895,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Đông",
   "address": "Số 90, Hai Bà Trưng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4265872,
   "Latitude": 106.1760208
 },
 {
   "STT": 896,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Ba",
   "address": "Số 3/338, Hoang Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4304368,
   "Latitude": 106.1737579
 },
 {
   "STT": 897,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Vĩnh Tường",
   "address": "Số 21, Bắc Ninh, phường Bà Triệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4277089,
   "Latitude": 106.1756274
 },
 {
   "STT": 898,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trường Sinh - Bùi Văn Bắc",
   "address": "Số 248, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4292314,
   "Latitude": 106.1746791
 },
 {
   "STT": 899,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Bích Hảo",
   "address": "Số 327, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4299713,
   "Latitude": 106.1740022
 },
 {
   "STT": 900,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Duy Đệ",
   "address": "Số 217, Trần Nhật Duật, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4376618,
   "Latitude": 106.1861384
 },
 {
   "STT": 901,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Ngọc Hưng",
   "address": "Số 49, đường Hoàng Diệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4193803,
   "Latitude": 106.1699903
 },
 {
   "STT": 902,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Thu Hà",
   "address": "Số 238, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4282353,
   "Latitude": 106.1757926
 },
 {
   "STT": 903,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Bào",
   "address": "Số 372, Trần Huy Liệu, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4160512,
   "Latitude": 106.1585705
 },
 {
   "STT": 904,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Đức Hùng",
   "address": "Số 12A, phán Chương B, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4157213,
   "Latitude": 106.1602991
 },
 {
   "STT": 905,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Năm",
   "address": "Số 20, Cửa Trường, phường Ngô Quyền, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4202963,
   "Latitude": 106.1757541
 },
 {
   "STT": 906,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Sơn",
   "address": "Số 80A, Hai Bà Trưng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4262789,
   "Latitude": 106.1763811
 },
 {
   "STT": 907,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thành Đạt - Nguyễn Duy Thành",
   "address": "Số 70, Trần Huy Liệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4191219,
   "Latitude": 106.1653241
 },
 {
   "STT": 908,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thành Đạt - Nguyễn Văn Phòng",
   "address": "Số 134/142, Trần Huy Liệu, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4185764,
   "Latitude": 106.1631324
 },
 {
   "STT": 909,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Đức Anh",
   "address": "Số 66, Trần Phú, phường Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4255796,
   "Latitude": 106.1756701
 },
 {
   "STT": 910,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Năng Doanh",
   "address": "Số 131, đường Phù Long, phường Trần Tế Xương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4362112,
   "Latitude": 106.1902403
 },
 {
   "STT": 911,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Thịnh - Phùng Đình Lạc",
   "address": "Số 304, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4296714,
   "Latitude": 106.1744531
 },
 {
   "STT": 912,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Huy Tuấn",
   "address": "Số 73B, đường Phạm Ngọc Thạch, phường Lộc Hạ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4499894,
   "Latitude": 106.1804191
 },
 {
   "STT": 913,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Năng Biên",
   "address": "Thôn Gia Hòa, xã Lộc An, thành phố Nam Định, Nam Định",
   "Longtitude": 20.410528,
   "Latitude": 106.1512373
 },
 {
   "STT": 914,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Sơn Đường - Đỗ Đức Tâm",
   "address": "Số 292, Trần Huy Liệu, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4175058,
   "Latitude": 106.1594869
 },
 {
   "STT": 915,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Dương Văn Tất",
   "address": "Số 87, ngách 75, ngõ 75, đường Trần Thái Tông, phường Lộc Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4445706,
   "Latitude": 106.1737547
 },
 {
   "STT": 916,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Tuấn Anh",
   "address": "Số 199, đường Văn Cao, phường Năng Tĩnh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4172256,
   "Latitude": 106.1659739
 },
 {
   "STT": 917,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Văn Học",
   "address": "Số 4/49/183, đường Bái, phường Lộc Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4362859,
   "Latitude": 106.1644906
 },
 {
   "STT": 918,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Quang Vinh",
   "address": "Số 219, Hàn Thuyên, phường Vị Xuyên, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4356943,
   "Latitude": 106.1822015
 },
 {
   "STT": 919,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nhất Minh Đường - Trần Minh Thủy",
   "address": "Số 323, đường Trần Thái Tông, phường Lộc Vượng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4550442,
   "Latitude": 106.1742904
 },
 {
   "STT": 920,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Đình Quý",
   "address": "Số 243, Hoàng Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4282746,
   "Latitude": 106.1755696
 },
 {
   "STT": 921,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Đình Thắng",
   "address": "Số 175, Hàng Tiện, phường Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4310391,
   "Latitude": 106.17677
 },
 {
   "STT": 922,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Thị Thúy Hải",
   "address": "Số 190, Trần Nhật Duật, phường Trần Tế Xương, thành phố Nam Định, Nam Định",
   "Longtitude": 20.438421,
   "Latitude": 106.1859938
 },
 {
   "STT": 923,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Minh Ngọc",
   "address": "Số 40, Nguyễn Trãi, phường Vị Hoàng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4282657,
   "Latitude": 106.1847314
 },
 {
   "STT": 924,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Đình Phú",
   "address": "Số 225, Hoàng Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4279702,
   "Latitude": 106.1758435
 },
 {
   "STT": 925,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Mạnh Toàn",
   "address": "Số 266, Hoang Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.428617,
   "Latitude": 106.1752723
 },
 {
   "STT": 926,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bùi Thị Xuân",
   "address": "Số 154, phố Hàng Thao, phường Ngô Quyền, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4220007,
   "Latitude": 106.1761445
 },
 {
   "STT": 927,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lưu Thị Kim Nhung",
   "address": "Số 110, Hàng Tiện, phường Quang Trung, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4301779,
   "Latitude": 106.1759584
 },
 {
   "STT": 928,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Ngọc Thế",
   "address": "Số 155, đường Kênh, phường Cửa Bắc, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4499627,
   "Latitude": 106.1703192
 },
 {
   "STT": 929,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phú Hậu - Vũ Mạnh Tường",
   "address": "Số 127, Hoàng Văn Thụ, phường Nguyễn Du, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4267198,
   "Latitude": 106.1771609
 },
 {
   "STT": 930,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Văn Thành",
   "address": "Số 95, phố Bà Triệu, phường Bà Triệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4291636,
   "Latitude": 106.1734771
 },
 {
   "STT": 931,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Thị Tâm",
   "address": "Đền Vấn Khẩu, phường Cửa Nam, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4111641,
   "Latitude": 106.177631
 },
 {
   "STT": 932,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thái Sinh Đường - Nguyễn Đình Năm",
   "address": "Số 414B, đường Đê Tiền Phong, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4221637,
   "Latitude": 106.1802429
 },
 {
   "STT": 933,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Mùi",
   "address": "Thôn Gia Hòa, xã Lộc An, thành phố Nam Định, Nam Định",
   "Longtitude": 20.410528,
   "Latitude": 106.1512373
 },
 {
   "STT": 934,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Vạn Xuân - Hoàng Văn Thắng",
   "address": "Số 15, Nguyễn Khuyến, Mỹ Xá, thành phố Nam Định, Nam Định",
   "Longtitude": 20.422914,
   "Latitude": 106.1563007
 },
 {
   "STT": 935,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Ngọc Bích",
   "address": "Số 1/16, Phan Bội Châu, phường Trần Đăng Ninh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4205622,
   "Latitude": 106.1673688
 },
 {
   "STT": 936,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Đình Chiên",
   "address": "Số 218, đường Trần Huy Liệu, phường Văn Miếu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4156186,
   "Latitude": 106.1583978
 },
 {
   "STT": 937,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Bích Thủy",
   "address": "Số 287B, đường Trần Huy Liệu, xã Mỹ Xá, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4153294,
   "Latitude": 106.1576164
 },
 {
   "STT": 938,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Phạm Ngọc Quang",
   "address": "Số 172, đường Văn Cao, phường Năng Tĩnh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4172256,
   "Latitude": 106.1659739
 },
 {
   "STT": 939,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Thị Lâm",
   "address": "Số 168, đường Văn Cao, phường Năng Tĩnh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.419097,
   "Latitude": 106.167439
 },
 {
   "STT": 940,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Ngọc Bích",
   "address": "Số 68 Hà Huy Tập, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4285209,
   "Latitude": 106.1713814
 },
 {
   "STT": 941,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Chí Công",
   "address": "Số 782, đường Văn Cao, xã Lộc An, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4076848,
   "Latitude": 106.1559256
 },
 {
   "STT": 942,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Thị Mai Phương",
   "address": "Số 245, Hoàng Văn Thụ, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4283259,
   "Latitude": 106.1755198
 },
 {
   "STT": 943,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Nhung",
   "address": "Số 43, Tô Hiệu, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4200168,
   "Latitude": 106.1748251
 },
 {
   "STT": 944,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tùng Khánh - Nguyễn Quốc Tùng",
   "address": "Số 2, Hoàng Hữu Nam, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4287026,
   "Latitude": 106.1759426
 },
 {
   "STT": 945,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Thị Thao",
   "address": "Số 327, đường Văn Cao, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4102876,
   "Latitude": 106.1583636
 },
 {
   "STT": 946,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Lại Thị Thắm",
   "address": "Xóm 3, xã Nam Phong, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4230198,
   "Latitude": 106.1978733
 },
 {
   "STT": 947,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Như Giang",
   "address": "Số 103, Lương Thế Vinh, thành phố Nam Định, Nam Định",
   "Longtitude": 20.424752,
   "Latitude": 106.1638326
 },
 {
   "STT": 948,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trịnh Thị Huệ",
   "address": "Số 82, Trần Bích San, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4115555,
   "Latitude": 106.167272
 },
 {
   "STT": 949,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Trần Văn Doanh",
   "address": "Số 69, Chu Văn, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4450839,
   "Latitude": 106.1863614
 },
 {
   "STT": 950,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Xuân Lâm",
   "address": "Số 115, Hàng Thao, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4215602,
   "Latitude": 106.1755887
 },
 {
   "STT": 951,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Văn Thành",
   "address": "số 4b/112, Trần Nhật Duật, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4353867,
   "Latitude": 106.1873717
 },
 {
   "STT": 952,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Ngô Bá Tưởng",
   "address": "Thôn Cấp Tiến, xã Mỹ Phúc, huyện Mỹ Lộc, Nam Định",
   "Longtitude": 20.4644392,
   "Latitude": 106.161442
 },
 {
   "STT": 953,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Vũ Công Tuy",
   "address": "Đội 12, xã Trực Đạo, huyện Trực Ninh, Nam Định",
   "Longtitude": 20.2651636,
   "Latitude": 106.2557838
 },
 {
   "STT": 954,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đỗ Thị Kim Thanh",
   "address": "Số 20/45, đường Giải Phóng, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4246702,
   "Latitude": 106.1585391
 },
 {
   "STT": 955,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Mẫn",
   "address": "Đường 57A, thị trấn Lâm, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3240802,
   "Latitude": 106.0170962
 },
 {
   "STT": 956,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Văn Mạnh - Trần Thị Khánh Hòa",
   "address": "Thôn Duyên Hưng, xã Nam Lợi, huyện Nam Trực, Nam Định",
   "Longtitude": 20.2968252,
   "Latitude": 106.2408709
 },
 {
   "STT": 957,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Việt Tiệp",
   "address": "Xóm 14, xã Hoành Sơn, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2776563,
   "Latitude": 106.4272783
 },
 {
   "STT": 958,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hoàng Văn Sơn",
   "address": "Số 152, Quán Chiền, xã Nam Dương, huyện Nam Trực, Nam Định",
   "Longtitude": 20.3258478,
   "Latitude": 106.1743396
 },
 {
   "STT": 959,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Nguyễn Văn Bào",
   "address": "Đội 8, xóm Thành Tiến, xã Bạch Long, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.3460198,
   "Latitude": 105.9668199
 },
 {
   "STT": 960,
   "Name": "Cơ Sở Dịch Vụ Trồng Răng Giả - Vũ Ngọc Chiển",
   "address": "Chợ Mụa, xã Yên Dương, huyện Ý Yên, Nam Định",
   "Longtitude": 20.3544765,
   "Latitude": 106.0402292
 },
 {
   "STT": 961,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả - Hoàng Văn Hải",
   "address": "Xóm 2, xã Giao Yến, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2396176,
   "Latitude": 106.4538386
 },
 {
   "STT": 962,
   "Name": "Cơ Sở Dịch Vụ Trồng Răng Giả - Mai Hòa Bình",
   "address": "Số 305, Trần Hưng Đạo, thành phố Nam Định, Nam Định",
   "Longtitude": 20.4299675,
   "Latitude": 106.1728288
 },
 {
   "STT": 963,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả - Trần Thế Hùng",
   "address": "Khu 5, Thị trấn Ngô Đồng, huyện Giao Thủy, Nam Định",
   "Longtitude": 20.2884171,
   "Latitude": 106.43874
 },
 {
   "STT": 964,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả - Nguyễn Duy Hoàn",
   "address": "Số 3, Lê Hồng Phong, thành phố Nam Định, Nam Định",
   "Longtitude": 20.426213,
   "Latitude": 106.1764116
 },
 {
   "STT": 965,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả - Trần Công Bằng",
   "address": "Xóm 32, xã Xuân Hồng, huyện Xuân Trường, Nam Định",
   "Longtitude": 20.3289924,
   "Latitude": 106.3209208
 }
];